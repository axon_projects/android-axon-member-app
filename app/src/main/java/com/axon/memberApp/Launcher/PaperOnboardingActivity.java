package com.axon.memberApp.Launcher;

import android.content.Intent;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;

public class PaperOnboardingActivity extends AppCompatActivity {
    // UI
    private ViewPager mSlideViewPager;
    private LinearLayout mDotLayout;
    private Button btnSkip;
    // Variables
    private SliderApapter sliderApapter;
    // Arrays
    private TextView[] mDots;
    LinearLayout.LayoutParams params;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paper_onboarding_activity);
        // Initial views from XML file
        initViews();
        addDotsIndicator(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);
    }

    private void initViews(){
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(50,0,50,0);

        mSlideViewPager = findViewById(R.id.sliderViewPager);
        mDotLayout = findViewById(R.id.llvDotsLayout);

        btnSkip = findViewById(R.id.btnSkip);

        sliderApapter = new SliderApapter(this);
        mSlideViewPager.setAdapter(sliderApapter);
        // Views actions
        setViewsListener();
    }

    private void setViewsListener(){
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaperOnboardingActivity.this, LoginActivity.class));
            }
        });
    }

    public void addDotsIndicator(int position){
        mDots = new TextView[3];
        mDotLayout.removeAllViews();

        for(int i = 0; i < mDots.length; i++){
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#9679;"));
            mDots[i].setTextSize(25);
            mDots[i].setLayoutParams(params);
            mDots[i].setTextColor(getResources().getColor(R.color.axonTextDarkPurpleColor));

            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            if(position == mDots.length-1){
                btnSkip.setText(getString(R.string.into_button_finish));
            }else {
                btnSkip.setText(getString(R.string.into_button_skip));
            }
            mDots[position].setText(Html.fromHtml("&#9675;"));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}

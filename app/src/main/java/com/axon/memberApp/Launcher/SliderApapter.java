package com.axon.memberApp.Launcher;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.axon.memberApp.R;

public class SliderApapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;

    // Arrays
    public int[] slide_images={R.drawable.intro, R.drawable.intro, R.drawable.intro};
    public int[] slide_strings={R.string.intro_string, R.string.intro_string, R.string.intro_string};

    public SliderApapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return slide_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView imgSlideImage = view.findViewById(R.id.imgSlideImage);
        TextView txtDescString = view.findViewById(R.id.txtDescString);

        imgSlideImage.setImageResource(slide_images[position]);
        txtDescString.setText(slide_strings[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
package com.axon.memberApp.Launcher;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;
import com.google.firebase.analytics.FirebaseAnalytics;

public class LauncherActivity extends AppCompatActivity {
    /*Duration of wait*/
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    private FirebaseAnalytics mFirebaseAnalytics;

    /*Called when the activity is first created*/
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        initFirebaseAnalytics();
        // Initial SharedPreferences
        AppCoreCode.getInstance().sharedpreferences = getSharedPreferences("MyPrefs", this.MODE_PRIVATE);
        AppCoreCode.getInstance().editor = AppCoreCode.getInstance().sharedpreferences.edit();
        setContentView(R.layout.launcher_screen);

        startTimerToChangeToAnotherScreen();
    }

    void initFirebaseAnalytics() {
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    void startTimerToChangeToAnotherScreen() {
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                try {

                    Intent mainIntent;
                    if (AppCoreCode.getInstance().sharedpreferences != null && AppCoreCode.getInstance().getBooleanValuesFromSharedPreferences("IntroScreenIsShowedBefore")) {
                        mainIntent = new Intent(LauncherActivity.this, LoginActivity.class); //
                    } else {
                        mainIntent = new Intent(LauncherActivity.this, PaperOnboardingActivity.class);
                        if (AppCoreCode.getInstance().editor != null)
                            AppCoreCode.getInstance().saveValuesToSharedPreferences("IntroScreenIsShowedBefore", true);
                    }
                    startActivity(mainIntent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}

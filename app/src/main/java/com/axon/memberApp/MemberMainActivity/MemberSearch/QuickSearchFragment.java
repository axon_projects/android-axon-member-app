package com.axon.memberApp.MemberMainActivity.MemberSearch;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.AdvancedSearch;
import com.axon.memberApp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuickSearchFragment extends Fragment {
    private RecyclerView recyclerViewSearchResults;
    private RecyclerView.Adapter adapter;
    private View view;

    private ImageButton ibtnBack; //, ibtnTopEditSearch;
//    private EditText etxtSearch;
    private String TAG = "QuickSearchFragment";

    private ProgressBar progresQuickSearchAddToList;
    private ProgressBar progressBarQuickSearch;
    private int pageNumber;
    private int totalPages;
    private String searchResultsResponse;
    private AsyncTaskClass asyncTaskClass;
    private String quickSearchResponseResults;


    private AsyncTaskQuickSearchClass asyncTaskQuickSearchClass;
    private String quickSearchResponse;
    private AsyncTaskProviderProfileClass asyncTaskProviderProfileClass;
    private MyAdapter.FavoriteAsyncTaskClass favoriteAsyncTaskClass;
    private boolean success;
    private String favoriteResponse;

    private String providerProfileResponse;

    public QuickSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.quick_search_fragment, container, false);

//        initViews();

        return view;
    }

    private void initViews() {
        progresQuickSearchAddToList = view.findViewById(R.id.progresQuickSearchAddToList);
        progressBarQuickSearch = view.findViewById(R.id.progressBarQuickSearch);
        ibtnBack = view.findViewById(R.id.ibtnBack);
//        ibtnTopEditSearch = view.findViewById(R.id.ibtnTopEditSearch);
//        etxtSearch = view.findViewById(R.id.etxtSearch);

        recyclerViewSearchResults = view.findViewById(R.id.recyclerViewSearchResults);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewSearchResults.setHasFixedSize(true);
        recyclerViewSearchResults.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));

//        if (asyncTaskQuickSearchClass == null) {
            getQuickSearchData();
//        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null && isAdded() && view != null) {
            Tools.hideKeyboard(getActivity(), view);
            initViews();
        }
    }

    private void setViewsActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
            }
        });

//        ibtnTopEditSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showCustomEditSearchDialog();
//            }
//        });
    }

    private void getProviderProfileFromServer() {
        asyncTaskProviderProfileClass = new AsyncTaskProviderProfileClass();
        asyncTaskProviderProfileClass.execute();
    }

    private void getQuickSearchData() {
        asyncTaskQuickSearchClass = new AsyncTaskQuickSearchClass();
        asyncTaskQuickSearchClass.execute();
    }

    private class AsyncTaskQuickSearchClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().cache.isSearchLoading = true;
            progressBarQuickSearch.setVisibility(View.VISIBLE);
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                AppCoreCode.getInstance().cache.quickSearchJsonToSend = "{\n" +
                        "\t\"providerSystemType\": null,\n" +
                        "\t\"providerName\": " + AppCoreCode.getInstance().cache.strQuickSearchProviderName + ",\n" +
                        "\t\"specialityId\": null,\n" +
                        "\t\"serviceId\": null,\n" +
                        "\t\"cityId\": null,\n" +
                        "\t\"areaId\": null,\n" +
                        "\t\"workingFrom\": null,\n" +
                        "\t\"workingTo\": null,\n" +
                        "\t\n";

                quickSearchResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getprovidersbysearch"
                        , AppCoreCode.getInstance().cache.quickSearchJsonToSend +
                                "    \"pageNumber\": 1,\n" +
                                "    \"pageSize\": 5\n" +
                                "}");
                Log.d(TAG, "doInBackground() --> quickSearchResponse: " + quickSearchResponse);
                JSONObject reader = new JSONObject(quickSearchResponse);
                AppCoreCode.getInstance().cache.advancedSearch = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), AdvancedSearch.class);

                String itemsArray = reader.getJSONObject("result").getJSONArray("items").toString();
                // TODO: Need to change List to Map
                AppCoreCode.getInstance().cache.advancedSearch.itemsList = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<AdvancedSearch.Items>>() {
                }.getType());
                Log.d(TAG, "doInBackground() --> itemsList size: " + AppCoreCode.getInstance().cache.advancedSearch.itemsList.size());

            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pageNumber = 1;
            if (AppCoreCode.getInstance().cache.advancedSearch != null)
                totalPages = AppCoreCode.getInstance().cache.advancedSearch.totalPages;
            progressBarQuickSearch.setVisibility(View.INVISIBLE);
            loadRecyclerViewDate();
            setViewsActions();
            AppCoreCode.getInstance().cache.isSearchLoading = false;
        }
    }

    void loadRecyclerViewDate() {
        adapter = new MyAdapter();
        recyclerViewSearchResults.setAdapter(adapter);
    }

    private void showCustomEditSearchDialog() {
        final Dialog customEditSearchDialog = new Dialog(getActivity());
        customEditSearchDialog.setContentView(R.layout.edit_search_results_custom_dialog);
        customEditSearchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageButton ibtnClose = customEditSearchDialog.findViewById(R.id.ibtnClose);
        Button ibtnDone = customEditSearchDialog.findViewById(R.id.ibtnDone);

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customEditSearchDialog.dismiss();
            }
        });

        ibtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customEditSearchDialog.dismiss();
            }
        });

        customEditSearchDialog.show();
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quick_search_result_card_layout, viewGroup, false);
            return new MyAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);

            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row position: " + position);
            final AdvancedSearch.Items currentItem = AppCoreCode.getInstance().cache.advancedSearch.itemsList.get(position);

            RatingBar ratingBarProviderRate = holder.itemView.findViewById(R.id.ratingBarProviderRate);
            Button btnAdvancedSearchResultBookNow = holder.itemView.findViewById(R.id.btnAdvancedSearchResultBookNow);
            final ImageButton btnAdvancedSearchResultFavorite = holder.itemView.findViewById(R.id.btnAdvancedSearchResultFavorite);
            LinearLayout lloBookNow = holder.itemView.findViewById(R.id.lloBookNow);
            final CircleImageView imgAdvancedSearchProviderAvater = holder.itemView.findViewById(R.id.imgAdvancedSearchProviderAvater);
            TextView txtAdvancedSearchDrName = holder.itemView.findViewById(R.id.txtAdvancedSearchDrName);
            TextView txtAdvancedSearchProviderTypeName = holder.itemView.findViewById(R.id.txtAdvancedSearchProviderTypeName);
            TextView txtAdvancedSearchSpecialization = holder.itemView.findViewById(R.id.txtAdvancedSearchSpecialization);

            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> AdvancedSearch rate: " + currentItem.rate);
            ratingBarProviderRate.setRating(currentItem.rate);

            if (!currentItem.avatar.contains("nophoto-slider")) {
                Glide.with(getApplicationContext())
                        .load(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.avatar)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .override(100, 100)
                        .into(imgAdvancedSearchProviderAvater);
            }

            if (currentItem.isInFavorite) {
                btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_active_favorite);
                btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_active_favorite);
            } else {
                btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_inactive_favorite);
                btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_inactive_favorite);
            }

            btnAdvancedSearchResultFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((int) btnAdvancedSearchResultFavorite.getTag() == R.drawable.quick_search_result_active_favorite) {
                        btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_inactive_favorite);
                        btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_inactive_favorite);
                    } else {
                        btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_active_favorite);
                        btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_active_favorite);
                    }

                    AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                    setUserFavorite();
                }
            });

            txtAdvancedSearchDrName.setText(currentItem.providerName);
            Tools.makeTextMarquee(txtAdvancedSearchDrName);
            txtAdvancedSearchProviderTypeName.setText(currentItem.providerTypeName);
            txtAdvancedSearchSpecialization.setText(currentItem.specializationName);

            if (currentItem.systemProviderType == 0) {
                btnAdvancedSearchResultBookNow.setText(getResources().getString(R.string.member_main_view_quick_search_screen_book_now_button));
            } else {
                btnAdvancedSearchResultBookNow.setText(getResources().getString(R.string.member_main_view_quick_search_screen_more_detail_button));
            }

            btnAdvancedSearchResultBookNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                    AppCoreCode.getInstance().cache.currentSelectedProviderTypeFromSearchResults = currentItem.providerTypeName;
                    getProviderProfileFromServer();
                }
            });

            lloBookNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                    AppCoreCode.getInstance().cache.currentSelectedProviderTypeFromSearchResults = currentItem.providerTypeName;
                    getProviderProfileFromServer();
                }
            });

            if (AppCoreCode.getInstance().cache.advancedSearch != null && AppCoreCode.getInstance().cache.advancedSearch.itemsList.size() - 1 == position && pageNumber < totalPages) {
                getAdvancedResultFromServer();
                progresQuickSearchAddToList.setVisibility(View.VISIBLE);
            }
        }

        private void setUserFavorite() {
            favoriteAsyncTaskClass = new FavoriteAsyncTaskClass();
            favoriteAsyncTaskClass.execute();
        }

        private class FavoriteAsyncTaskClass extends AsyncTask<String, String, String> {
            @Override
            protected void onPreExecute() {
                AppCoreCode.getInstance().httpRest = new HTTPRest();
            }

            @Override
            protected String doInBackground(String... strings) {
                try {
                    favoriteResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/favoriteupsert", "{\"id\": " + AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults + "}");

                    Log.d(TAG, "doInBackground() --> searchResultsResponse: " + favoriteResponse);
                    JSONObject reader = new JSONObject(favoriteResponse);
                    success = reader.getBoolean("success");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                // https://stackoverflow.com/questions/30220771/recyclerview-inconsistency-detected-invalid-item-position
            }
        }

        private void getAdvancedResultFromServer() {
            asyncTaskClass = new AsyncTaskClass();
            asyncTaskClass.execute();
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public int getItemCount() {
            // TODO: NPE here cause Server delay
            if (AppCoreCode.getInstance().cache.advancedSearch != null && AppCoreCode.getInstance().cache.advancedSearch.itemsList != null) {
                return AppCoreCode.getInstance().cache.advancedSearch.itemsList.size();
            } else {
                return 0;
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            if (pageNumber < AppCoreCode.getInstance().cache.advancedSearch.totalPages) {
                recyclerViewSearchResults.addOnItemTouchListener(disabler);
                ibtnBack.setEnabled(false);
            }
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                AdvancedSearch advancedSearch = null;
                if (AppCoreCode.getInstance().cache.advancedSearch != null && pageNumber < totalPages) {
                    pageNumber = pageNumber + 1;
                    advancedSearch = AppCoreCode.getInstance().cache.advancedSearch;
                }
                quickSearchResponseResults = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getprovidersbysearch"
                        , AppCoreCode.getInstance().cache.quickSearchJsonToSend +
                                "    \"pageNumber\": " + pageNumber + ",\n" +
                                "    \"pageSize\": 5\n" +
                                "}");
                Log.d(TAG, "doInBackground() --> quickSearchResponse: " + quickSearchResponseResults);
                JSONObject reader = new JSONObject(quickSearchResponseResults);
                AppCoreCode.getInstance().cache.advancedSearch = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), AdvancedSearch.class);

                String itemsArray = reader.getJSONObject("result").getJSONArray("items").toString();
                // TODO: Need to change List to Map
                AppCoreCode.getInstance().cache.advancedSearch.itemsList = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<AdvancedSearch.Items>>() {
                }.getType());

                if (advancedSearch != null) {
                    advancedSearch.itemsList.addAll(AppCoreCode.getInstance().cache.advancedSearch.itemsList);
                    AppCoreCode.getInstance().cache.advancedSearch.itemsList = advancedSearch.itemsList;
                }

                totalPages = AppCoreCode.getInstance().cache.advancedSearch.totalPages;

                Log.d(TAG, "doInBackground() --> itemsList size: " + AppCoreCode.getInstance().cache.advancedSearch.itemsList.size());
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            recyclerViewSearchResults.getAdapter().notifyDataSetChanged();
            recyclerViewSearchResults.removeOnItemTouchListener(disabler);
            ibtnBack.setEnabled(true);
            progresQuickSearchAddToList.setVisibility(View.GONE);
        }
    }

    RecyclerView.OnItemTouchListener disabler = new RecyclerViewDisabler();

    public class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private class AsyncTaskProviderProfileClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                providerProfileResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getproviderinfo"
                        , "{\"id\": " + AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults + "}");

                Log.d(TAG, "doInBackground() --> providerProfileResponse: " + providerProfileResponse);
                JSONObject reader = new JSONObject(providerProfileResponse);
                AppCoreCode.getInstance().cache.providerProfile = AppCoreCode.getInstance().cache.gson.fromJson(providerProfileResponse, ProviderProfile.class);
                Log.d(TAG, "doInBackground() --> providerProfile: " + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData);

                String providerProfileData = reader.getJSONObject("result").getJSONObject("providerProfileData").toString();
                Log.d(TAG, "doInBackground() --> providerProfileData: " + providerProfileData);
                // TODO: Checking for null cause Network delay
                if (AppCoreCode.getInstance().cache.providerProfile != null && AppCoreCode.getInstance().cache.providerProfile.result != null) {
                    AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData = AppCoreCode.getInstance().cache.gson.fromJson(providerProfileData, ProviderProfile.ProviderProfileData.class);

                    Log.d(TAG, "doInBackground() --> mediaFiles: " + AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles);

                    String providerServices = reader.getJSONObject("result").getJSONArray("providerServices").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList = AppCoreCode.getInstance().cache.gson.fromJson(providerServices, new TypeToken<List<ProviderProfile.ProviderServices>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerServicesList: " + AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList);

                    String providerOffers = reader.getJSONObject("result").getJSONArray("providerOffers").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList = AppCoreCode.getInstance().cache.gson.fromJson(providerOffers, new TypeToken<List<ProviderProfile.ProviderOffers>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerOffers: " + AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList);

                    String providerSpecializations = reader.getJSONObject("result").getJSONArray("providerSpecializations").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerSpecializationsList = AppCoreCode.getInstance().cache.gson.fromJson(providerSpecializations, new TypeToken<List<ProviderProfile.ProviderSpecializations>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerSpecializations: " + AppCoreCode.getInstance().cache.providerProfile.result.providerSpecializationsList);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO: Checking for null cause Network delay
            if (AppCoreCode.getInstance().cache.providerProfile != null
                    && AppCoreCode.getInstance().cache.providerProfile.result != null
                    && AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData != null) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(15);
            }
        }
    }
}

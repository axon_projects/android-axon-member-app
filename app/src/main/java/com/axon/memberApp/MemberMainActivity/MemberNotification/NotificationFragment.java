package com.axon.memberApp.MemberMainActivity.MemberNotification;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberNotification.Classes.Notification;
import com.axon.memberApp.R;

import org.json.JSONObject;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {
    private RecyclerView recyclerViewNotificationResults;
    private RecyclerView.Adapter adapter;
    private View view;

    private String TAG = "NotificationFragment";
    private String notificationResponse;
    private String readNotificationType;
    private String currentNotificationID;
    private AsyncTaskClass asyncTaskClass;
    private ReloadNotificationAsyncTaskClass reloadNotificationAsyncTaskClass;
    private boolean isNotificationResponseSucceeded;

    private ImageButton ibtnNotificationBack, ibtnNotificationReadAll;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.notification_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        ibtnNotificationBack = view.findViewById(R.id.ibtnNotificationBack);
        ibtnNotificationReadAll = view.findViewById(R.id.ibtnNotificationReadAll);

        recyclerViewNotificationResults = view.findViewById(R.id.recyclerViewNotificationResults);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewNotificationResults.setHasFixedSize(true);
        recyclerViewNotificationResults.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        loadRecyclerViewDate();
        setViewsActions();
    }

    private void setViewsActions() {
        ibtnNotificationBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
            }
        });
        ibtnNotificationReadAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readNotifications("All");
            }
        });
    }

    private void readNotifications(String readNotificationType) {
        this.readNotificationType = readNotificationType;
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    void loadRecyclerViewDate() {
        adapter = new MyAdapter();
        recyclerViewNotificationResults.setAdapter(adapter);
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_row_custom_layout, viewGroup, false);
            return new MyAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);
            final Notification.Result currentNotification = AppCoreCode.getInstance().cache.notification.result.get(position);

            ImageView imgNotificationImage = holder.itemView.findViewById(R.id.imgNotificationImage);
            TextView txtNotificationText = holder.itemView.findViewById(R.id.txtNotificationText);
            TextView txtNotificationSeverityName = holder.itemView.findViewById(R.id.txtNotificationSeverityName);
            TextView txtNotificationMessage = holder.itemView.findViewById(R.id.txtNotificationMessage);
            ImageView imgNotificationState = holder.itemView.findViewById(R.id.imgNotificationState);
            TextView txtNotificationCreationTime = holder.itemView.findViewById(R.id.txtNotificationCreationTime);

            txtNotificationText.setText(currentNotification.text);
            Tools.makeTextMarquee(txtNotificationText);
            txtNotificationSeverityName.setText(currentNotification.severityName);
            txtNotificationMessage.setText(currentNotification.message);
            txtNotificationCreationTime.setText(currentNotification.creationTime);

            if (currentNotification.state == 0) {
                imgNotificationState.setVisibility(View.VISIBLE);
            } else {
                imgNotificationState.setVisibility(View.INVISIBLE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentNotificationID = currentNotification.id;
                    if (currentNotification.state == 0) {
                        readNotifications("Single");
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            if (AppCoreCode.getInstance().cache.notification != null && AppCoreCode.getInstance().cache.notification.result != null)
                return AppCoreCode.getInstance().cache.notification.result.size();
            else
                return 0;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (readNotificationType.equalsIgnoreCase("Single")) {
                    notificationResponse = AppCoreCode.getInstance().httpRest.sendHTTPPutRequest("/api/memberapp/updatenotificationstate"
                            , "{\"id\": \" " + currentNotificationID + " \"}");
                    Log.d(TAG, "doInBackground() --> notificationResponse: " + notificationResponse);
                } else if (readNotificationType.equalsIgnoreCase("All")) {
                    notificationResponse = AppCoreCode.getInstance().httpRest.sendHTTPPutRequest("/api/memberapp/updateallnotificationstates", "");
                    Log.d(TAG, "doInBackground() --> notificationResponse: " + notificationResponse);
                }
                JSONObject reader = new JSONObject(notificationResponse);
                isNotificationResponseSucceeded = reader.getBoolean("success");
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Notification exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (isNotificationResponseSucceeded) {
                reloadNotificationAsyncTaskClass = new ReloadNotificationAsyncTaskClass();
                reloadNotificationAsyncTaskClass.execute();
            } else {
                if (getActivity() != null)
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_LONG);
            }
        }
    }

    private class ReloadNotificationAsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                notificationResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getnotifications");
                AppCoreCode.getInstance().cache.notification = AppCoreCode.getInstance().cache.gson.fromJson(notificationResponse, Notification.class);
                Log.d(TAG, "doInBackground() --> notificationResponse: " + notificationResponse);
                JSONObject reader = new JSONObject(notificationResponse);
                isNotificationResponseSucceeded = reader.getBoolean("success");
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (isNotificationResponseSucceeded) {
                adapter.notifyDataSetChanged();
                recyclerViewNotificationResults.setAdapter(adapter);
            }
        }
    }
}

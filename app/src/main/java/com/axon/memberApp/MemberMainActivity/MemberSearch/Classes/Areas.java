package com.axon.memberApp.MemberMainActivity.MemberSearch.Classes;

import java.util.List;

public class Areas {
    public  String targetUrl;
    public  boolean success;
    public  boolean unAuthorizedRequest;
    public  boolean __abp;

    public Error error = new Error();
    public List<Areas.Result> result;

    public class Result{
        public String name;
        public int id;
    }

    public class Error{
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }
}

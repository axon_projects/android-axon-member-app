package com.axon.memberApp.MemberMainActivity.CreateMemberShip;


import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.MembershipFeesInfo;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Areas;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Cities;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateMembershipFragment extends Fragment {

    private View view;
    private ImageButton ibtnBack;
    private Button btnNext, btnCreateMembershipUploadImage;
    private CircleImageView imgCreateMembershipMemberAvatar;
    private TextView txtMembershipBenefits, txtCreateMembershipCity, txtCreateMembershipArea, txtCreateMembershipAddress, txtCreateMembershipPromoCode;
    private SpinnerDialog spinnerDialog;
    private SweetAlertDialog sweetAlertDialog;

    private String citiesResponse;
    private String areasResponse;
    private String membership;
    private int areaID;

    ArrayList<String> citiesList;
    ArrayList<String> areasList;

    private Integer cityId;

    private AsyncTaskClass asyncTaskClass;
    private String typeOfServerOperation;

    private String TAG = "CreateMembershipFragment";

    public CreateMembershipFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.create_membership_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        txtCreateMembershipAddress = view.findViewById(R.id.txtCreateMembershipAddress);
        txtCreateMembershipPromoCode = view.findViewById(R.id.txtCreateMembershipPromoCode);
        txtCreateMembershipCity = view.findViewById(R.id.txtCreateMembershipCity);
        txtCreateMembershipArea = view.findViewById(R.id.txtCreateMembershipArea);
        txtCreateMembershipArea.setVisibility(View.GONE);
        imgCreateMembershipMemberAvatar = view.findViewById(R.id.imgCreateMembershipMemberAvatar);
        btnCreateMembershipUploadImage = view.findViewById(R.id.btnCreateMembershipUploadImage);
        txtMembershipBenefits = view.findViewById(R.id.txtMembershipBenefits);
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnNext = view.findViewById(R.id.btnNext);

        getDataFromAndToServer("GetData");
        setViewsActions();
    }

    private void getDataFromAndToServer(String typeOfServerOperation) {
        this.typeOfServerOperation = typeOfServerOperation;
        btnNext.setEnabled(false);
        btnNext.setAlpha((float) 0.5);
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void setViewsActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(6);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInputs();
            }
        });

        btnCreateMembershipUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 1234);

            }
        });

        txtCreateMembershipCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (citiesList != null)
                    showSpinnerDialog("Select or Search City", txtCreateMembershipCity, citiesList);
            }
        });

        txtCreateMembershipArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areasList != null)
                    showSpinnerDialog("Select or Search Area", txtCreateMembershipArea, areasList);
            }
        });

        txtMembershipBenefits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMembershipBenefitsDialog();
            }
        });
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permission)) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);

                } else {

                    ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);

                }
            } else {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1001);//one can be replaced with any action code
//            Toast.makeText(getActivity(), "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, 1001);//one can be replaced with any action code
        }
    }

    private void validateUserInputs() {
        if (imgCreateMembershipMemberAvatar.getDrawable().getConstantState() == getResources().getDrawable(R.mipmap.ic_launcher_round).getConstantState()) {
            imgCreateMembershipMemberAvatar.requestFocus();

            sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setCanceledOnTouchOutside(true);
            sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_required_image));
            sweetAlertDialog.show();

            Button btn = sweetAlertDialog.findViewById(R.id.confirm_button);
            btn.setBackgroundColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
            return;
        }
        if (txtCreateMembershipCity.getText().toString().trim().isEmpty()) {
            txtCreateMembershipCity.setError(getResources().getString(R.string.view_error_city_required));
            txtCreateMembershipCity.requestFocus();
            return;
        }
        if (txtCreateMembershipArea.getText().toString().trim().isEmpty()) {
            txtCreateMembershipArea.setError(getResources().getString(R.string.view_error_area_required));
            txtCreateMembershipArea.requestFocus();
            return;
        }
        getDataFromAndToServer("SendMembership");
    }

    private void showSpinnerDialog(final String dialogTitle, final TextView textView, ArrayList<String> list) {
        spinnerDialog = new SpinnerDialog(getActivity(), list, dialogTitle, R.style.DialogAnimations_SmileWindow, "Close");// With 	Animation

        spinnerDialog.setCancellable(true); // for cancellable
        spinnerDialog.setShowKeyboard(false);// for open keyboard by default

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                if (item.trim().length() > 0) {
                    textView.setText(item);
                    if (dialogTitle.equalsIgnoreCase("Select or Search Area")) {
                        areaID = AppCoreCode.getInstance().cache.areas.result.get(position).id;
                    } else if (dialogTitle.equalsIgnoreCase("Select or Search City") && AppCoreCode.getInstance().cache.cities != null) {
                        for (Cities.Result result : AppCoreCode.getInstance().cache.cities.result) {
                            if (txtCreateMembershipCity.getText().toString().trim().length() > 0 && txtCreateMembershipCity.getText().toString().equalsIgnoreCase(result.name)) {
                                cityId = result.id;
                                getDataFromAndToServer("GetAreas");
                                break;
                            }
                            cityId = null;
                        }
                    }
                }
            }
        });
        if (spinnerDialog != null)
            spinnerDialog.showSpinerDialog();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1001:
                if (resultCode == RESULT_OK) {
                    AppCoreCode.getInstance().cache.selectedImage = data.getData();
                    Log.d(TAG, "onActivityResult --> resultCode: " + requestCode + " - selectedImage: " + AppCoreCode.getInstance().cache.selectedImage);
                    if (imgCreateMembershipMemberAvatar != null)
                        imgCreateMembershipMemberAvatar.setImageURI(AppCoreCode.getInstance().cache.selectedImage);
                }
        }
    }

    private void showCustomMembershipBenefitsDialog() {
        final Dialog customMembershipBenefitsDialog = new Dialog(getActivity());
        customMembershipBenefitsDialog.setContentView(R.layout.membership_benefits_custom_dialog);
        customMembershipBenefitsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageButton ibtnClose = customMembershipBenefitsDialog.findViewById(R.id.ibtnClose);

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customMembershipBenefitsDialog.dismiss();
            }
        });
        customMembershipBenefitsDialog.show();
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfServerOperation.equalsIgnoreCase("GetData")) {
                    citiesResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/cities"
                            , "{\"countryId\": 1}");
                    Log.d(TAG, "doInBackground() --> citiesResponse: " + citiesResponse);
                    AppCoreCode.getInstance().cache.cities = AppCoreCode.getInstance().cache.gson.fromJson(citiesResponse, Cities.class);
                    JSONObject citiesReader = new JSONObject(citiesResponse);
                    String citiesItemsArray = citiesReader.get("result").toString();
                    Log.d(TAG, "doInBackground() --> citiesReader.getJSONArray: " + citiesItemsArray);
                    AppCoreCode.getInstance().cache.cities.result = AppCoreCode.getInstance().cache.gson.fromJson(citiesItemsArray, new TypeToken<List<Cities.Result>>() {
                    }.getType());
                } else if (typeOfServerOperation.equalsIgnoreCase("GetAreas")) {
                    areasResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/areas"
                            , "{\"cityId\": " + cityId + "}");
                    Log.d(TAG, "doInBackground() --> areasResponse: " + areasResponse);
                    AppCoreCode.getInstance().cache.areas = AppCoreCode.getInstance().cache.gson.fromJson(areasResponse, Areas.class);
                    JSONObject areasReader = new JSONObject(areasResponse);
                    String areasItemsArray = areasReader.getJSONArray("result").toString();
                    AppCoreCode.getInstance().cache.areas.result = AppCoreCode.getInstance().cache.gson.fromJson(areasItemsArray, new TypeToken<List<Areas.Result>>() {
                    }.getType());
                } else {
                    AppCoreCode.getInstance().cache.membershipJsonToServer = "{\"address\": \"" + txtCreateMembershipAddress.getText().toString() + "\",\"areaId\": " + areaID + ",\"promoCode\": \"" + txtCreateMembershipPromoCode.getText().toString() + "\"}";
                    membership = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getmembershipfeesinfo"
                            , AppCoreCode.getInstance().cache.membershipJsonToServer);

                    Log.d(TAG, "doInBackground() --> membership fees: " + membership);
                    JSONObject reader = new JSONObject(membership);
                    AppCoreCode.getInstance().cache.membershipFeesInfo = AppCoreCode.getInstance().cache.gson.fromJson(reader.toString(), MembershipFeesInfo.class);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (typeOfServerOperation.equalsIgnoreCase("GetData")) {
                citiesList = new ArrayList();
                citiesList.add("");
                if (AppCoreCode.getInstance().cache.cities != null)
                    for (Cities.Result result : AppCoreCode.getInstance().cache.cities.result) {
                        citiesList.add(result.name);
                    }


            } else if (typeOfServerOperation.equalsIgnoreCase("GetAreas")) {
                areasList = new ArrayList();
                areasList.add("");
                for (Areas.Result result : AppCoreCode.getInstance().cache.areas.result) {
                    areasList.add(result.name);
                }
                txtCreateMembershipArea.setVisibility(View.VISIBLE);
            } else {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(10);
            }
            btnNext.setEnabled(true);
            btnNext.setAlpha((float) 1);
        }
    }
}

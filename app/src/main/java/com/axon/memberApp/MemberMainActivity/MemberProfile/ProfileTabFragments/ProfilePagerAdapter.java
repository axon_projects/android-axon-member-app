package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ProfilePagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> _fragments;
    int numberOfTabs;
    public ProfilePagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
        this._fragments = new ArrayList<Fragment>();
    }

    public void add(Fragment fragment) {
        this._fragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                NextAppointmentsFragment nextAppointmentsFragment = new NextAppointmentsFragment();
                return nextAppointmentsFragment;
            case 1:
                PreAppointmentsFragment preAppointmentsFragment = new PreAppointmentsFragment();
                return preAppointmentsFragment;
            case 2:
                FavoriteFragment favoriteFragment = new FavoriteFragment();
                return favoriteFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}

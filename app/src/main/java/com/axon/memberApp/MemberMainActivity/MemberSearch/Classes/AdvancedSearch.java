package com.axon.memberApp.MemberMainActivity.MemberSearch.Classes;

import java.util.List;

public class AdvancedSearch {
    public int currentPage;
    public int pageSize;
    public int totalPages;
    public int totalCount;

    public List<AdvancedSearch.Items> itemsList;

    public class Items {
        public int providerId;
        public String providerName;
        public String description;
        public String country;
        public String city;
        public String area;
        public String address;
        public double longitude;
        public double latitude;
        public String avatar;
        public float rate;
        public int systemProviderType;
        public String providerTypeName;
        public String doctorName;
        public String doctorTitleName;
        public String specializationName;
        public boolean isInFavorite;
    }
}

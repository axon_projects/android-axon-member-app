package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile;

import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.CustomViewPager;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.ProviderProfileTabAdapter;
import com.axon.memberApp.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.stfalcon.frescoimageviewer.ImageViewer;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderProfileFragment extends Fragment {
    private View view;
    private ImageButton ibtnBack;
    private TextView txtProfileProviderType, txtProfileProviderName, txtProfileProviderRate, txtProfileProviderTitle, txtProfileProviderTitleSpeciality;
    private RatingBar ratingBarProfileProviderRate;
    private SimpleDraweeView imgProfileProviderAvatar;
    private TabLayout tabLayoutProviderProfile;
    private CustomViewPager viewPagerProviderProfile;
    private ProviderProfileTabAdapter providerProfileTabAdapter;

    private int ClinicsSysProviderType = 0;
    private int ScanSysProviderType = 1;
    private int FitnessSysProviderType = 2;
    private int PhysiotherapySysProviderType = 3;
    private int LabsSysProviderType = 4;
    private int OpticsSysProviderType = 5;
    private int PharmacySysProviderType = 6;

    private String TAG = "ProviderProfileFragment";

    public ProviderProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.provider_profile_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        ibtnBack = view.findViewById(R.id.ibtnBack);
        txtProfileProviderType = view.findViewById(R.id.txtProfileProviderType);
        txtProfileProviderName = view.findViewById(R.id.txtProfileProviderName);
        txtProfileProviderRate = view.findViewById(R.id.txtProfileProviderRate);
        txtProfileProviderTitle = view.findViewById(R.id.txtProfileProviderTitle);
        txtProfileProviderTitleSpeciality = view.findViewById(R.id.txtProfileProviderTitleSpeciality);
        ratingBarProfileProviderRate = view.findViewById(R.id.ratingBarProfileProviderRate);
        imgProfileProviderAvatar = view.findViewById(R.id.imgProfileProviderAvatar);
        tabLayoutProviderProfile = view.findViewById(R.id.tabLayoutProviderProfile);
        viewPagerProviderProfile = view.findViewById(R.id.viewPagerProviderProfile);

        setViewsValues();
        setViewsActions();
    }

    private void setViewsValues() {
        if (AppCoreCode.getInstance().cache.providerProfile != null
                && AppCoreCode.getInstance().cache.providerProfile.result != null
                && AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData != null) {
            txtProfileProviderType.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerTypeName);
            if (!AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.avatar.contains("nophoto-slider")) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
//                        final Drawable drawable = Tools.LoadImageFromWebOperations(AppCoreCode.getInstance().cache.mainURL + "/" + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.avatar);
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imgProfileProviderAvatar.setImageURI(AppCoreCode.getInstance().cache.mainURL + "/" + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.avatar);
                                }
                            });
                        }
                    }
                });
            }
            txtProfileProviderName.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerName);
            txtProfileProviderRate.setText(String.valueOf(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.rate));
            ratingBarProfileProviderRate.setRating((float) AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.rate);
            txtProfileProviderTitle.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.doctorTitleName);
            txtProfileProviderTitleSpeciality.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.specializationName);

            if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == ScanSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == OpticsSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == LabsSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == FitnessSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == PharmacySysProviderType) {
                tabLayoutProviderProfile.getTabAt(1).parent.setVisibility(View.GONE);
            } else {
                tabLayoutProviderProfile.getTabAt(1).parent.setVisibility(View.VISIBLE);
            }

            initPagerAdapter();
        }
    }

    /**
     * isAdded() -> using this method, we can do whatever we want which will prevent   **java.lang.IllegalStateException: Fragment not attached to Activity** exception.
     */
    private void initPagerAdapter() {
        if (getActivity() != null && this.isAdded()) {
            // getChildFragmentManager() -> Resolve Fragment not attach again after first time
            providerProfileTabAdapter = new ProviderProfileTabAdapter(getChildFragmentManager(), tabLayoutProviderProfile.getTabCount());
            viewPagerProviderProfile.setAdapter(providerProfileTabAdapter);
            viewPagerProviderProfile.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutProviderProfile));
        }
    }

    private void setViewsActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "setViewsActions() --> getActivity() : " + (getActivity() != null) + " - " + AppCoreCode.getInstance().cache.lastFragmentIndexBeforeProfileProvider);
                if (getActivity() != null) {
                    if (viewPagerProviderProfile != null)
                        viewPagerProviderProfile.setCurrentItem(0);
                    if (AppCoreCode.getInstance().cache.lastFragmentIndexBeforeProfileProvider == 5
                            || AppCoreCode.getInstance().cache.lastFragmentIndexBeforeProfileProvider == 3
                            || AppCoreCode.getInstance().cache.lastFragmentIndexBeforeProfileProvider == 1) {
//                        getActivity().onBackPressed();
                        ((MemberMainViewActivity) getActivity()).changeFragment(AppCoreCode.getInstance().cache.lastFragmentIndexBeforeProfileProvider);
                    }
                }
            }
        });

        tabLayoutProviderProfile.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerProviderProfile.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        imgProfileProviderAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] listimages = {AppCoreCode.getInstance().cache.mainURL + "/" + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.avatar};
                new ImageViewer.Builder(getActivity(), listimages)
                        .show();
            }
        });
    }
}

package com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes;

import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;

import java.util.List;

public class MembershipFeesInfo {
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public Error error = new Error();
    public Result result;

    public class Error {
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }

    public class Result {
        public int membershipFees;
        public double membershipDiscount;
        public int totalMembershipFees;
        public String membershipStartDate;
        public String membershipEndDate;
        public String cardDeliveryDate;
    }
}

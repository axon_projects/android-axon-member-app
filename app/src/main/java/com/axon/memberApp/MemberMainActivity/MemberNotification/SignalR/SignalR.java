package com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR;

import java.util.List;

public class SignalR {
    public int tenantId;
    public int userId;
    public int state;
    public String id;

    public Notification notification;

    public class Notification{
        public int tenantId;
        public String notificationName;
        public String entityType;
        public String entityTypeName;
        public String entityId;
        public int severity;
        public String creationTime;
        public String id;

        public Data data;

       public class Data{
           public String senderUserName;
           public String messageAr;
           public String messageEn;
           public String entityId;
           public String paymentReferenceDate;
           public String type;
//           List<Properties> propertiesList
       }
    }
}

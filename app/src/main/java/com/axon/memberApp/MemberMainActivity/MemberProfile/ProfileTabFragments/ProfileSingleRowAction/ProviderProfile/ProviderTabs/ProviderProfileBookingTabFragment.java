package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.BookingAppointment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderReservations;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.github.johnpersano.supertoasts.library.SuperActivityToast.create;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderProfileBookingTabFragment extends Fragment {
    private View view;
    private Button btnProviderBookingBookNow;
    private ScrollView scrollViewProfileBookingInfo;
    private TextView txtBookingSystem, txtWaitingTime, txtQueueSystem, txtBookingServiceFees, txtBookingDiscount, txtBookingTotalFees, txtBookingWaitingTimeTitle, txtBookingSelectService, txtNoAppointmentsTitle;
    private CalendarView calendarViewBooking;
    private RecyclerView recyclerViewBookingTime;
    public RecyclerView.Adapter bookingTimeAdapter;
    private SpinnerDialog spinnerDialog;
    private ProgressBar progressBarBookingTime;
    ArrayList<String> providerListOfName;

    // GridLayoutManager is a part of the RecyclerView.LayoutManager. For displaying list of data in Grid like Photo Gallery we can use GridLayoutManager.
    private GridLayoutManager gridLayoutManager;
    // Extending SnapHelper by adding a means of listening to changes in snap position
    private SnapHelper snapHelper;

    private AsyncTaskClass asyncTaskClass;
    private String typeOfRequestFromServer;
    public String reservationStartDate;

    public String currentSelectTime = "";
    public ProviderReservations.Result currentSelectItem;

    private boolean isMemberHasMembership;
    private String promoCode;

    private ProviderProfile.ProviderServices currentProviderProfile;

    private SweetAlertDialog sweetAlertDialog;
    private SweetAlertDialog sweetAlertProgressDialog;

    private String TAG = "ProviderProfileBookingTabFragment";

    public ProviderProfileBookingTabFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.provider_profile_booking_tab_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            AppCoreCode.getInstance().cache.isBookingTabFragmentIsVisible = true;
        } else {
            AppCoreCode.getInstance().cache.isBookingTabFragmentIsVisible = false;
        }
        Log.d(TAG, "isBookingTabFragmentIsVisible" + AppCoreCode.getInstance().cache.isBookingTabFragmentIsVisible);
    }

    private void initViews() {
        btnProviderBookingBookNow = view.findViewById(R.id.btnProviderBookingBookNow);
        btnProviderBookingBookNow.setVisibility(View.GONE);

        txtBookingSystem = view.findViewById(R.id.txtBookingSystem);
        scrollViewProfileBookingInfo = view.findViewById(R.id.scrollViewProfileBookingInfo);
        progressBarBookingTime = view.findViewById(R.id.progressBarBookingTime);
        progressBarBookingTime.setVisibility(View.GONE);
        calendarViewBooking = view.findViewById(R.id.calendarViewBooking);
        calendarViewBooking.setDate(System.currentTimeMillis(), true, true);
        txtWaitingTime = view.findViewById(R.id.txtWaitingTime);
        txtQueueSystem = view.findViewById(R.id.txtQueueSystem);
        txtBookingServiceFees = view.findViewById(R.id.txtBookingServiceFees);
        txtBookingDiscount = view.findViewById(R.id.txtBookingDiscount);
        txtBookingTotalFees = view.findViewById(R.id.txtBookingTotalFees);
        txtBookingWaitingTimeTitle = view.findViewById(R.id.txtBookingWaitingTimeTitle);
        txtBookingSelectService = view.findViewById(R.id.txtBookingSelectService);
        recyclerViewBookingTime = view.findViewById(R.id.recyclerViewBookingTime);
        txtNoAppointmentsTitle = view.findViewById(R.id.txtNoAppointmentsTitle);
        txtNoAppointmentsTitle.setVisibility(View.GONE);

        providerListOfName = new ArrayList<>();

        snapHelper = new LinearSnapHelper();

        setViewsValues();
        setViewsActions();
    }

    private void setViewsActions() {
        btnProviderBookingBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtBookingSelectService.getText().toString().trim().isEmpty()) {
                    scrollViewProfileBookingInfo.scrollTo(txtBookingSelectService.getScrollX(), (int) txtBookingSelectService.getY());
                    txtBookingSelectService.requestFocus();
                    txtBookingSelectService.setError(getResources().getString(R.string.view_error_choose_service));
                    return;
                }
                // TODO: Validate chosen time
                if (currentSelectTime.isEmpty() && currentSelectTime.trim().equals("")) {
                    sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                    sweetAlertDialog.setCanceledOnTouchOutside(true);
                    sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_select_time));
                    sweetAlertDialog.show();

                    Button btn = sweetAlertDialog.findViewById(R.id.confirm_button);
                    btn.setBackgroundColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
                    return;
                }

                if (AppCoreCode.getInstance().cache.memberProfile.memberShip == null) {
                    isMemberHasMembership = false;
                    showCustomButMembershipDialog();
                } else if (AppCoreCode.getInstance().cache.memberProfile.memberShip.membershipStatus == 1) {
                    // TODO: request data from here
                    promoCode = null;
                    getProviderReservationFromServer("Booking");
                } else {
                    isMemberHasMembership = true;
                    showCustomButMembershipDialog();
                }
            }
        });

        txtBookingSelectService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog("Select Service", txtBookingSelectService, providerListOfName);
            }
        });

        calendarViewBooking.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                String strMonth = String.valueOf(month + 1);
                String strDays = String.valueOf(dayOfMonth);
                if ((month + 1) < 10) {
                    strMonth = "0" + (month + 1);
                }
                if (dayOfMonth < 10) {
                    strDays = "0" + dayOfMonth;
                }
                reservationStartDate = year + "-" + strMonth + "-" + strDays;
                Log.d(TAG, "Calendar Time: " + reservationStartDate);
                getProviderReservationFromServer("GetTime");
            }
        });
    }

    public void getProviderReservationFromServer(String typeOfRequestFromServer) {
        this.typeOfRequestFromServer = typeOfRequestFromServer;
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void showSpinnerDialog(String dialogTitle, final TextView textView, ArrayList<String> list) {
        spinnerDialog = new SpinnerDialog(getActivity(), list, dialogTitle, R.style.DialogAnimations_SmileWindow, "Close");// With 	Animation

        spinnerDialog.setCancellable(true); // for cancellable
        spinnerDialog.setShowKeyboard(false);// for open keyboard by default

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                textView.setText(item);
                currentProviderProfile = AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.get(position);
                txtBookingServiceFees.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.get(position).price);
                txtBookingDiscount.setText("-"+ getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.get(position).discount);
                txtBookingTotalFees.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.get(position).priceAfterDiscount);
                txtBookingSelectService.setError(null);
            }
        });

        spinnerDialog.showSpinerDialog();
    }

    private void setViewsValues() {
        if (AppCoreCode.getInstance().cache.providerProfile != null
                && AppCoreCode.getInstance().cache.providerProfile.result != null
                && AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData != null) {

            txtBookingSystem.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.bookingSystem);
            if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.bookingSystem.equalsIgnoreCase("Working Hours")) {
                txtBookingWaitingTimeTitle.setText(getResources().getString(R.string.provider_watining_time));
            } else {
                txtBookingWaitingTimeTitle.setText(getResources().getString(R.string.provider_appointment_time));
            }
            txtWaitingTime.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.waitingTime);
            txtQueueSystem.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.bookingWay);
            for (ProviderProfile.ProviderServices providerService : AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList) {
                providerListOfName.add(providerService.name);
            }
            // Get calendar date and send request to server
            SimpleDateFormat ss = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(calendarViewBooking.getDate());
            String currentdate = ss.format(date);
            reservationStartDate = currentdate;
            Log.d(TAG, "setViewsValues() --> Calendar init Time: " + reservationStartDate);
            getProviderReservationFromServer("GetTime");
        }
    }

    private void showCustomButMembershipDialog() {
        final Dialog customButMembershipDialog = new Dialog(getActivity());
        customButMembershipDialog.setContentView(R.layout.buy_membership_or_promo_code_dialog);
        customButMembershipDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final ImageButton ibtnClose = customButMembershipDialog.findViewById(R.id.ibtnClose);
        final Button btnCustomBuyMemberDialog = customButMembershipDialog.findViewById(R.id.btnCustomBuyMemberDialog);
        Button btnCustomConfirmDialog = customButMembershipDialog.findViewById(R.id.btnCustomConfirmDialog);
        final TextView txtCustomPromoCodeDialog = customButMembershipDialog.findViewById(R.id.txtCustomPromoCodeDialog);
        LinearLayout lloBookingMembershipLayout = customButMembershipDialog.findViewById(R.id.lloBookingMembershipLayout);
        LinearLayout lloBookingPromoCodeLayout = customButMembershipDialog.findViewById(R.id.lloBookingPromoCodeLayout);

        if (isMemberHasMembership) {
            lloBookingMembershipLayout.setVisibility(View.GONE);
        } else {
            lloBookingMembershipLayout.setVisibility(View.VISIBLE);
        }

        btnCustomConfirmDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCustomPromoCodeDialog.getText().toString().trim().isEmpty() && txtCustomPromoCodeDialog.getText().toString().trim().length() == 0) {
                    txtCustomPromoCodeDialog.requestFocus();
                    txtCustomPromoCodeDialog.setError(getResources().getString(R.string.view_error_promo_code_required));
                    return;
                }
                promoCode = txtCustomPromoCodeDialog.getText().toString().trim();
                // TODO: request data from here
                getProviderReservationFromServer("Booking");
            }
        });

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customButMembershipDialog.dismiss();
            }
        });

        btnCustomBuyMemberDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customButMembershipDialog.dismiss();
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(9);
            }
        });

        customButMembershipDialog.show();
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            if (typeOfRequestFromServer.equalsIgnoreCase("GetTime")) {
                progressBarBookingTime.setVisibility(View.VISIBLE);
            } else {
                showProgressDialog();
            }
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfRequestFromServer.equalsIgnoreCase("GetTime") || typeOfRequestFromServer.equalsIgnoreCase("GetTimeFromSignalR")) {
                    String providerReservation = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getproviderreservationtime"
                            , "{\"axProviderId\": " + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerId + ",\"startDate\": \" " + reservationStartDate + " \"}");

                    Log.d(TAG, "doInBackground() --> providerReservation: " + providerReservation);
                    JSONObject reader = new JSONObject(providerReservation);
                    AppCoreCode.getInstance().cache.providerReservations = null;
                    AppCoreCode.getInstance().cache.providerReservations = AppCoreCode.getInstance().cache.gson.fromJson(providerReservation, ProviderReservations.class);

                    if (reader != null && !reader.isNull("result")) {
                        String providerResrv = reader.getJSONArray("result").toString();
                        AppCoreCode.getInstance().cache.providerReservations.result = AppCoreCode.getInstance().cache.gson.fromJson(providerResrv, new TypeToken<List<ProviderReservations.Result>>() {
                        }.getType());
                    }
                } else {
                    String bookingResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/bookappointment"
                            , "{\n" +
                                    "\t\"providerId\": " + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerId + ",\n" +
                                    "\t\"appointmentDate\": \"" + reservationStartDate + "\",\n" +
                                    "\t\"timeFrom\": \"" + currentSelectItem.dateFrom + "\",\n" +
                                    "\t\"timeTo\": \"" + currentSelectItem.dateTo + "\",\n" +
                                    "\t\"promoCode\": " + promoCode + ",\n" +
                                    "\t\"appointmentServices\": [\n" +
                                    "\t\t{\n" +
                                    "\t\t\t\"price\": " + currentProviderProfile.price + ",\n" +
                                    "\t\t\t\"discountPercentage\": " + currentProviderProfile.discount + ",\n" +
                                    "\t\t\t\"priceAfterDiscount\": " + currentProviderProfile.priceAfterDiscount + ",\n" +
                                    "\t\t\t\"specializationDoctorServiceId\": " + currentProviderProfile.id + "\n" +
                                    "\t\t}\n" +
                                    "\t\t]\n" +
                                    "}");

                    Log.d(TAG, "doInBackground() --> Booking response: " + bookingResponse);
                    AppCoreCode.getInstance().cache.bookingAppointment = AppCoreCode.getInstance().cache.gson.fromJson(bookingResponse, BookingAppointment.class);
                    Log.d(TAG, "doInBackground() --> bookingAppointment.success: " + AppCoreCode.getInstance().cache.bookingAppointment.success);
                    Log.d(TAG, "doInBackground() --> bookingAppointment.success: " + AppCoreCode.getInstance().cache.bookingAppointment.error.message);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (typeOfRequestFromServer.equalsIgnoreCase("GetTime") || typeOfRequestFromServer.equalsIgnoreCase("GetTimeFromSignalR")) {
                progressBarBookingTime.setVisibility(View.GONE);
                if (AppCoreCode.getInstance().cache.providerReservations != null && AppCoreCode.getInstance().cache.providerReservations.result != null) {
                    loadRecyclerViewDate();
                    if (AppCoreCode.getInstance().cache.providerReservations.result != null && AppCoreCode.getInstance().cache.providerReservations.result.size() == 0) {
                        txtNoAppointmentsTitle.setVisibility(View.VISIBLE);
                        btnProviderBookingBookNow.setVisibility(View.GONE);
                    } else {
                        txtNoAppointmentsTitle.setVisibility(View.GONE);
                        btnProviderBookingBookNow.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (sweetAlertProgressDialog != null) {
                    sweetAlertProgressDialog.dismissWithAnimation();
                }
                if (AppCoreCode.getInstance().cache.bookingAppointment != null) {
                    if (getActivity() != null) {
                        if (AppCoreCode.getInstance().cache.bookingAppointment.success && AppCoreCode.getInstance().cache.bookingAppointment.result != 0) {
                            showSuccessMessage("Booking created successfully");
                        } else {
                            if (AppCoreCode.getInstance().cache.bookingAppointment.error != null)
                                showErrorMessage(AppCoreCode.getInstance().cache.bookingAppointment.error.message);
                        }
                    }
                }
            }
        }
    }

    private void showSuccessMessage(String msg) {
        if (getActivity() != null) {
            sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_done))
                    .setContentText(msg)
                    .show();

            Button btn = sweetAlertDialog.findViewById(R.id.confirm_button); // confirm_button
            btn.setBackgroundColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
        }
    }

    private void showErrorMessage(String msg) {
        if (getActivity() != null) {
            sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_sorry));
            sweetAlertDialog.setContentText(msg);
            sweetAlertDialog.show();

            Button btn1 = sweetAlertDialog.findViewById(R.id.confirm_button); // confirm_button
            btn1.setBackgroundColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
        }
    }

    private void showProgressDialog() {
        if (getActivity() != null) {
            sweetAlertProgressDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            sweetAlertProgressDialog.getProgressHelper().setBarColor(R.color.axonTextDarkPurpleColor);
            sweetAlertProgressDialog.setTitleText(getResources().getString(R.string.sweet_alert_loading));
            sweetAlertProgressDialog.setCanceledOnTouchOutside(true);
            sweetAlertProgressDialog.setCancelable(true);
            sweetAlertProgressDialog.show();
        }
    }

    private void loadRecyclerViewDate() {
        if (AppCoreCode.getInstance().cache.providerReservations != null
                && AppCoreCode.getInstance().cache.providerReservations.result != null
                && AppCoreCode.getInstance().cache.providerReservations.result.size() != 0
                && AppCoreCode.getInstance().cache.providerReservations.result.get(0).bookingSystem == 0) {
            // Simple GridLayoutManager that spans three columns
            gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
        } else {
            gridLayoutManager = new GridLayoutManager(getActivity(), 4, GridLayoutManager.VERTICAL, false);
        }

        snapHelper.attachToRecyclerView(recyclerViewBookingTime);

        recyclerViewBookingTime.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerViewBookingTime.setLayoutManager(gridLayoutManager);

        bookingTimeAdapter = new MyServicesAdapter();
        recyclerViewBookingTime.setAdapter(bookingTimeAdapter);
    }

    class MyServicesAdapter extends RecyclerView.Adapter<MyServicesAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = null;

            if (AppCoreCode.getInstance().cache.providerReservations.result.get(0).bookingSystem == 0) {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.booking_from_to_custom_row, viewGroup, false);
            } else {
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.booking_from_custom_row, viewGroup, false);
            }
            return new MyServicesAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyServicesAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull MyServicesAdapter.ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);
            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row position: " + position);
            final ProviderReservations.Result currentItem = AppCoreCode.getInstance().cache.providerReservations.result.get(position);

            final TextView txtBookingTime = holder.itemView.findViewById(R.id.txtBookingTime);
            if (AppCoreCode.getInstance().cache.providerReservations.result.get(0).bookingSystem == 0) {
                txtBookingTime.setText(currentItem.dateFrom + " - " + currentItem.dateTo);
            } else {
                currentItem.dateTo = "";
                txtBookingTime.setText(currentItem.dateFrom);
            }

            if (currentItem.isAvailable) {
                txtBookingTime.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners); // button_colored_bg_rounded_corners
                txtBookingTime.setTextColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
                txtBookingTime.setEnabled(true);
            } else {
                txtBookingTime.setBackgroundResource(R.drawable.button_bg_rounded_dimmed_color_corners);
                txtBookingTime.setTextColor(getResources().getColor(R.color.axonGray));
                txtBookingTime.setEnabled(false);
            }
            // Change background for selected data
            if (currentItem.dateFrom.equalsIgnoreCase(currentSelectTime)) {
                txtBookingTime.setBackgroundResource(R.drawable.button_colored_bg_rounded_corners);
                txtBookingTime.setTextColor(getResources().getColor(R.color.white));
                currentSelectItem = currentItem;
                Log.d(TAG, "MyServicesAdapter --> onBindViewHolder() --> currentSelectItem date from: " + currentSelectItem.dateFrom);
            }

            txtBookingTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentSelectTime = currentItem.dateFrom;
                    recyclerViewBookingTime.getAdapter().notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return AppCoreCode.getInstance().cache.providerReservations.result.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }
}

package com.axon.memberApp.MemberMainActivity.MemberNotification.Classes;

import java.util.List;

public class Notification {
    public  String targetUrl;
    public  boolean success;
    public  boolean unAuthorizedRequest;
    public  boolean __abp;

    public Error error = new Error();
    public List<Result> result;

    public class Result{
        public String id;
        public String text;
        public String message;
        public String creationTime;
        public String severityName;
        public int state;
    }

    public class Error{
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }
}

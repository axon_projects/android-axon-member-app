package com.axon.memberApp.MemberMainActivity.MemberProfile.MemberProfileSettings;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileResetPasswordLinkFragment extends Fragment {
    private View view;
    ImageButton ibtnBack;
    Button btnDone;
    public ProfileResetPasswordLinkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.profile_reset_password_link_fragment, container, false);

        initViews();

        return view;
    }
    private void initViews(){
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnDone = view.findViewById(R.id.btnDone);
        setViewActions();
    }

    private void setViewActions(){
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                ((MemberMainViewActivity)getActivity()).changeFragment(6);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                ((MemberMainViewActivity)getActivity()).changeFragment(6);
            }
        });
    }
}

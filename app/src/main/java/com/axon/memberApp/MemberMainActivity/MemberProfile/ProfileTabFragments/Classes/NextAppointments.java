package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes;

import com.axon.memberApp.MemberMainActivity.MemberProfile.Classes.MemberProfile;

import java.util.LinkedList;
import java.util.List;

public class NextAppointments {
    public int currentPage;
    public int pageSize;
    public int totalPages;
    public int totalCount;

    public List<Items> itemsList;

    public class Items {
        public int providerId;
        public int appointmentId;
        public String providerName;
        public String docTitle;
        public String docName;
        public String providerAvatarUrl;
        public float rate;
        public int reservationId;
        public String reservationDate;
        public String timeFrom;
        public String timeTo;
        public String reservationStatus;
        public boolean isWalkIn;
        public int price;
    }
}

package com.axon.memberApp.MemberMainActivity.MemberSearch;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.AdvancedSearch;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Areas;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Cities;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Services;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Specializations;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;
import com.philliphsu.bottomsheetpickers.BottomSheetPickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.grid.GridTimePickerDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvancedSearchFragment extends Fragment implements BottomSheetTimePickerDialog.OnTimeSetListener {
    private View view;
    private ImageButton ibtnBack, ibtnSearchLogo;
    private Button btnAdvancedSearchSearch, btnAdvancedSearchType;
    private EditText etxtAdvancedSearchName;
    private TextView txtAdvancedSearchWorkingFrom, txtAdvancedSearchWorkingTo, txtAdvancedSearchSpeciality, txtAdvancedSearchService, txtAdvancedSearchCity, txtAdvancedSearchArea, txtAdvancedSearchTitle;
    private LinearLayout lloName, lloSpeciality, lloService, lloCity, lloArea, lloWorkingHours;
    private SpinnerDialog spinnerDialog;
    private ProgressBar progressBarAdvancedSearchloadingLists;

    private ArrayAdapter<Cities.Result> citiesAdapter;
    private ArrayAdapter<Specializations.Result> specializationAdapter;
    private ArrayAdapter<Areas.Result> areasAdapter;
    private ArrayAdapter<Services.Result> servicesAdapter;

    private AsyncTaskClass asyncTaskClass;
    private String citiesResponse;
    private String specializationResponse;
    private String servicesResponse;
    private String areasResponse;
    private String searchResultsResponse;
    private String typeOfServerOperation;
    private String TAG = "AdvancedSearchFragment";

    private static final int Clinics = 1;
    private static final int Hospital = 8;
    private static final int Scan = 6;
    private static final int Fitness = 3;
    private static final int Physiotherapy = 2;
    private static final int Labs = 7;
    private static final int Optics = 5;
    private static final int Pharmacy = 4;

    private Integer providerTypeId;
    private String providerName;
    private Integer specialityId;
    private Integer serviceId;
    private Integer cityId;
    private Integer areaId;

    private String currentSelectedTextView = "";
    private String strWorkingFrom;
    private String strWorkingTo;

    ArrayList<String> citiesList;
    ArrayList<String> specializationsList;
    ArrayList<String> areasList;
    ArrayList<String> servicesList;

    public AdvancedSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.advanced_search_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, "setUserVisibleHint() --> isVisibleToUser: " + isVisibleToUser + " - isAdded(): " + isAdded() + " - getActivity(): " + getActivity() + " - view: " + view);
        if (isVisibleToUser && isAdded() && getActivity() != null && view != null) {
            initViews();
        }
    }

    private void initViews() {
        ibtnBack = view.findViewById(R.id.ibtnBack); // getActivity instead of view
        ibtnSearchLogo = view.findViewById(R.id.ibtnSearchLogo);
        btnAdvancedSearchSearch = view.findViewById(R.id.btnAdvancedSearchSearch);
        btnAdvancedSearchType = view.findViewById(R.id.btnAdvancedSearchType);
        txtAdvancedSearchTitle = view.findViewById(R.id.txtAdvancedSearchTitle);

        progressBarAdvancedSearchloadingLists = view.findViewById(R.id.progressBarAdvancedSearchloadingLists);

        lloName = view.findViewById(R.id.lloName);
        lloSpeciality = view.findViewById(R.id.lloSpeciality);
        lloService = view.findViewById(R.id.lloService);
        lloCity = view.findViewById(R.id.lloCity);
        lloArea = view.findViewById(R.id.lloArea);
        lloArea.setVisibility(View.GONE);
        lloWorkingHours = view.findViewById(R.id.lloOffDays);

        etxtAdvancedSearchName = view.findViewById(R.id.etxtAdvancedSearchName);
        txtAdvancedSearchSpeciality = view.findViewById(R.id.txtAdvancedSearchSpeciality);
        txtAdvancedSearchService = view.findViewById(R.id.txtAdvancedSearchService);
        txtAdvancedSearchCity = view.findViewById(R.id.txtAdvancedSearchCity);
        txtAdvancedSearchArea = view.findViewById(R.id.txtAdvancedSearchArea);
        txtAdvancedSearchWorkingFrom = view.findViewById(R.id.txtAdvancedSearchWorkingFrom);
        txtAdvancedSearchWorkingTo = view.findViewById(R.id.txtAdvancedSearchWorkingTo);

        changeSearchLogo(AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch, null);
        getSearchSpinnerData("SearchParameters");
        setViewsActions();
    }

    public void getSearchSpinnerData(String typeOfServerOperation) {
        if (citiesList == null && specializationsList == null && areasList == null && servicesList == null) { //
            progressBarAdvancedSearchloadingLists.setVisibility(View.VISIBLE);
            btnAdvancedSearchSearch.setEnabled(false);
            btnAdvancedSearchSearch.setAlpha((float) 0.5);
        }
        this.typeOfServerOperation = typeOfServerOperation;
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    /**
     * @param viewGroup The view associated with this listener.
     * @param hourOfDay The hour that was set.
     * @param minute    The minute that was set.
     */
    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {
        Log.d(TAG, "onTimeSet() --> hourOfDay: " + hourOfDay + " - minute: " + minute);
        String strHours = String.valueOf(hourOfDay);
        int hours = 0;
        String strDayOrNight = "";
        String strMin = String.valueOf(minute);

        if (hourOfDay < 10) {
            if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
                strWorkingFrom = "0" + hourOfDay;
            } else {
                strWorkingTo = "0" + hourOfDay;
            }
            if (minute < 10) {
                if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
                    strWorkingFrom = "\"" + strWorkingFrom + ":0" + minute + "\"";
                } else {
                    strWorkingTo = "\"" + strWorkingTo + ":0" + minute + "\"";
                }
            } else {
                if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
                    strWorkingFrom = "\"" + strWorkingFrom + ":" + minute + "\"";
                } else {
                    strWorkingTo = "\"" + strWorkingTo + ":" + minute + "\"";
                }
            }
        } else {
            if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
                strWorkingFrom = "" + hourOfDay;
            } else {
                strWorkingTo = "" + hourOfDay;
            }
            if (minute < 10) {
                if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
                    strWorkingFrom = "\"" + strWorkingFrom + ":0" + minute + "\"";
                } else {
                    strWorkingTo = "\"" + strWorkingTo + ":0" + minute + "\"";
                }
            } else {
                if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
                    strWorkingFrom = "\"" + strWorkingFrom + ":" + minute + "\"";
                } else {
                    strWorkingTo = "\"" + strWorkingTo + ":" + minute + "\"";
                }
            }
        }
        Log.d(TAG, "onTimeSet() --> strWorkingFrom: " + strWorkingFrom + " - strWorkingTo: " + strWorkingTo);

        if (hourOfDay > 12) {
            hours = hourOfDay - 12;
            strDayOrNight = "PM";
            if (hours < 10) {
                strHours = "0" + hours;
            }
        } else {
            strDayOrNight = "AM";
            if (hourOfDay < 10) {
                strHours = "0" + hourOfDay;
            }
        }
        if (minute < 10) {
            strMin = "0" + minute;
        }
        if (currentSelectedTextView.equalsIgnoreCase("WorkingFrom")) {
            txtAdvancedSearchWorkingFrom.setText(strHours + ":" + strMin + " " + strDayOrNight);
        } else {
            txtAdvancedSearchWorkingTo.setText(strHours + ":" + strMin + " " + strDayOrNight);
        }
    }

    private void showTimePickerDialog() {
        Calendar now = Calendar.getInstance();
        BottomSheetPickerDialog.Builder builder = new GridTimePickerDialog.Builder(this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(getActivity()));
        GridTimePickerDialog.Builder gridDialogBuilder = (GridTimePickerDialog.Builder) builder;
        gridDialogBuilder.setHeaderTextColorSelected(Color.parseColor("#FFFFFF"));
        gridDialogBuilder.setHeaderTextColorUnselected(Color.parseColor("#AEAEE7"));

        gridDialogBuilder.setAccentColor(Color.parseColor("#171743"));
        gridDialogBuilder.setBackgroundColor(Color.parseColor("#FFFFFF"));
        gridDialogBuilder.setHeaderColor(Color.parseColor("#171743"));
        gridDialogBuilder.setHeaderTextDark(false);
        builder.build().show(getFragmentManager(), TAG);
    }

    private void showSpinnerDialog(final String dialogTitle, final TextView textView, ArrayList<String> list) {
        spinnerDialog = new SpinnerDialog(getActivity(), list, dialogTitle, R.style.DialogAnimations_SmileWindow, "Close");// With 	Animation

        spinnerDialog.setCancellable(true); // for cancellable
        spinnerDialog.setShowKeyboard(false);// for open keyboard by default

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                if (item.trim().length() > 0) {
                    textView.setText(item);
                    if (dialogTitle.equalsIgnoreCase("Select or Search City") && AppCoreCode.getInstance().cache.cities != null)
                        for (Cities.Result result : AppCoreCode.getInstance().cache.cities.result) {
                            if (txtAdvancedSearchCity.getText().toString().trim().length() > 0 && txtAdvancedSearchCity.getText().toString().equalsIgnoreCase(result.name)) {
                                cityId = result.id;
                                getSearchSpinnerData("GetAreas");
                                break;
                            }
                            cityId = null;
                        }
                }
            }
        });

        spinnerDialog.showSpinerDialog();
    }

    private void setViewsActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
            }
        });

        txtAdvancedSearchWorkingFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
                currentSelectedTextView = "WorkingFrom";
            }
        });

        txtAdvancedSearchWorkingTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
                currentSelectedTextView = "WorkingTo";
            }
        });

        txtAdvancedSearchSpeciality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog("Select or Search Speciality", txtAdvancedSearchSpeciality, specializationsList);
            }
        });

        txtAdvancedSearchService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog("Select or Search Service", txtAdvancedSearchService, servicesList);
            }
        });

        txtAdvancedSearchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog("Select or Search City", txtAdvancedSearchCity, citiesList);
            }
        });

        txtAdvancedSearchArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog("Select or Search Area", txtAdvancedSearchArea, areasList);
            }
        });

        btnAdvancedSearchSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etxtAdvancedSearchName.getText().toString().trim().length() > 0) {
                    providerName = "\"" + etxtAdvancedSearchName.getText().toString().trim() + "\"";
                } else {
                    providerName = null;
                }
                if (AppCoreCode.getInstance().cache.specializations != null && AppCoreCode.getInstance().cache.specializations.result != null)
                    if (AppCoreCode.getInstance().cache.specializations.result != null)
                        for (Specializations.Result result : AppCoreCode.getInstance().cache.specializations.result) {
                            if (txtAdvancedSearchSpeciality.getText().toString().trim().length() > 0 && txtAdvancedSearchSpeciality.getText().toString().equalsIgnoreCase(result.name)) {
                                specialityId = result.id;
                                break;
                            }
                            specialityId = null;
                        }

                if (AppCoreCode.getInstance().cache.services != null && AppCoreCode.getInstance().cache.services.result != null)
                    for (Services.Result result : AppCoreCode.getInstance().cache.services.result) {
                        if (txtAdvancedSearchService.getText().toString().trim().length() > 0 && txtAdvancedSearchService.getText().toString().equalsIgnoreCase(result.name)) {
                            serviceId = result.id;
                            break;
                        }
                        serviceId = null;
                    }
                if (AppCoreCode.getInstance().cache.areas != null && AppCoreCode.getInstance().cache.areas.result != null)
                    for (Areas.Result result : AppCoreCode.getInstance().cache.areas.result) {
                        if (txtAdvancedSearchArea.getText().toString().trim().length() > 0 && txtAdvancedSearchArea.getText().toString().equalsIgnoreCase(result.name)) {
                            areaId = result.id;
                            break;
                        }
                        areaId = null;
                    }
                if (AppCoreCode.getInstance().cache.cities != null && AppCoreCode.getInstance().cache.cities.result != null)
                    for (Cities.Result result : AppCoreCode.getInstance().cache.cities.result) {
                        if (txtAdvancedSearchCity.getText().toString().trim().length() > 0 && txtAdvancedSearchCity.getText().toString().equalsIgnoreCase(result.name)) {
                            cityId = result.id;
                            break;
                        }
                        cityId = null;
                    }
                progressBarAdvancedSearchloadingLists.setVisibility(View.VISIBLE);
                btnAdvancedSearchSearch.setEnabled(false);
                btnAdvancedSearchSearch.setAlpha((float) 0.5);
                getSearchSpinnerData("SearchResults");
            }
        });

        btnAdvancedSearchType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomSearchTypePickerDialog();
            }
        });

    }

    private void showCustomSearchTypePickerDialog() {
        final Dialog searchTypePickerDialog = new Dialog(getActivity());
        searchTypePickerDialog.setContentView(R.layout.search_type_custom_dialog);
        searchTypePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageButton ibtnClinic = searchTypePickerDialog.findViewById(R.id.ibtnClinic);
        ImageButton ibtnHospital = searchTypePickerDialog.findViewById(R.id.ibtnHospital);
        ImageButton ibtnScanCenter = searchTypePickerDialog.findViewById(R.id.ibtnScanCenter);
        ImageButton ibtnLab = searchTypePickerDialog.findViewById(R.id.ibtnLab);
        ImageButton ibtnPharmacy = searchTypePickerDialog.findViewById(R.id.ibtnPharmacy);
        ImageButton ibtnPhysiotherapy = searchTypePickerDialog.findViewById(R.id.ibtnPhysiotherapy);
        ImageButton ibtnWellnessFitness = searchTypePickerDialog.findViewById(R.id.ibtnWellnessFitness);
        ImageButton ibtnOptics = searchTypePickerDialog.findViewById(R.id.ibtnOptics);

        ibtnClinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Clinics, searchTypePickerDialog);
            }
        });

        ibtnHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Hospital, searchTypePickerDialog);
            }
        });

        ibtnScanCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Scan, searchTypePickerDialog);
            }
        });

        ibtnLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Labs, searchTypePickerDialog);
            }
        });

        ibtnPharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Pharmacy, searchTypePickerDialog);
            }
        });

        ibtnPhysiotherapy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Physiotherapy, searchTypePickerDialog);
            }
        });

        ibtnWellnessFitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Fitness, searchTypePickerDialog);
            }
        });

        ibtnOptics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeSearchLogo(Optics, searchTypePickerDialog);
            }
        });
        searchTypePickerDialog.show();
    }

    private void changeSearchLogo(int indexOfSelectSearch, Dialog dialog) {
        Log.d(TAG, "changeSearchLogo() --> indexOfSelectSearch: " + indexOfSelectSearch);
        providerTypeId = indexOfSelectSearch;
        AppCoreCode.getInstance().memberMainViewActivity.currentSelectedProviderType = indexOfSelectSearch;
        if (indexOfSelectSearch == 0) {
            txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_quick_search));
            providerTypeId = null;
        }

        if (indexOfSelectSearch == 0) {
            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_empty_icon);
            btnAdvancedSearchType.setText(getResources().getString(R.string.advanc_filter_dialog_provider_type));
            lloName.setVisibility(View.GONE);
            lloSpeciality.setVisibility(View.GONE);
            lloService.setVisibility(View.GONE);
            lloCity.setVisibility(View.VISIBLE);
            lloArea.setVisibility(View.GONE);
            lloWorkingHours.setVisibility(View.GONE);
        } else if (indexOfSelectSearch == Clinics) {
            providerTypeId = Clinics;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_clinic_icon);
            btnAdvancedSearchType.setText(R.string.advanc_clinic_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                if (txtAdvancedSearchTitle != null)
                    txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.VISIBLE);
                lloService.setVisibility(View.VISIBLE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        } else if (indexOfSelectSearch == Hospital) {
            providerTypeId = Hospital;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_hospital_icon);
            btnAdvancedSearchType.setText(R.string.advanc_hospital_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.VISIBLE);
                lloService.setVisibility(View.VISIBLE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.GONE);
            }
        } else if (indexOfSelectSearch == Scan) {
            providerTypeId = Scan;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_scan_center_icon);
            btnAdvancedSearchType.setText(R.string.advanc_scan_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.GONE);
                lloService.setVisibility(View.VISIBLE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        } else if (indexOfSelectSearch == Labs) {
            providerTypeId = Labs;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_lab_icon);
            btnAdvancedSearchType.setText(R.string.advanc_lab_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.GONE);
                lloService.setVisibility(View.GONE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        } else if (indexOfSelectSearch == Pharmacy) {
            providerTypeId = Pharmacy;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_pharmacy_icon);
            btnAdvancedSearchType.setText(R.string.advanc_pharmacy_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.GONE);
                lloService.setVisibility(View.GONE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        } else if (indexOfSelectSearch == Physiotherapy) {
            providerTypeId = Physiotherapy;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_physiotherapy_icon);
            btnAdvancedSearchType.setText(R.string.advanc_physiotherapy_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.GONE);
                lloService.setVisibility(View.VISIBLE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        } else if (indexOfSelectSearch == Fitness) {
            providerTypeId = Fitness;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_wellness_fitness_icon);
            btnAdvancedSearchType.setText(R.string.advanc_wellness_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.GONE);
                lloService.setVisibility(View.GONE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        } else if (indexOfSelectSearch == Optics) {
            providerTypeId = Optics;

            ibtnSearchLogo.setImageResource(R.drawable.advanced_search_optics_icon);
            btnAdvancedSearchType.setText(R.string.advanc_optics_type);
            if (AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch != 0) {
                txtAdvancedSearchTitle.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
                lloName.setVisibility(View.VISIBLE);
                lloSpeciality.setVisibility(View.GONE);
                lloService.setVisibility(View.GONE);
                lloCity.setVisibility(View.VISIBLE);
//            lloArea.setVisibility(View.VISIBLE);
                lloWorkingHours.setVisibility(View.VISIBLE);
            }
        }
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfServerOperation.equalsIgnoreCase("SearchParameters")) {
                    citiesResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/cities"
                            , "{\"countryId\": 1}");
                    Log.d(TAG, "doInBackground() --> citiesResponse: " + citiesResponse);
                    AppCoreCode.getInstance().cache.cities = AppCoreCode.getInstance().cache.gson.fromJson(citiesResponse, Cities.class);
                    JSONObject citiesReader = new JSONObject(citiesResponse);
                    String citiesItemsArray = citiesReader.get("result").toString();
                    Log.d(TAG, "doInBackground() --> citiesReader.getJSONArray: " + citiesItemsArray);
                    AppCoreCode.getInstance().cache.cities.result = AppCoreCode.getInstance().cache.gson.fromJson(citiesItemsArray, new TypeToken<List<Cities.Result>>() {
                    }.getType());


                    specializationResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/specializations");
                    Log.d(TAG, "doInBackground() --> specializationResponse: " + specializationResponse);
                    AppCoreCode.getInstance().cache.specializations = AppCoreCode.getInstance().cache.gson.fromJson(specializationResponse, Specializations.class);
                    JSONObject specializationReader = new JSONObject(specializationResponse);
                    String specializationItemsArray = specializationReader.getJSONArray("result").toString();
                    AppCoreCode.getInstance().cache.specializations.result = AppCoreCode.getInstance().cache.gson.fromJson(specializationItemsArray, new TypeToken<List<Specializations.Result>>() {
                    }.getType());

                    servicesResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/services"
                            , "{\"providerType\": null}");
                    Log.d(TAG, "doInBackground() --> servicesResponse: " + servicesResponse);
                    AppCoreCode.getInstance().cache.services = AppCoreCode.getInstance().cache.gson.fromJson(servicesResponse, Services.class);
                    JSONObject servicesReader = new JSONObject(servicesResponse);
                    String servicesItemsArray = servicesReader.getJSONArray("result").toString();
                    AppCoreCode.getInstance().cache.services.result = AppCoreCode.getInstance().cache.gson.fromJson(servicesItemsArray, new TypeToken<List<Services.Result>>() {
                    }.getType());
                } else if (typeOfServerOperation.equalsIgnoreCase("GetAreas")) {
                    areasResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/areas"
                            , "{\"cityId\": " + cityId + "}");
                    Log.d(TAG, "doInBackground() --> areasResponse: " + areasResponse);
                    AppCoreCode.getInstance().cache.areas = AppCoreCode.getInstance().cache.gson.fromJson(areasResponse, Areas.class);
                    JSONObject areasReader = new JSONObject(areasResponse);
                    String areasItemsArray = areasReader.getJSONArray("result").toString();
                    AppCoreCode.getInstance().cache.areas.result = AppCoreCode.getInstance().cache.gson.fromJson(areasItemsArray, new TypeToken<List<Areas.Result>>() {
                    }.getType());
                } else {
                    // providerSystemType
                    AppCoreCode.getInstance().cache.advancedSearchJsonToSend = "{\"providerSystemType\": null,\"providerName\": " + providerName
                            + ",\"specialityId\": " + specialityId + " ,\"serviceId\": " + serviceId + " " +
                            ",\"cityId\": " + cityId + ",\"areaId\": " + areaId + ",\"workingFrom\": " + strWorkingFrom +
                            ",\"workingTo\": " + strWorkingTo + ", \"ProviderTypeId\": " + providerTypeId + "";
                    searchResultsResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getprovidersbysearch"
                            , AppCoreCode.getInstance().cache.advancedSearchJsonToSend + ",\"pageNumber\": 1,\"pageSize\": 10}");

                    Log.d(TAG, "doInBackground() --> searchResultsResponse: " + searchResultsResponse);
                    JSONObject reader = new JSONObject(searchResultsResponse);
                    AppCoreCode.getInstance().cache.advancedSearch = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), AdvancedSearch.class);

                    String itemsArray = reader.getJSONObject("result").getJSONArray("items").toString();
                    // TODO: Need to change List to Map
                    AppCoreCode.getInstance().cache.advancedSearch.itemsList = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<AdvancedSearch.Items>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> itemsList size: " + AppCoreCode.getInstance().cache.advancedSearch.itemsList.size());
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (typeOfServerOperation.equalsIgnoreCase("SearchParameters")) {
                citiesList = new ArrayList();
                citiesList.add("");
                if (AppCoreCode.getInstance().cache.cities != null && AppCoreCode.getInstance().cache.cities.result != null)
                    for (Cities.Result result : AppCoreCode.getInstance().cache.cities.result) {
                        citiesList.add(result.name);
                    }

                specializationsList = new ArrayList();
                specializationsList.add("");
                if (AppCoreCode.getInstance().cache.specializations != null && AppCoreCode.getInstance().cache.specializations.result != null)
                    for (Specializations.Result result : AppCoreCode.getInstance().cache.specializations.result) {
                        specializationsList.add(result.name);
                    }

                servicesList = new ArrayList();
                servicesList.add("");
                if (AppCoreCode.getInstance().cache.services != null && AppCoreCode.getInstance().cache.services.result != null)
                    for (Services.Result result : AppCoreCode.getInstance().cache.services.result) {
                        servicesList.add(result.name);
                    }

                btnAdvancedSearchSearch.setEnabled(true);
                btnAdvancedSearchSearch.setAlpha((float) 1);
                progressBarAdvancedSearchloadingLists.setVisibility(View.INVISIBLE);
            } else if (typeOfServerOperation.equalsIgnoreCase("GetAreas")) {
                areasList = new ArrayList();
                areasList.add("");
                if (AppCoreCode.getInstance().cache.areas != null && AppCoreCode.getInstance().cache.areas.result != null) {
                    for (Areas.Result result : AppCoreCode.getInstance().cache.areas.result) {
                        areasList.add(result.name);
                    }
                }
                lloArea.setVisibility(View.VISIBLE);
                btnAdvancedSearchSearch.setEnabled(true);
                btnAdvancedSearchSearch.setAlpha((float) 1);
                progressBarAdvancedSearchloadingLists.setVisibility(View.INVISIBLE);
            } else {
                progressBarAdvancedSearchloadingLists.setVisibility(View.INVISIBLE);
                btnAdvancedSearchSearch.setEnabled(true);
                btnAdvancedSearchSearch.setAlpha((float) 1);
                if (getActivity() != null) {
                    ((MemberMainViewActivity) getActivity()).changeFragment(3);
                }
            }
        }
    }
}

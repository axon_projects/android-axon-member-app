package com.axon.memberApp.MemberMainActivity.MemberProfile.MemberProfileSettings;


import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;
import com.stfalcon.frescoimageviewer.ImageViewer;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingsFragment extends Fragment implements com.philliphsu.bottomsheetpickers.date.DatePickerDialog.OnDateSetListener {
    private View view;
    private ImageButton ibtnClose;
    private Button btnSave, btnBuyMembership, btnProfileSettingReset, btnBuyFromFawryMembership;
    private LinearLayout lloMemberShipExists, lloBuyMemberShip, lloCorporateLayout, lloResetPassword, lloRequestMessageMemberShip;
    private SimpleDraweeView imgProfileSettingMemberAvatar;
    private TextView txtProfileSettingMemberName, txtProfileSettingMemberMobileNo, txtProfileSettingMemberEmail, txtProfileSettingMemberBirthdate, txtProfileSettingMemberShipID, txtProfileSettingMemberShipExpirDate, txtProfileSettingMemberShipStatus, txtProfileSettingMemberShipAddress, txtProfileSettingCity, txtProfileSettingArea, txtProfileSettingMemberCorporate, txtProfileSettingSocialLogin, txtRequestMembershipMessage;
    private Spinner spProfileSettingMemberGender;

    private ArrayAdapter<CharSequence> genderAdapter;
    private int currentYear;
    private AsyncTaskClass asyncTaskClass;
    private String updateProfileResponse;
    private String msg;
    private String TAG = "ProfileSettingsFragment";
    private String typeOfRequest;
    private String newPasswordToRequest;
    private boolean success;

    private Dialog customEditSearchDialog;

    public ProfileSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.profile_settings_fragment, container, false);
//        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null && isAdded() && view != null) {
            initViews();
        }
    }

    private void initViews() {
        txtProfileSettingCity = view.findViewById(R.id.txtProfileSettingCity);
        txtProfileSettingArea = view.findViewById(R.id.txtProfileSettingArea);
        txtProfileSettingMemberCorporate = view.findViewById(R.id.txtProfileSettingMemberCorporate);
        txtProfileSettingSocialLogin = view.findViewById(R.id.txtProfileSettingSocialLogin);
        lloCorporateLayout = view.findViewById(R.id.lloCorporateLayout);
        lloResetPassword = view.findViewById(R.id.lloResetPassword);
        ibtnClose = view.findViewById(R.id.ibtnClose);
        btnSave = view.findViewById(R.id.btnSave);
        btnProfileSettingReset = view.findViewById(R.id.btnProfileSettingReset);
        btnBuyMembership = view.findViewById(R.id.btnBuyMembership);

        btnBuyFromFawryMembership = view.findViewById(R.id.btnBuyFromFawryMembership);
        txtRequestMembershipMessage = view.findViewById(R.id.txtRequestMembershipMessage);
        lloRequestMessageMemberShip = view.findViewById(R.id.lloRequestMessageMemberShip);

        lloMemberShipExists = view.findViewById(R.id.lloMemberShipExists);
        lloBuyMemberShip = view.findViewById(R.id.lloBuyMemberShip);
        imgProfileSettingMemberAvatar = view.findViewById(R.id.imgProfileSettingMemberAvatar);
        txtProfileSettingMemberName = view.findViewById(R.id.txtProfileSettingMemberName);
        txtProfileSettingMemberName.setEnabled(false);
        txtProfileSettingMemberMobileNo = view.findViewById(R.id.txtProfileSettingMemberMobileNo);
        txtProfileSettingMemberEmail = view.findViewById(R.id.txtProfileSettingMemberEmail);
        txtProfileSettingMemberBirthdate = view.findViewById(R.id.txtProfileSettingMemberBirthdate);
        spProfileSettingMemberGender = view.findViewById(R.id.spProfileSettingMemberGender);
        txtProfileSettingMemberShipID = view.findViewById(R.id.txtProfileSettingMemberShipID);
        txtProfileSettingMemberShipExpirDate = view.findViewById(R.id.txtProfileSettingMemberShipExpirDate);
        txtProfileSettingMemberShipStatus = view.findViewById(R.id.txtProfileSettingMemberShipStatus);
        txtProfileSettingMemberShipAddress = view.findViewById(R.id.txtProfileSettingMemberShipAddress);

        btnProfileSettingReset.setVisibility(View.GONE);
        txtProfileSettingSocialLogin.setVisibility(View.GONE);

        // Setup Gender drop down list
        if (getActivity() != null) {
            genderAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.gender, R.layout.spinner_textview_align);
            spProfileSettingMemberGender.setAdapter(genderAdapter);
        }
        setViewsActions();

        if (AppCoreCode.getInstance().cache.memberProfile != null)
            setViewsValues();
    }

    private void setViewsActions() {
        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(5);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeOfRequest = "ChangeProfileData";
                updateProfile();
            }
        });

        txtProfileSettingMemberBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDatePickerDialog();
            }
        });

        btnProfileSettingReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomChangePasswordDialog();
            }
        });

        btnBuyMembership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(9);
            }
        });

        imgProfileSettingMemberAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] listimages = {AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl};
                new ImageViewer.Builder(getActivity(), listimages)
                        .show();
            }
        });
    }

    private void showCustomChangePasswordDialog() {
        customEditSearchDialog = new Dialog(getActivity());
        customEditSearchDialog.setContentView(R.layout.custom_change_member_password_dialog);
        customEditSearchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageButton ibtnClose = customEditSearchDialog.findViewById(R.id.ibtnClose);
        Button ibtnDone = customEditSearchDialog.findViewById(R.id.ibtnDone);
        final EditText etxtOldPassword = customEditSearchDialog.findViewById(R.id.etxtOldPassword);
        final EditText etxtNewPassword = customEditSearchDialog.findViewById(R.id.etxtNewPassword);
        final EditText etxtConfirmNewPassword = customEditSearchDialog.findViewById(R.id.etxtConfirmNewPassword);

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customEditSearchDialog.dismiss();
            }
        });

        ibtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInputs(etxtOldPassword, etxtNewPassword, etxtConfirmNewPassword);
            }
        });

        customEditSearchDialog.show();
    }

    private void validateUserInputs(EditText oldPassword, EditText newPassword, EditText confirmNewPassword) {
        if (oldPassword.getText().toString().trim().isEmpty()) {
            oldPassword.setError("Current password is required!");
            oldPassword.requestFocus();
            return;
        }
        if (!oldPassword.getText().toString().trim().equalsIgnoreCase(AppCoreCode.getInstance().getStringValuesFromSharedPreferences("password"))) {
            oldPassword.setError("Current password is wrong!");
            oldPassword.requestFocus();
            return;
        }

        if (newPassword.getText().toString().trim().isEmpty()) {
            newPassword.setError("New password is required!");
            newPassword.requestFocus();
            return;
        }

        if (confirmNewPassword.getText().toString().trim().isEmpty()) {
            confirmNewPassword.setError("Confirmation password is required!");
            confirmNewPassword.requestFocus();
            return;
        }

        if (!newPassword.getText().toString().trim().equalsIgnoreCase(confirmNewPassword.getText().toString().trim())) {
            newPassword.setError("Password doesn't match!");
            newPassword.requestFocus();
            return;
        }
        typeOfRequest = "ChangePassword";
        newPasswordToRequest = newPassword.getText().toString().trim();
        updateProfile();
    }

    private void updateProfile() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void setViewsValues() {
        imgProfileSettingMemberAvatar.setImageURI(AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl);
        txtProfileSettingMemberName.setText(AppCoreCode.getInstance().cache.memberProfile.memberName);
        txtProfileSettingMemberMobileNo.setText(AppCoreCode.getInstance().cache.memberProfile.mobile);
        txtProfileSettingMemberEmail.setText(AppCoreCode.getInstance().cache.memberProfile.email);
        txtProfileSettingMemberBirthdate.setText(AppCoreCode.getInstance().cache.memberProfile.birthDate);
        if (AppCoreCode.getInstance().cache.memberProfile.corporateName != null && !AppCoreCode.getInstance().cache.memberProfile.corporateName.isEmpty()) {
            lloCorporateLayout.setVisibility(View.VISIBLE);
            txtProfileSettingMemberCorporate.setText(AppCoreCode.getInstance().cache.memberProfile.corporateName);
        } else {
            lloCorporateLayout.setVisibility(View.GONE);
            txtProfileSettingMemberCorporate.setText("");
        }

        if (AppCoreCode.getInstance().cache.memberProfile.gender.equalsIgnoreCase("male")) {
            spProfileSettingMemberGender.setSelection(0);
        } else {
            spProfileSettingMemberGender.setSelection(1);
        }

        if (AppCoreCode.getInstance().cache.validMembership.result == null) {
            Log.d(TAG, "AppCoreCode.getInstance().cache.validMembership.result is null");
            lloMemberShipExists.setVisibility(View.GONE);
            lloBuyMemberShip.setVisibility(View.VISIBLE);
            lloRequestMessageMemberShip.setVisibility(View.GONE);
        } else if (AppCoreCode.getInstance().cache.validMembership.result.membership != null) {
            Log.d(TAG, "AppCoreCode.getInstance().cache.validMembership.result.membership not null");
            lloMemberShipExists.setVisibility(View.VISIBLE);
            lloBuyMemberShip.setVisibility(View.GONE);
            lloRequestMessageMemberShip.setVisibility(View.GONE);
            txtProfileSettingCity.setText(AppCoreCode.getInstance().cache.memberProfile.memberShip.cityName);
            txtProfileSettingArea.setText(AppCoreCode.getInstance().cache.memberProfile.memberShip.areaName);
            txtProfileSettingMemberShipAddress.setText(AppCoreCode.getInstance().cache.memberProfile.memberShip.address);
            txtProfileSettingMemberShipStatus.setText(AppCoreCode.getInstance().cache.memberProfile.memberShip.membershipStatusName);
            txtProfileSettingMemberShipID.setText(AppCoreCode.getInstance().cache.memberProfile.memberShip.membershipCode);
            txtProfileSettingMemberShipExpirDate.setText(AppCoreCode.getInstance().cache.memberProfile.memberShip.endDate);
        } else if (AppCoreCode.getInstance().cache.validMembership.result.requestMessage != null) {
            Log.d(TAG, "AppCoreCode.getInstance().cache.validMembership.result.requestMessage not null");
            lloMemberShipExists.setVisibility(View.GONE);
            lloBuyMemberShip.setVisibility(View.GONE);
            lloRequestMessageMemberShip.setVisibility(View.VISIBLE);
            txtRequestMembershipMessage.setText(AppCoreCode.getInstance().cache.validMembership.result.requestMessage.message);
            if (AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl != null) {
//                Log.d(TAG, "AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl not null " + AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl);
                btnBuyFromFawryMembership.setVisibility(View.VISIBLE);
                btnBuyFromFawryMembership.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl
                        AppCoreCode.getInstance().cache.fawryPaymentSource = "ProfileSetting";
                        if (getActivity() != null) {
                            try {
                                openFawryURL();
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(getActivity(), "No application can handle this request."
                                        + " Please install a webbrowser", Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } else {
                btnBuyFromFawryMembership.setVisibility(View.GONE);
            }
        }
        if (AppCoreCode.getInstance().sharedpreferences != null) {
            Log.d(TAG, "setViewsValues() --> typeOfRequest: " + AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest"));
//        if (lloResetPassword != null)
            if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("FaceLogin")) {
                txtProfileSettingMemberEmail.setEnabled(false);
                txtProfileSettingSocialLogin.setVisibility(View.VISIBLE);
                btnProfileSettingReset.setVisibility(View.GONE);
//                lloResetPassword.setVisibility(View.GONE);
            } else {
                txtProfileSettingMemberEmail.setEnabled(true);
                btnProfileSettingReset.setVisibility(View.VISIBLE);
                txtProfileSettingSocialLogin.setVisibility(View.GONE);
//                lloResetPassword.setVisibility(View.VISIBLE);
            }
        }
    }

    private void openFawryURL() {
        if (AppCoreCode.getInstance().cache.fawryPaymentSource != null && AppCoreCode.getInstance().cache.fawryPaymentSource.equalsIgnoreCase("ProfileSetting")) {
            if (AppCoreCode.getInstance().cache.validMembership != null && AppCoreCode.getInstance().cache.validMembership.result != null && AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl != null) {
                AppCoreCode.getInstance().cache.isBrowserOpened = true;
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl + "&access_token=" + URLEncoder.encode(AppCoreCode.getInstance().cache.accessToken))));
            }
        } else if (AppCoreCode.getInstance().cache.fawryPaymentSource.equalsIgnoreCase("CreateMembership")) {
            if (AppCoreCode.getInstance().cache.membership != null && AppCoreCode.getInstance().cache.membership.result != null && AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl != null) {
                AppCoreCode.getInstance().cache.isBrowserOpened = true;
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl + "&access_token=" + URLEncoder.encode(AppCoreCode.getInstance().cache.accessToken))));
            }
        }
        if (AppCoreCode.getInstance().cache.membership != null && AppCoreCode.getInstance().cache.membership.result != null && AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl != null) {
            AppCoreCode.getInstance().cache.isBrowserOpened = true;
            getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl + "&access_token=" + URLEncoder.encode(AppCoreCode.getInstance().cache.accessToken))));
        }
//        if (getActivity() != null)
//            ((MemberMainViewActivity) getActivity()).changeFragment(11);
    }

    private void showCustomDatePickerDialog() {
        SimpleDateFormat date1 = null;
        Calendar now = Calendar.getInstance();
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd");
            if (date1 != null) {
                now.setTime(date1.parse(txtProfileSettingMemberBirthdate.getText().toString()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DatePickerDialog date = new DatePickerDialog.Builder(this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)).build();
        date.setHeaderTextColorSelected(Color.parseColor("#FFFFFF"));
        date.setHeaderTextColorUnselected(Color.parseColor("#FFFFFF"));
        date.setDayOfWeekHeaderTextColorSelected(Color.parseColor("#FFFFFF"));
        date.setDayOfWeekHeaderTextColorUnselected(Color.parseColor("#FFFFFF"));

        date.setAccentColor(Color.parseColor("#171743"));
        date.setBackgroundColor(Color.parseColor("#FFFFFF"));
        date.setHeaderColor(Color.parseColor("#171743"));
        date.setHeaderTextDark(false);
        date.show(getFragmentManager(), TAG);
    }

    /**
     * @param dialog      The dialog associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String strMonth = String.valueOf(monthOfYear + 1);
        String strDays = String.valueOf(dayOfMonth);
        if ((monthOfYear + 1) < 10) {
            strMonth = "0" + (monthOfYear + 1);
        }
        if (dayOfMonth < 10) {
            strDays = "0" + dayOfMonth;
        }
        txtProfileSettingMemberBirthdate.setText(year + "-" + strMonth + "-" + strDays);
        dialog.dismiss();
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfRequest.equalsIgnoreCase("ChangeProfileData")) {
                    updateProfileResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/updateprofileinfo"
                            , "{\"name\": \"" + txtProfileSettingMemberName.getText().toString().trim() + "\",\"mobileNumber\": \"" + txtProfileSettingMemberMobileNo.getText().toString().trim()
                                    + "\",\"emailAddress\": \"" + txtProfileSettingMemberEmail.getText().toString().trim() + "\",\"password\": \"" + AppCoreCode.getInstance().getStringValuesFromSharedPreferences("password") + "\",\"gender\": \""
                                    + spProfileSettingMemberGender.getSelectedItem().toString() + "\",\"birthDate\": \""
                                    + txtProfileSettingMemberBirthdate.getText().toString().trim() + "\"}");

                    Log.d(TAG, "doInBackground() --> updateProfileResponse: " + updateProfileResponse);
                    JSONObject reader = new JSONObject(updateProfileResponse);
                    boolean success = reader.getBoolean("success");
                    Log.d(TAG, "doInBackground() --> update profile success status: " + success);
                    if (success) {
                        msg = getResources().getString(R.string.member_success_update);
                    } else {
                        JSONObject error = reader.getJSONObject("error");
                        if (error != null) {
                            msg = error.getString("message");
                        }
                    }
                } else {
                    updateProfileResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/changepassword"
                            , "{\"newPassword\": \"" + newPasswordToRequest + "\"}");

                    Log.d(TAG, "doInBackground() --> updateProfileResponse: " + updateProfileResponse);
                    JSONObject reader = new JSONObject(updateProfileResponse);
                    success = reader.getBoolean("success");
                    Log.d(TAG, "doInBackground() --> update profile success status: " + success);
                    if (success) {
                        msg = getResources().getString(R.string.member_password_success_update);
                    } else {
                        JSONObject error = reader.getJSONObject("error");
                        if (error != null) {
                            msg = error.getString("message");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            if (typeOfRequest.equalsIgnoreCase("ChangeProfileData")) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(5);
            } else {
                if (customEditSearchDialog != null)
                    customEditSearchDialog.dismiss();
                if (AppCoreCode.getInstance().memberMainViewActivity != null && success)
                    AppCoreCode.getInstance().memberMainViewActivity.logout();
            }
        }
    }
}

package com.axon.memberApp.MemberMainActivity.CreateMemberShip;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmMemberShipPaymentFragment extends Fragment {
    private View view;
    private Button btnConfirm;
    private TextView txtConfirmPaymentMembershipCode, txtConfirmPaymentMembershipExpireDate;
    private String TAG = "ConfirmMemberShipPaymentFragment";

    public ConfirmMemberShipPaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.confirm_member_ship_payment_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null) {
            initViews();
        }
    }

    private void initViews() {
        txtConfirmPaymentMembershipCode = view.findViewById(R.id.txtConfirmPaymentMembershipCode);
        txtConfirmPaymentMembershipExpireDate = view.findViewById(R.id.txtConfirmPaymentMembershipExpireDate);
        btnConfirm = view.findViewById(R.id.btnConfirm);

        setViewsValues();
        setViewsActions();
    }

    private void setViewsValues() {
        if (AppCoreCode.getInstance().cache.membership != null && AppCoreCode.getInstance().cache.membership.result != null) {
            txtConfirmPaymentMembershipCode.setText("#" + AppCoreCode.getInstance().cache.membership.result.memberShipCode);
            txtConfirmPaymentMembershipExpireDate.setText(AppCoreCode.getInstance().cache.membership.result.endDate);
        } else if (AppCoreCode.getInstance().cache.validMembership != null && AppCoreCode.getInstance().cache.validMembership.result != null
                && AppCoreCode.getInstance().cache.validMembership.result.requestMessage != null) {
            if (AppCoreCode.getInstance().cache.validMembership.result.requestMessage.membershipCode != null) {
                txtConfirmPaymentMembershipCode.setText("#" + AppCoreCode.getInstance().cache.validMembership.result.requestMessage.membershipCode);
            }
            if (AppCoreCode.getInstance().cache.validMembership.result.requestMessage.endDate != null) {
                txtConfirmPaymentMembershipExpireDate.setText(AppCoreCode.getInstance().cache.validMembership.result.requestMessage.endDate);
            }

        }
    }

    private void setViewsActions() {
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "setViewsActions() --> getActivity() " + getActivity());
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
            }
        });
    }
}

package com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes;

public class Membership {
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public Error error = new Error();
    public Result result;

    public class Error {
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }

    public class Result {
        public String membershipCreationRequestId; // memberShipId
        public String memberShipCode;
        public String address;
        public int countryId;
        public String countryName;
        public int cityId;
        public String cityName;
        public int areaId;
        public String areaName;
        public String startDate;
        public String endDate;
        public int membershipStatus;
        public String membershipStatusName;
        public String fawryPaymentUrl;
    }

}

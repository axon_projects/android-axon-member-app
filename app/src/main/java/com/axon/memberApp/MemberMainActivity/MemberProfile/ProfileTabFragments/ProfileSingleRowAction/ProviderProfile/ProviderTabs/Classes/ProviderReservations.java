package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes;

import java.util.ArrayList;
import java.util.List;

public class ProviderReservations {
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public Error error = new Error();
    public List<Result> result = new ArrayList<>();

    public class Result {
        public String date;
        public String dateFrom;
        public String dateTo;
        public boolean isAvailable;
        public int bookingSystem;
    }
}

package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.axon.memberApp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderProfileInfoTabFragment extends Fragment {
    private LinearLayout lloOffersSection, lloProviderServices, lloOffDays, lloWorkingHours;
    private RecyclerView recyclerViewProviderGallary, recyclerViewProviderOffers, recyclerViewProviderServices;
    private RecyclerView.Adapter galleryAdapter, offersAdapter, servicesAdapter;
    private TextView txtProviderProfileDesc, txtProviderProfileClinicName, txtProviderProfileCity, txtProviderProfileArea, txtProviderProfileAddress, txtProviderProfileGallaryNum, txtProviderProfileOffersNum, txtProviderNameTitle, txtProviderProfileWorkingHours;
    private ImageView imgProviderInfoLocation;
    private SweetAlertDialog sweetAlertDialog;
    private Button btnOffDaysSun, btnOffDaysMon, btnOffDaysTus, btnOffDaysWed, btnOffDaysThu, btnOffDaysFri, btnOffDaysSat;

    private View view;
    private String TAG = "ProviderProfileInfoTabFragment";
    private int CLINICS_INDEX = 1;
    private int HOSPITAL_INDEX = 8;
    private int SCAN_INDEX = 6;
    private int FITNESS_INDEX = 3;
    private int PHYSIOTHERAPY_INDEX = 2;
    private int LABS_INDEX = 7;
    private int OPTICS_INDEX = 5;
    private int PHARMACY_INDEX = 4;

    private int ClinicsSysProviderType = 0;
    private int ScanSysProviderType = 1;
    private int FitnessSysProviderType = 2;
    private int PhysiotherapySysProviderType = 3;
    private int LabsSysProviderType = 4;
    private int OpticsSysProviderType = 5;
    private int PharmacySysProviderType = 6;

    public ProviderProfileInfoTabFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.provider_profile_info_tab_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        lloOffDays = view.findViewById(R.id.lloOffDays);
        lloWorkingHours = view.findViewById(R.id.lloWorkingHours);
        txtProviderProfileWorkingHours = view.findViewById(R.id.txtProviderProfileWorkingHours);
        btnOffDaysSun = view.findViewById(R.id.btnOffDaysSun);
        btnOffDaysMon = view.findViewById(R.id.btnOffDaysMon);
        btnOffDaysTus = view.findViewById(R.id.btnOffDaysTus);
        btnOffDaysWed = view.findViewById(R.id.btnOffDaysWed);
        btnOffDaysThu = view.findViewById(R.id.btnOffDaysThu);
        btnOffDaysFri = view.findViewById(R.id.btnOffDaysFri);
        btnOffDaysSat = view.findViewById(R.id.btnOffDaysSat);

        lloOffersSection = view.findViewById(R.id.lloOffersSection);
        lloProviderServices = view.findViewById(R.id.lloProviderServices);

        imgProviderInfoLocation = view.findViewById(R.id.imgProviderInfoLocation);

        txtProviderProfileDesc = view.findViewById(R.id.txtProviderProfileDesc);
        txtProviderProfileClinicName = view.findViewById(R.id.txtProviderProfileClinicName);
        txtProviderProfileCity = view.findViewById(R.id.txtProviderProfileCity);
        txtProviderProfileArea = view.findViewById(R.id.txtProviderProfileArea);
        txtProviderProfileAddress = view.findViewById(R.id.txtProviderProfileAddress);
        txtProviderProfileGallaryNum = view.findViewById(R.id.txtProviderProfileGallaryNum);
        txtProviderProfileOffersNum = view.findViewById(R.id.txtProviderProfileOffersNum);
        txtProviderNameTitle = view.findViewById(R.id.txtProviderNameTitle);

        recyclerViewProviderServices = view.findViewById(R.id.recyclerViewProviderServices);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewProviderServices.setHasFixedSize(true);
        recyclerViewProviderServices.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));

        recyclerViewProviderGallary = view.findViewById(R.id.recyclerViewProviderGallary);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewProviderGallary.setHasFixedSize(true);
        recyclerViewProviderGallary.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        recyclerViewProviderOffers = view.findViewById(R.id.recyclerViewProviderOffers);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewProviderOffers.setHasFixedSize(true);
        recyclerViewProviderOffers.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        setValuesToViews();
    }

    public void setValuesToViews() {
        if (AppCoreCode.getInstance().cache.providerProfile != null
                && AppCoreCode.getInstance().cache.providerProfile.result != null
                && AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData != null
                && txtProviderProfileDesc != null
                && txtProviderProfileClinicName != null
                && txtProviderProfileCity != null
                && txtProviderProfileArea != null
                && txtProviderProfileAddress != null
                && txtProviderProfileGallaryNum != null
                && txtProviderProfileOffersNum != null
                && lloOffersSection != null) {
            Log.d(TAG, "setValuesToViews() --> Provider name: "
                    + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerName);
            txtProviderProfileDesc.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.description);
            txtProviderProfileClinicName.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerName);
            txtProviderProfileCity.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.city);
            txtProviderProfileArea.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.area);
            if(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.address != null) {
                txtProviderProfileAddress.setText("" + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.address);
                imgProviderInfoLocation.setEnabled(true);
            }else {
                imgProviderInfoLocation.setEnabled(false);
            }
            txtProviderProfileGallaryNum.setText("(" + AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles.size() + " pics)");

            txtProviderProfileAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sweetAlertDialog = new SweetAlertDialog(getActivity());
                    sweetAlertDialog.setCanceledOnTouchOutside(true);
                    sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_address));
                    sweetAlertDialog.setContentText(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.address);
                    sweetAlertDialog.show();

                    Button btn = sweetAlertDialog.findViewById(R.id.confirm_button);
                    btn.setBackgroundColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
                }
            });

            imgProviderInfoLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getActivity() != null) {
                        //"geo:%f,%f"
                        // "geo:%f,%f?q="
                        // https://stackoverflow.com/questions/6205827/how-to-open-standard-google-map-application-from-my-application
                        // https://riptutorial.com/android/example/10912/open-google-map-with-specified-latitude--longitude
                        // https://stackoverflow.com/questions/15700971/open-google-maps-app-and-show-marker-on-it-from-my-own-apps-activity
                        // https://developers.google.com/maps/documentation/urls/android-intents
                        // https://developers.google.com/maps/documentation/android-sdk/intents
                        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.latitude
                                , AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.longitude);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        getActivity().startActivity(intent);
                    }
                }
            });

//            if(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == CLINICS_INDEX){
//                lloOffersSection.setVisibility(View.VISIBLE);
//            }else {
//                lloOffersSection.setVisibility(View.GONE);
//            }

//            if(AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == LABS_INDEX){
            if (AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList != null && AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.size() > 0) {
                lloProviderServices.setVisibility(View.VISIBLE);
            } else {
                lloProviderServices.setVisibility(View.GONE);
            }

            txtProviderNameTitle.setText(R.string.member_main_view_advanced_search_name_edittext);

//            if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == CLINICS_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_clinic_name);
//            } else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == SCAN_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_scan_name);
//            } else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == FITNESS_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_fitness_name);
//            } else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == PHYSIOTHERAPY_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_physiotherapy_name);
//            } else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == LABS_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_lab_name);
//            } else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == OPTICS_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_optic_name);
//            } else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == PHARMACY_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_pharmacy_name);
//            }else if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == HOSPITAL_INDEX) {
////                txtProviderNameTitle.setText(R.string.provider_hospital_name);
//            }
            txtProviderProfileOffersNum.setText("(" + AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.size() + " offer)");

            if (AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == ScanSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == OpticsSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == LabsSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == FitnessSysProviderType
                    || AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.systemProviderType == PharmacySysProviderType) {
                Log.d(TAG, "AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays size: " + AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays);
                if (AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays != null && AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays.size() > 0) {
                    lloOffDays.setVisibility(View.VISIBLE);
                    lloWorkingHours.setVisibility(View.GONE);

                    setDaysOff();

                } else {
                    lloOffDays.setVisibility(View.GONE);
                    lloWorkingHours.setVisibility(View.GONE);
                }
            } else {
                lloOffDays.setVisibility(View.GONE);
                lloWorkingHours.setVisibility(View.GONE);
            }

            loadRecyclerViewDate();
        }
    }

    private void setDaysOff() {
        resetDaysOffButtonsToNotActive(btnOffDaysSun);
        resetDaysOffButtonsToNotActive(btnOffDaysMon);
        resetDaysOffButtonsToNotActive(btnOffDaysTus);
        resetDaysOffButtonsToNotActive(btnOffDaysWed);
        resetDaysOffButtonsToNotActive(btnOffDaysThu);
        resetDaysOffButtonsToNotActive(btnOffDaysFri);
        resetDaysOffButtonsToNotActive(btnOffDaysSat);

        resetDaysToActiveState();

        setDaysOffButtonsActions();
    }

    private void resetDaysOffButtonsToNotActive(Button button) {
        button.setBackgroundResource(R.drawable.button_colored_bg_rounded_corners);
        button.setTextColor(Color.WHITE);
        button.setTag(0);
    }

    private void resetDaysToActiveState() {
        for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
            if (providerWorkinDays.dayOfWeek == 0) {
                setDaysOffButtonActive(btnOffDaysSun);
            } else if (providerWorkinDays.dayOfWeek == 1) {
                setDaysOffButtonActive(btnOffDaysMon);
            } else if (providerWorkinDays.dayOfWeek == 2) {
                setDaysOffButtonActive(btnOffDaysTus);
            } else if (providerWorkinDays.dayOfWeek == 3) {
                setDaysOffButtonActive(btnOffDaysWed);
            } else if (providerWorkinDays.dayOfWeek == 4) {
                setDaysOffButtonActive(btnOffDaysThu);
            } else if (providerWorkinDays.dayOfWeek == 5) {
                setDaysOffButtonActive(btnOffDaysFri);
            } else if (providerWorkinDays.dayOfWeek == 6) {
                setDaysOffButtonActive(btnOffDaysSat);
            }
        }
    }

    private void setDaysOffButtonActive(Button button) {
        button.setBackgroundResource(R.drawable.button_bg_rounded_dimmed_color_corners);
        button.setTextColor(getResources().getColor(R.color.axonTextDarkPurpleColor));
        button.setTag(1);
    }

    private void setDaysOffButtonsActions() {
        btnOffDaysSun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysSun.getTag() == 1) {
                    btnOffDaysSun.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 0) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });
        btnOffDaysMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysMon.getTag() == 1) {
                    btnOffDaysMon.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 1) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });
        btnOffDaysTus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysTus.getTag() == 1) {
                    btnOffDaysTus.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 2) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });
        btnOffDaysWed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysWed.getTag() == 1) {
                    btnOffDaysWed.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 3) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });
        btnOffDaysThu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysThu.getTag() == 1) {
                    btnOffDaysThu.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 4) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });
        btnOffDaysFri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysFri.getTag() == 1) {
                    btnOffDaysFri.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 5) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });
        btnOffDaysSat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDaysToActiveState();
                if ((int) btnOffDaysSat.getTag() == 1) {
                    btnOffDaysSat.setBackgroundResource(R.drawable.button_bg_rounded_colored_corners);
                    lloWorkingHours.setVisibility(View.VISIBLE);
                    for (ProviderProfile.ProviderWorkinDays providerWorkinDays : AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays) {
                        if (providerWorkinDays.dayOfWeek == 6) {
                            txtProviderProfileWorkingHours.setText(providerWorkinDays.dateFrom + " to " + providerWorkinDays.dateTo);
                            break;
                        }
                    }
                } else {
                    lloWorkingHours.setVisibility(View.GONE);
                }
            }
        });

    }

    private void loadRecyclerViewDate() {
        servicesAdapter = new MyServicesAdapter();
        recyclerViewProviderServices.setAdapter(servicesAdapter);

        galleryAdapter = new MyGalleryAdapter();
        recyclerViewProviderGallary.setAdapter(galleryAdapter);

        offersAdapter = new MyOffersAdapter();
        recyclerViewProviderOffers.setAdapter(offersAdapter);
    }

    class MyServicesAdapter extends RecyclerView.Adapter<MyServicesAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.provider_profile_custom_service, viewGroup, false);
            return new MyServicesAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyServicesAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);
            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row position: " + position);
            final ProviderProfile.ProviderServices currentItem = AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.get(position);

            TextView txtProviderProfileServiceTitle = holder.itemView.findViewById(R.id.txtProviderProfileServiceTitle);
            TextView txtProviderProfileServiceDiscount = holder.itemView.findViewById(R.id.txtProviderProfileServiceDiscount);
            TextView txtProviderProfileServiceDiscountBefore = holder.itemView.findViewById(R.id.txtProviderProfileServiceDiscountBefore);
            TextView txtProviderProfileServiceDiscountAfter = holder.itemView.findViewById(R.id.txtProviderProfileServiceDiscountAfter);

            txtProviderProfileServiceTitle.setText(currentItem.name);
            txtProviderProfileServiceDiscount.setText("-" + currentItem.discount + " %");
            txtProviderProfileServiceDiscountBefore.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + currentItem.price);
            txtProviderProfileServiceDiscountAfter.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + ((int) currentItem.priceAfterDiscount));
        }

        @Override
        public int getItemCount() {
            return AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    class MyGalleryAdapter extends RecyclerView.Adapter<MyGalleryAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyGalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.provider_profile_info_tab_gallary_custom_row, viewGroup, false);
            return new MyGalleryAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyGalleryAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);
            if (AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles.size() > 0) {
                final String currentItem = AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles.get(position);
                final ImageView imgProviderProfileImage = holder.itemView.findViewById(R.id.imgProviderProfileImage);

                Glide.with(getApplicationContext())
                        .load(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .fitCenter()
                        .into(imgProviderProfileImage);

                imgProviderProfileImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppCoreCode.getInstance().cache.currentImageSlider = "Gallery";
                        AppCoreCode.getInstance().cache.currentImageSliderIndex = position;
                        if (getActivity() != null)
                            ((MemberMainViewActivity) getActivity()).changeFragment(17);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    class MyOffersAdapter extends RecyclerView.Adapter<MyOffersAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyOffersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.provider_profile_info_tab_offers_custom_row, viewGroup, false);
            return new MyOffersAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyOffersAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);
            if (AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.size() > 0) {
                final ProviderProfile.ProviderOffers currentItem = AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.get(position);
                final SimpleDraweeView imgProviderProfileOfferImage = holder.itemView.findViewById(R.id.imgProviderProfileOfferImage);
                final TextView txtProviderProfileOfferDiscount = holder.itemView.findViewById(R.id.txtProviderProfileOfferDiscount);
                txtProviderProfileOfferDiscount.setText("-" + currentItem.discount + " %");

                Glide.with(getApplicationContext())
                        .load(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.avatar)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .fitCenter()
                        .into(imgProviderProfileOfferImage);

                imgProviderProfileOfferImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        String[] listimages = {AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.avatar};
//                        new ImageViewer.Builder(getActivity(), listimages)
//                                .show();
                        AppCoreCode.getInstance().cache.currentImageSlider = "Offers";
                        AppCoreCode.getInstance().cache.currentImageSliderIndex = position;
                        if (getActivity() != null)
                            ((MemberMainViewActivity) getActivity()).changeFragment(17);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }
}

package com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR;

public class BookingNotification {
    public int providerId;
    public String reservationDate;
    public String timeFrom;
    public String timeTo;
}

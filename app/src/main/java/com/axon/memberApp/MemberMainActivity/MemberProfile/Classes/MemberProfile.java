package com.axon.memberApp.MemberMainActivity.MemberProfile.Classes;

public class MemberProfile {
    public int memberId;
    public String memberCode;
    public String memberName;
    public String mobile;
    public String email;
    public String gender;
    public String birthDate;
    public String avatarUrl;
    public String corporateName;
    public boolean hasFinishedTransaction;
    public int totalSavedAmount;

    public MemberShip memberShip = new MemberShip();

    public class MemberShip {
        public int memberShipId;
        public String membershipCode;
        public String startDate;
        public String endDate;
        public int membershipStatus;
        public String membershipStatusName;
        public String address;
        public Integer countryId;
        public String countryName;
        public Integer cityId;
        public String cityName;
        public Integer areaId;
        public String areaName;
        public boolean isPhotoExists;
    }
}

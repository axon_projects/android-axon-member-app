package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments;


import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.Favorites;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {
    private View view;
    private RecyclerView recyclerViewFavorites;
    private RecyclerView.Adapter adapter;

    private AsyncTaskClass asyncTaskClass;
    private String favoritesResponse;
    private String TAG = "FavoriteFragment";

    private AsyncTaskProviderProfileClass asyncTaskProviderProfileClass;
    private String providerProfileResponse;

    private FavoriteAsyncTaskClass favoriteAsyncTaskClass;
    private String favoriteResponse;
    private boolean success;

    public FavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.favorite_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null) {
            initViews();
        }
    }

    private void initViews() {
        recyclerViewFavorites = view.findViewById(R.id.recyclerViewFavorites);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewFavorites.setHasFixedSize(true);
        recyclerViewFavorites.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        loadRecyclerViewDate();
        getFavoritesFromServer();
    }

    private void getFavoritesFromServer() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    void loadRecyclerViewDate() {
        adapter = new MyAdapter();
        recyclerViewFavorites.setAdapter(adapter);
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.favorite_custom_row, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);

            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row position: " + position);
            final Favorites.Result currentItem = AppCoreCode.getInstance().cache.favorites.result.get(position);

            TextView txtFavoriteProviderName = holder.itemView.findViewById(R.id.txtFavoriteProviderName);
            TextView txtFavoriteProviderType = holder.itemView.findViewById(R.id.txtFavoriteProviderType);
            TextView txtFavoriteProviderSpeciality = holder.itemView.findViewById(R.id.txtFavoriteProviderSpeciality);
            LinearLayout lloFavoriteBookNow = holder.itemView.findViewById(R.id.lloFavoriteBookNow);
            RatingBar rateBarFavoriteProviderRate = holder.itemView.findViewById(R.id.rateBarFavoriteProviderRate);
            final ImageButton btnFavorite = holder.itemView.findViewById(R.id.ibtnFavorite);
            Button btnFavoriteProviderBook = holder.itemView.findViewById(R.id.btnFavoriteProviderBook);
            final CircleImageView imgFavoriteProviderImage = holder.itemView.findViewById(R.id.imgFavoriteProviderImage);

            txtFavoriteProviderName.setText(currentItem.providerName);
            txtFavoriteProviderType.setText(currentItem.providerTypeName);
            txtFavoriteProviderSpeciality.setText(currentItem.specializationName);
            rateBarFavoriteProviderRate.setRating((float) currentItem.rate);

            if (!currentItem.avatar.contains("nophoto-slider")) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        final Drawable drawable = Tools.LoadImageFromWebOperations(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.avatar);
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imgFavoriteProviderImage.setImageDrawable(drawable);
                                }
                            });
                        }
                    }
                });
            }

            if (currentItem.systemProviderType == 0) {
                btnFavoriteProviderBook.setText(getResources().getString(R.string.member_main_view_quick_search_screen_book_now_button));
            } else {
                btnFavoriteProviderBook.setText(getResources().getString(R.string.member_main_view_quick_search_screen_more_detail_button));
            }

            if (currentItem.isInFavorite) {
                btnFavorite.setTag(R.drawable.quick_search_result_active_favorite);
                btnFavorite.setImageResource(R.drawable.quick_search_result_active_favorite);
            } else {
                btnFavorite.setTag(R.drawable.quick_search_result_inactive_favorite);
                btnFavorite.setImageResource(R.drawable.quick_search_result_inactive_favorite);
            }

            btnFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((int) btnFavorite.getTag() == R.drawable.quick_search_result_active_favorite) {
                        btnFavorite.setImageResource(R.drawable.quick_search_result_inactive_favorite);
                        btnFavorite.setTag(R.drawable.quick_search_result_inactive_favorite);
                    } else {
                        btnFavorite.setImageResource(R.drawable.quick_search_result_active_favorite);
                        btnFavorite.setTag(R.drawable.quick_search_result_active_favorite);
                    }

                    AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                    setUserFavorite();
                }
            });

            btnFavoriteProviderBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                    AppCoreCode.getInstance().cache.currentSelectedProviderTypeFromSearchResults = currentItem.providerTypeName;
                    getProviderProfileFromServer();
                }
            });

            lloFavoriteBookNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                    AppCoreCode.getInstance().cache.currentSelectedProviderTypeFromSearchResults = currentItem.providerTypeName;
                    getProviderProfileFromServer();
                }
            });

        }

        @Override
        public int getItemCount() {
            if (AppCoreCode.getInstance().cache.favorites != null && AppCoreCode.getInstance().cache.favorites.result != null) {
                return AppCoreCode.getInstance().cache.favorites.result.size();
            } else {
                return 0;
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    private void setUserFavorite() {
        favoriteAsyncTaskClass = new FavoriteAsyncTaskClass();
        favoriteAsyncTaskClass.execute();
    }

    private class FavoriteAsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                favoriteResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/favoriteupsert", "{\"id\": " + AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults + "}");

                Log.d(TAG, "doInBackground() --> searchResultsResponse: " + favoriteResponse);
                JSONObject reader = new JSONObject(favoriteResponse);
                success = reader.getBoolean("success");
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            getFavoritesFromServer();
        }
    }

    private void getProviderProfileFromServer() {
        asyncTaskProviderProfileClass = new AsyncTaskProviderProfileClass();
        asyncTaskProviderProfileClass.execute();
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                favoritesResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getfavoritelist", "");
                Log.d(TAG, "doInBackground() --> favoritesResponse: " + favoritesResponse);
//                JSONObject reader = new JSONObject();
                AppCoreCode.getInstance().cache.favorites = AppCoreCode.getInstance().cache.gson.fromJson(favoritesResponse, Favorites.class);

//                String itemsArray = reader.getJSONArray("result").toString();
//                AppCoreCode.getInstance().cache.favorites.result = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<Favorites.Result>>() {}.getType());
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            adapter.notifyDataSetChanged();
        }
    }

    private class AsyncTaskProviderProfileClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                providerProfileResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getproviderinfo"
                        , "{\"id\": " + AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults + "}");

                Log.d(TAG, "doInBackground() --> providerProfileResponse: " + providerProfileResponse);
                JSONObject reader = new JSONObject(providerProfileResponse);
                AppCoreCode.getInstance().cache.providerProfile = AppCoreCode.getInstance().cache.gson.fromJson(providerProfileResponse, ProviderProfile.class);
                Log.d(TAG, "doInBackground() --> providerProfile: " + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData);

                String providerProfileData = reader.getJSONObject("result").getJSONObject("providerProfileData").toString();
                Log.d(TAG, "doInBackground() --> providerProfileData: " + providerProfileData);
                // TODO: Checking for null cause Network delay
                if (AppCoreCode.getInstance().cache.providerProfile != null && AppCoreCode.getInstance().cache.providerProfile.result != null) {
                    AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData = AppCoreCode.getInstance().cache.gson.fromJson(providerProfileData, ProviderProfile.ProviderProfileData.class);

                    Log.d(TAG, "doInBackground() --> mediaFiles: " + AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles);

                    String providerServices = reader.getJSONObject("result").getJSONArray("providerServices").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList = AppCoreCode.getInstance().cache.gson.fromJson(providerServices, new TypeToken<List<ProviderProfile.ProviderServices>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerServicesList: " + AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList);

                    String providerOffers = reader.getJSONObject("result").getJSONArray("providerOffers").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList = AppCoreCode.getInstance().cache.gson.fromJson(providerOffers, new TypeToken<List<ProviderProfile.ProviderOffers>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerOffers: " + AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList);

                    String providerSpecializations = reader.getJSONObject("result").getJSONArray("providerSpecializations").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerSpecializationsList = AppCoreCode.getInstance().cache.gson.fromJson(providerSpecializations, new TypeToken<List<ProviderProfile.ProviderSpecializations>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerSpecializations: " + AppCoreCode.getInstance().cache.providerProfile.result.providerSpecializationsList);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO: Checking for null cause Network delay
            if (AppCoreCode.getInstance().cache.providerProfile != null
                    && AppCoreCode.getInstance().cache.providerProfile.result != null
                    && AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData != null) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(15);
            }
        }
    }
}

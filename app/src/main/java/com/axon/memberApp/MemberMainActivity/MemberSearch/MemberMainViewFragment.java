package com.axon.memberApp.MemberMainActivity.MemberSearch;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberNotification.Classes.Notification;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberMainViewFragment extends Fragment {
    private View view;
    private EditText etxtQuickSearch;
    private ImageButton ibtnSideMenu, ibtnNotification, ibtnSearchNearby, ibtnClinic, ibtnHospital
            , ibtnScanCenter, ibtnLab, ibtnPharmacy, ibtnPhysiotherapy, ibtnWellnessFitness, ibtnOptics;

    private String TAG = "MemberMainViewFragment";

    public MemberMainViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.main_view_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Log.d(TAG, "setUserVisibleHint() --> isVisibleToUser: " + isVisibleToUser + " - isAdded(): " + isAdded() + " - getActivity(): " + getActivity() + " - view: " + view);
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null && isAdded() && view != null) {
            initViews();
        }
    }

    private void initViews() {
        etxtQuickSearch = view.findViewById(R.id.etxtQuickSearch);

        ibtnSideMenu = view.findViewById(R.id.ibtnSideMenu);
        ibtnNotification = view.findViewById(R.id.ibtnNotification);

        ibtnSearchNearby = view.findViewById(R.id.ibtnSearchNearby);
        ibtnClinic = view.findViewById(R.id.ibtnClinic);
        ibtnHospital = view.findViewById(R.id.ibtnHospital);
        ibtnScanCenter = view.findViewById(R.id.ibtnScanCenter);
        ibtnLab = view.findViewById(R.id.ibtnLab);
        ibtnPharmacy = view.findViewById(R.id.ibtnPharmacy);
        ibtnPhysiotherapy = view.findViewById(R.id.ibtnPhysiotherapy);
        ibtnWellnessFitness = view.findViewById(R.id.ibtnWellnessFitness);
        ibtnOptics = view.findViewById(R.id.ibtnOptics);

        checkIfUserGotNotification();
        setViewsActions();
    }

    private void checkIfUserGotNotification() {
        if (AppCoreCode.getInstance().cache.notification != null && AppCoreCode.getInstance().cache.notification.result != null) {
            for (Notification.Result result : AppCoreCode.getInstance().cache.notification.result) {
                if (result.state == 0) {
                    ibtnNotification.setImageResource(R.drawable.notification_numberd_icon);
                    break;
                } else {
                    ibtnNotification.setImageResource(R.drawable.notification_icon);
                }
            }
        }
    }

    private void setViewsActions() {
        etxtQuickSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (etxtQuickSearch.getText().toString().trim().isEmpty()) {
                        AppCoreCode.getInstance().cache.strQuickSearchProviderName = null;
                    } else {
                        AppCoreCode.getInstance().cache.strQuickSearchProviderName = "\"" + etxtQuickSearch.getText().toString().trim() + "\"";
                        etxtQuickSearch.setText(null);
                    }

                    Log.d(TAG, "setViewsActions() --> etxtQuickSearch: " + etxtQuickSearch.getText().toString());

                    if (getActivity() != null)
                        ((MemberMainViewActivity) getActivity()).changeFragment(1);

                }
                return false;
            }
        });

        etxtQuickSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etxtQuickSearch.getText().toString().trim().isEmpty()) {
                    AppCoreCode.getInstance().cache.strQuickSearchProviderName = null;
                } else {
                    AppCoreCode.getInstance().cache.strQuickSearchProviderName = "\"" + etxtQuickSearch.getText().toString().trim() + "\"";
                    etxtQuickSearch.setText(null);
                }

                Log.d(TAG, "setViewsActions() --> etxtQuickSearch: " + etxtQuickSearch.getText().toString());

                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(1);
            }
        });

        ibtnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(4);
            }
        });

        ibtnSideMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).drawer.openDrawer(Gravity.START);
            }
        });

        ibtnSearchNearby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 0;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnClinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 1;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 8;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnScanCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 6;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 7;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnPharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 4;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnPhysiotherapy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 2;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnWellnessFitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 3;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });

        ibtnOptics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().memberMainViewActivity.currentAdvancedSearch = 5;
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });
    }
}

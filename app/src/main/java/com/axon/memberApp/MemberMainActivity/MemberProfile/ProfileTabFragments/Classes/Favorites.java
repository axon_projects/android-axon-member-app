package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes;


import java.util.List;

public class Favorites {
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public Error error = new Error();
    public List<Result> result;

    public class Result {
        public int providerId;
        public String providerName;
        public String description;
        public String mobileNumber;
        public String landLine;
        public String phone1;
        public String phone2;
        public String hotLine;
        public String country;
        public String city;
        public String area;
        public String address;
        public String addressOnMap;
        public String avatar;
        public double longitude;
        public double latitude;
        public double rate;
        public int systemProviderType;
        public String providerTypeName;
        public String doctorName;
        public String doctorTitleName;
        public String specializationName;
        public int bookingWay;
        public int bookingSystem;
        public int waitingTime;
        public boolean isInFavorite;
    }

    public class Error {
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }
}

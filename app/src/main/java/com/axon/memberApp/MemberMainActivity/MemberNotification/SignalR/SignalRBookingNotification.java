package com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

public class SignalRBookingNotification {
    HubConnection hubConnection;
    HubProxy hubProxy;
    Handler handler = new Handler();
    public Context context;
    String state = "NotConnected";
    String TAG = "SignalRConnector";

    public NotificationManager mNotificationManager;
    private static String DEFAULT_CHANNEL_ID = "default_channel";
    private static String DEFAULT_CHANNEL_NAME = "Default";

//    String bearerToken = "Bearer " + AppCoreCode.getInstance().cache.accessToken;

    /**
     * Signalr
     */
    public void connect() {
        Log.d(TAG, "connect() called");
//        Platform.loadPlatformComponent(new AndroidPlatformComponent());
        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(Request request) {
                if (AppCoreCode.getInstance().cache != null && AppCoreCode.getInstance().cache.accessToken != null)
                    request.addHeader("Authorization", "Bearer " + AppCoreCode.getInstance().cache.accessToken);
            }
        };
        hubConnection = new HubConnection(AppCoreCode.getInstance().cache.mainURL);
        hubConnection.setCredentials(credentials);
        hubConnection.connected(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "connect() --> Connected to: " + AppCoreCode.getInstance().cache.mainURL);
                        state = "Connected";
                    }
                });
            }
        });

        hubProxy = hubConnection.createHubProxy("reservationRealTimeNotifierHub");
        ClientTransport clientTransport = new ServerSentEventsTransport(hubConnection.getLogger());
        SignalRFuture<Void> signalRFuture = hubConnection.start(clientTransport);

        hubProxy.on("notify", new SubscriptionHandler1<Object>() {
            @Override
            public void run(Object json) {
                Log.d(TAG, "Json response: " + json);
                try {
                    // Solve Object not taken Json format(shape)
                    JSONObject jsonObject = Tools.decodeResult(json);
                    Log.d(TAG, "Message Received --> jsonObject " + jsonObject.toString());

                    AppCoreCode.getInstance().cache.bookingNotification = AppCoreCode.getInstance().cache.gson.fromJson(jsonObject.toString(), BookingNotification.class);

                    Log.d(TAG, "Message Received --> isBookingTabFragmentIsVisible: " + AppCoreCode.getInstance().cache.isBookingTabFragmentIsVisible);
                    Log.d(TAG, "Message Received --> providerProfileBookingTabFragment: " + AppCoreCode.getInstance().cache.providerProfileBookingTabFragment);
                    Log.d(TAG, "Message Received --> providerId: " + (AppCoreCode.getInstance().cache.bookingNotification.providerId == AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerId));
                    Log.d(TAG, "Message Received --> reservationDate: " + (AppCoreCode.getInstance().cache.bookingNotification.reservationDate.equalsIgnoreCase(AppCoreCode.getInstance().cache.providerProfileBookingTabFragment.reservationStartDate)));

                    if (AppCoreCode.getInstance().cache.isBookingTabFragmentIsVisible) {
                        if (AppCoreCode.getInstance().cache.providerProfileBookingTabFragment != null) {
                            if (AppCoreCode.getInstance().cache.bookingNotification.providerId == AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData.providerId) {
                                if (AppCoreCode.getInstance().cache.bookingNotification.reservationDate.equalsIgnoreCase(AppCoreCode.getInstance().cache.providerProfileBookingTabFragment.reservationStartDate)) {
                                    AppCoreCode.getInstance().cache.providerProfileBookingTabFragment.currentSelectTime = "";
                                    AppCoreCode.getInstance().cache.providerProfileBookingTabFragment.getProviderReservationFromServer("GetTimeFromSignalR");
                                    Log.d(TAG, "Message Received --> GetTimeFromSignalR");
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, Object.class);
        try {
            signalRFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reinit
     */
    public void disconnect() {
        if (state.equalsIgnoreCase("Connected")) {
            state = "NotConnected";
            hubConnection.stop();
        }
    }
}

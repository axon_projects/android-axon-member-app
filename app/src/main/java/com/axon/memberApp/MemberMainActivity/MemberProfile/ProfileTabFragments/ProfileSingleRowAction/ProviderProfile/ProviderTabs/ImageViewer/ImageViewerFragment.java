package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.ImageViewer;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageViewerFragment extends Fragment {
    private View view;
    private ViewPager viewPager;
    private TextView txtDescription;
    private ViewPagerAdapter adapter;
    private String TAG = "ImageViewerFragment";

    public ImageViewerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.provider_profile_image_viewer_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null) {
            initViews();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initViews();
    }

    private void initViews() {
        viewPager = view.findViewById(R.id.viewPagerImageViewer);
        txtDescription = view.findViewById(R.id.txtDescription);

        setViewActions();
    }

    private void setupViewPager() {
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(AppCoreCode.getInstance().cache.currentImageSliderIndex);
        Log.d(TAG, "setupViewPager() --> current index: " + AppCoreCode.getInstance().cache.currentImageSliderIndex);
    }

    private void setViewActions() {
        if (AppCoreCode.getInstance().cache.currentImageSlider.equalsIgnoreCase("Gallery")) {
            txtDescription.setVisibility(View.GONE);
            adapter = new ViewPagerAdapter(getActivity(), AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles, null);
        } else if (AppCoreCode.getInstance().cache.currentImageSlider.equalsIgnoreCase("Offers")) {
            txtDescription.setVisibility(View.VISIBLE);
            adapter = new ViewPagerAdapter(getActivity(), AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList, txtDescription);
            if (AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.size() > 0
                    && AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.get(AppCoreCode.getInstance().cache.currentImageSliderIndex) != null) {
                txtDescription.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.get(AppCoreCode.getInstance().cache.currentImageSliderIndex).description);
            }

            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    if (AppCoreCode.getInstance().cache.providerProfile.result != null
                            && AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList != null
                            && AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.size() > 0) {
                        txtDescription.setText(AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList.get(i).description);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        }
        setupViewPager();
    }
}

package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes;

import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.Classes.CancellationReasons;

import java.util.ArrayList;
import java.util.List;

public class ProviderProfile {
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public ProviderProfile.Error error = new ProviderProfile.Error();
    public Result result;

    public class Error {
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }

    public class Result {
        public ProviderProfileData providerProfileData;
        public List<String> mediaFiles;
        public List<ProviderServices> providerServicesList;
        public List<ProviderOffers> providerOffersList;
        public List<ProviderSpecializations> providerSpecializationsList;
        public List<ProviderWorkinDays> providerWorkinDays;
    }

    public class ProviderProfileData {
        public int providerId;
        public String providerName;
        public String description;
        public String country;
        public String city;
        public String area;
        public String address;
        public double longitude;
        public double latitude;
        public String avatar;
        public double rate;
        public int systemProviderType;
        public String providerTypeName;
        public String doctorName;
        public String doctorTitleName;
        public String specializationName;
        public String bookingWay;
        public String bookingSystem;
        public String waitingTime;
    }

    public class ProviderServices{ // List
        public int id;
        public String name;
        public double price;
        public double discount;
        public double priceAfterDiscount;
    }

    public class ProviderOffers{ // List
        public int id;
        public String avatar;
        public String avatarName;
        public String description;
        public double discount;
    }

    public class ProviderSpecializations{ // List
        public int id;
        public String name;
    }

    public class ProviderWorkinDays{
        public int id;
        public int dayOfWeek;
        public String dateFrom;
        public String dateTo;
    }
}

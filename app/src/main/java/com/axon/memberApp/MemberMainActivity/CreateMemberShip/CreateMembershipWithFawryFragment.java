package com.axon.memberApp.MemberMainActivity.CreateMemberShip;


import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.FetchPath;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.Membership;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

import org.json.JSONObject;

import java.net.URLEncoder;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateMembershipWithFawryFragment extends Fragment {
    private View view;
    private ImageButton ibtnBack;
    private Button btnPayNow;
    private TextView txtMembershipWithFawryFees, txtMembershipWithFawryDiscount, txtMembershipWithFawryTotalFees, txtMembershipWithFawryCardDate;
    private ProgressBar progressBarCreateMembershipWithFawry;

    private String TAG = "CreateMembershipWithFawryFragment";
    private String membership;
    private AsyncTaskClass asyncTaskClass;
    private UploadImageAsyncTaskClass uploadImageAsyncTaskClass;

    public CreateMembershipWithFawryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.create_membership_with_fawry_fragment, container, false);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null && isAdded() && view != null) {
            initViews();
        }
    }

    private void initViews() {
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnPayNow = view.findViewById(R.id.btnPayNow);

        progressBarCreateMembershipWithFawry = view.findViewById(R.id.progressBarCreateMembershipWithFawry);
        progressBarCreateMembershipWithFawry.setVisibility(View.INVISIBLE);

        txtMembershipWithFawryFees = view.findViewById(R.id.txtMembershipWithFawryFees);
        txtMembershipWithFawryDiscount = view.findViewById(R.id.txtMembershipWithFawryDiscount);
        txtMembershipWithFawryTotalFees = view.findViewById(R.id.txtMembershipWithFawryTotalFees);
        txtMembershipWithFawryCardDate = view.findViewById(R.id.txtMembershipWithFawryCardDate);

        setViewsValue();
        setViewsActions();
    }

    private void sendDataToServer() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void setViewsValue() {
        if (AppCoreCode.getInstance().cache.membershipFeesInfo != null && AppCoreCode.getInstance().cache.membershipFeesInfo.result != null) {
            txtMembershipWithFawryFees.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.membershipFeesInfo.result.membershipFees);
            txtMembershipWithFawryDiscount.setText("-" + getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.membershipFeesInfo.result.membershipDiscount);
            txtMembershipWithFawryTotalFees.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.membershipFeesInfo.result.totalMembershipFees);
            txtMembershipWithFawryCardDate.setText(AppCoreCode.getInstance().cache.membershipFeesInfo.result.cardDeliveryDate);
        }
    }

    private void setViewsActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(9);
            }
        });

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataToServer();
            }
        });
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            progressBarCreateMembershipWithFawry.setVisibility(View.VISIBLE);
            btnPayNow.setEnabled(false);
            btnPayNow.setAlpha((float) 0.5);
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                membership = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/createmembership"
                        , AppCoreCode.getInstance().cache.membershipJsonToServer);

                Log.d(TAG, "doInBackground() --> membership: " + membership);
                JSONObject reader = new JSONObject(membership);
                AppCoreCode.getInstance().cache.membership = AppCoreCode.getInstance().cache.gson.fromJson(reader.toString(), Membership.class);

            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "onPostExecute() --> membership success: " + AppCoreCode.getInstance().cache.membership.success);
            if (AppCoreCode.getInstance().cache.membership.success) {
                sendimageToServer();
            }
        }
    }

    private void sendimageToServer() {
        uploadImageAsyncTaskClass = new UploadImageAsyncTaskClass();
        uploadImageAsyncTaskClass.execute();
    }

    private class UploadImageAsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String imageResponse = AppCoreCode.getInstance().httpRest.POST_Data(FetchPath.getPath(getActivity(), AppCoreCode.getInstance().cache.selectedImage));
                Log.d(TAG, "doInBackground() --> imageResponse: " + imageResponse);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressBarCreateMembershipWithFawry.setVisibility(View.INVISIBLE);
            btnPayNow.setEnabled(true);
            btnPayNow.setAlpha((float) 1);
            AppCoreCode.getInstance().cache.fawryPaymentSource = "CreateMembership";

//            if (getActivity() != null)
//                ((MemberMainViewActivity) getActivity()).changeFragment(17);

            if (getActivity() != null) {
                try {
                    openFawryURL();
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "No application can handle this request."
                            + " Please install a webbrowser", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    private void openFawryURL() {
        if (AppCoreCode.getInstance().cache.fawryPaymentSource != null && AppCoreCode.getInstance().cache.fawryPaymentSource.equalsIgnoreCase("ProfileSetting")) {
            if (AppCoreCode.getInstance().cache.validMembership != null && AppCoreCode.getInstance().cache.validMembership.result != null && AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl != null) {
                AppCoreCode.getInstance().cache.isBrowserOpened = true;
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppCoreCode.getInstance().cache.validMembership.result.requestMessage.fawryPaymentUrl + "&access_token=" + URLEncoder.encode(AppCoreCode.getInstance().cache.accessToken))));
            }
        } else if (AppCoreCode.getInstance().cache.fawryPaymentSource.equalsIgnoreCase("CreateMembership")) {
            if (AppCoreCode.getInstance().cache.membership != null && AppCoreCode.getInstance().cache.membership.result != null && AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl != null) {
                AppCoreCode.getInstance().cache.isBrowserOpened = true;
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl + "&access_token=" + URLEncoder.encode(AppCoreCode.getInstance().cache.accessToken))));
            }
        }
        if (AppCoreCode.getInstance().cache.membership != null && AppCoreCode.getInstance().cache.membership.result != null && AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl != null) {
            AppCoreCode.getInstance().cache.isBrowserOpened = true;
            getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppCoreCode.getInstance().cache.membership.result.fawryPaymentUrl + "&access_token=" + URLEncoder.encode(AppCoreCode.getInstance().cache.accessToken))));
        }
//        if (getActivity() != null)
//            ((MemberMainViewActivity) getActivity()).changeFragment(11);
    }
}

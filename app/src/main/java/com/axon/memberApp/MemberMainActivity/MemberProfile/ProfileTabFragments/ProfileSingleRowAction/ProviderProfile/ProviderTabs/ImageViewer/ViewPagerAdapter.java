package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.ImageViewer;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private List imageUrls;
    private String singleImageURL;
    private TextView textView;
    private String TAG = "ViewPagerAdapter";

    ViewPagerAdapter(Context context, List imageUrls, TextView textView) {
        this.context = context;
        this.imageUrls = imageUrls;
        this.textView = textView;
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        TouchImageView imageView = new TouchImageView(context);
        if (AppCoreCode.getInstance().cache.currentImageSlider.equalsIgnoreCase("Offers")) {
            singleImageURL = ((ProviderProfile.ProviderOffers) imageUrls.get(position)).avatar;
        } else {
            singleImageURL = imageUrls.get(position).toString();
        }

        Glide.with(getApplicationContext())
                .load(AppCoreCode.getInstance().cache.mainURL + "/" + singleImageURL)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .fitCenter()
                .into(imageView);
        container.addView(imageView);

        if (textView != null)
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "instantiateItem() --> txtDescription getVisibility: " + textView.getVisibility());
                    if (textView.getVisibility() == View.GONE) {
                        textView.setVisibility(View.VISIBLE);
                    } else if (textView.getVisibility() == View.VISIBLE) {
                        textView.setVisibility(View.GONE);
                    }
                }
            });

        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }


}

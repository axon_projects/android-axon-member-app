package com.axon.memberApp.MemberMainActivity.MemberSearch;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.AdvancedSearch;
import com.axon.memberApp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvancedSearchResultsWithEditFragment extends Fragment {
    private RecyclerView recyclerViewSearchResults;
    private RecyclerView.Adapter adapter;
    private View view;
    private ImageButton ibtnBack; //, ibtnTopEditSearch;
    private TextView txtSearchTitle, txtBookingServiceFees;
    private Button btnEditSearch;
    private String TAG = "AdvancedSearchResultsWithEditFragment";

    private ProgressBar progressBarAdvancedSearch;
    private int pageNumber;
    private int totalPages;
    private String searchResultsResponse;
    private String favoriteResponse;
    private AsyncTaskClass asyncTaskClass;
    private AsyncTaskProviderProfileClass asyncTaskProviderProfileClass;
    private FavoriteAsyncTaskClass favoriteAsyncTaskClass;
    private boolean success;

    private String providerProfileResponse;

    public AdvancedSearchResultsWithEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null && isAdded() && view != null) {
            Log.d(TAG, "setUserVisibleHint() --> isVisibleToUser: " + isVisibleToUser);
            initViews();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.advanced_search_results_with_edit_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        pageNumber = 1;
        if (AppCoreCode.getInstance().cache.advancedSearch != null) {
            totalPages = AppCoreCode.getInstance().cache.advancedSearch.totalPages;
        }

        progressBarAdvancedSearch = view.findViewById(R.id.progressBarAdvancedSearch);
        ibtnBack = view.findViewById(R.id.ibtnBack);
//        ibtnTopEditSearch = view.findViewById(R.id.ibtnTopEditSearch);
        txtSearchTitle = view.findViewById(R.id.txtSearchTitle);
        txtBookingServiceFees = view.findViewById(R.id.txtBookingServiceFees);
        btnEditSearch = view.findViewById(R.id.btnEditSearch);
        recyclerViewSearchResults = view.findViewById(R.id.recyclerViewSearchResults);
// setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewSearchResults.setHasFixedSize(true);

        loadRecyclerViewDate();
        setViewsActions();

        changeSearchLogo(AppCoreCode.getInstance().memberMainViewActivity.currentSelectedProviderType);
    }

    private void setViewsActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (AppCoreCode.getInstance().cache.advancedSearch != null && AppCoreCode.getInstance().cache.advancedSearch.itemsList != null) {
//                    adapter.notifyItemRangeRemoved(0, AppCoreCode.getInstance().cache.advancedSearch.itemsList.size() - 1);
//                }
                if (progressBarAdvancedSearch.getVisibility() == View.GONE) {
                    if (getActivity() != null)
                        ((MemberMainViewActivity) getActivity()).changeFragment(2);
                }
            }
        });

//        recyclerViewSearchResults.setOnTouchListener(
//                new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View v, MotionEvent event) {
//                        if (progressBarAdvancedSearch.getVisibility() == View.VISIBLE) {
//                            return false;
//                        } else {
//                            return true;
//                        }
//                    }
//                }
//        );

//        ibtnTopEditSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (getActivity() != null)
//                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
////                showCustomEditSearchDialog();
//            }
//        });

        btnEditSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(2);
            }
        });
    }

    private void showCustomEditSearchDialog() {
        final Dialog customEditSearchDialog = new Dialog(getActivity());
        customEditSearchDialog.setContentView(R.layout.edit_search_results_custom_dialog);
        customEditSearchDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageButton ibtnClose = customEditSearchDialog.findViewById(R.id.ibtnClose);
        Button ibtnDone = customEditSearchDialog.findViewById(R.id.ibtnDone);

        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customEditSearchDialog.dismiss();
            }
        });

        ibtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customEditSearchDialog.dismiss();
            }
        });

        customEditSearchDialog.show();
    }

    private void changeSearchLogo(int indexOfSelectSearch) {
        txtBookingServiceFees.setText(getResources().getString(R.string.member_main_view_advanced_search_title));
        if (indexOfSelectSearch == 0) {
            txtBookingServiceFees.setText(getResources().getString(R.string.member_main_view_quick_search));
            txtSearchTitle.setText("");
        } else if (indexOfSelectSearch == 1) {
            txtSearchTitle.setText(R.string.advanc_clinic_type);
        } else if (indexOfSelectSearch == 8) {
            txtSearchTitle.setText(R.string.advanc_hospital_type);
        } else if (indexOfSelectSearch == 6) {
            txtSearchTitle.setText(R.string.advanc_scan_type);
        } else if (indexOfSelectSearch == 7) {
            txtSearchTitle.setText(R.string.advanc_lab_type);
        } else if (indexOfSelectSearch == 4) {
            txtSearchTitle.setText(R.string.advanc_pharmacy_type);
        } else if (indexOfSelectSearch == 2) {
            txtSearchTitle.setText(R.string.advanc_physiotherapy_type);
        } else if (indexOfSelectSearch == 3) {
            txtSearchTitle.setText(R.string.advanc_wellness_type);
        } else if (indexOfSelectSearch == 5) {
            txtSearchTitle.setText(R.string.advanc_optics_type);
        }
    }

    void loadRecyclerViewDate() {
        try {
            adapter = new MyAdapter();
            if (getActivity() != null)
                recyclerViewSearchResults.setLayoutManager(new LinearLayoutManager(getActivity())); //, RecyclerView.VERTICAL, false) // WrapContentLinearLayoutManager
            recyclerViewSearchResults.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProviderProfileFromServer() {
        asyncTaskProviderProfileClass = new AsyncTaskProviderProfileClass();
        asyncTaskProviderProfileClass.execute();
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.quick_search_result_card_layout, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);

            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row position: " + position);

            if (AppCoreCode.getInstance().cache.advancedSearch != null && AppCoreCode.getInstance().cache.advancedSearch.itemsList != null) {
                final AdvancedSearch.Items currentItem = AppCoreCode.getInstance().cache.advancedSearch.itemsList.get(position);
                if (currentItem != null) {
                    RatingBar ratingBarProviderRate = holder.itemView.findViewById(R.id.ratingBarProviderRate);
                    LinearLayout lloBookNow = holder.itemView.findViewById(R.id.lloBookNow);
                    Button btnAdvancedSearchResultBookNow = holder.itemView.findViewById(R.id.btnAdvancedSearchResultBookNow);
                    final ImageButton btnAdvancedSearchResultFavorite = holder.itemView.findViewById(R.id.btnAdvancedSearchResultFavorite);
                    final CircleImageView imgAdvancedSearchProviderAvater = holder.itemView.findViewById(R.id.imgAdvancedSearchProviderAvater);
                    TextView txtAdvancedSearchDrName = holder.itemView.findViewById(R.id.txtAdvancedSearchDrName);
                    TextView txtAdvancedSearchProviderTypeName = holder.itemView.findViewById(R.id.txtAdvancedSearchProviderTypeName);
                    TextView txtAdvancedSearchSpecialization = holder.itemView.findViewById(R.id.txtAdvancedSearchSpecialization);


                    Log.d(TAG, "MyAdapter --> onBindViewHolder() --> AdvancedSearch rate: " + currentItem.rate);
                    ratingBarProviderRate.setRating(currentItem.rate);

                    if (!currentItem.avatar.contains("nophoto-slider")) {
                        Glide.with(getApplicationContext())
                                .load(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.avatar)
                                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                .override(100, 100)
                                .into(imgAdvancedSearchProviderAvater);
                    }

                    if (currentItem.isInFavorite) {
                        btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_active_favorite);
                        btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_active_favorite);
                    } else {
                        btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_inactive_favorite);
                        btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_inactive_favorite);
                    }

                    btnAdvancedSearchResultFavorite.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if ((int) btnAdvancedSearchResultFavorite.getTag() == R.drawable.quick_search_result_active_favorite) {
                                btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_inactive_favorite);
                                btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_inactive_favorite);
                            } else {
                                btnAdvancedSearchResultFavorite.setImageResource(R.drawable.quick_search_result_active_favorite);
                                btnAdvancedSearchResultFavorite.setTag(R.drawable.quick_search_result_active_favorite);
                            }

                            AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                            setUserFavorite();
                        }
                    });

                    txtAdvancedSearchDrName.setText(currentItem.providerName);
                    Tools.makeTextMarquee(txtAdvancedSearchDrName);
                    txtAdvancedSearchProviderTypeName.setText(currentItem.providerTypeName);
                    txtAdvancedSearchSpecialization.setText(currentItem.specializationName);

                    if (currentItem.systemProviderType == 0) {
                        btnAdvancedSearchResultBookNow.setText(getResources().getString(R.string.member_main_view_quick_search_screen_book_now_button));
                    } else {
                        btnAdvancedSearchResultBookNow.setText(getResources().getString(R.string.member_main_view_quick_search_screen_more_detail_button));
                    }

                    btnAdvancedSearchResultBookNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                            AppCoreCode.getInstance().cache.currentSelectedProviderTypeFromSearchResults = currentItem.providerTypeName;
                            getProviderProfileFromServer();
                        }
                    });

                    lloBookNow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults = currentItem.providerId;
                            AppCoreCode.getInstance().cache.currentSelectedProviderTypeFromSearchResults = currentItem.providerTypeName;
                            getProviderProfileFromServer();
                        }
                    });

                    if (AppCoreCode.getInstance().cache.advancedSearch != null && AppCoreCode.getInstance().cache.advancedSearch.itemsList.size() - 1 == position && pageNumber < totalPages) {
                        getAdvancedResultFromServer();
                        progressBarAdvancedSearch.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public int getItemCount() {
            // TODO: NPE here cause Server delay
            if (AppCoreCode.getInstance().cache.advancedSearch != null && AppCoreCode.getInstance().cache.advancedSearch.itemsList != null) {
                return AppCoreCode.getInstance().cache.advancedSearch.itemsList.size();
            } else {
                return 0;
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    private void getAdvancedResultFromServer() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void setUserFavorite() {
        favoriteAsyncTaskClass = new FavoriteAsyncTaskClass();
        favoriteAsyncTaskClass.execute();
    }

    private class FavoriteAsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                favoriteResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/favoriteupsert", "{\"id\": " + AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults + "}");

                Log.d(TAG, "doInBackground() --> searchResultsResponse: " + favoriteResponse);
                JSONObject reader = new JSONObject(favoriteResponse);
                success = reader.getBoolean("success");
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // https://stackoverflow.com/questions/30220771/recyclerview-inconsistency-detected-invalid-item-position
        }
    }

    AdvancedSearch advancedSearch;
    RecyclerView.OnItemTouchListener disabler = new RecyclerViewDisabler();

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            if (pageNumber < AppCoreCode.getInstance().cache.advancedSearch.totalPages) {
                AppCoreCode.getInstance().cache.isSearchLoading = true;
                recyclerViewSearchResults.addOnItemTouchListener(disabler);
                ibtnBack.setEnabled(false);
            }
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                advancedSearch = null;
                if (AppCoreCode.getInstance().cache.advancedSearch != null && pageNumber < totalPages) {
                    pageNumber = pageNumber + 1;
                    advancedSearch = AppCoreCode.getInstance().cache.advancedSearch;
                    /*advancedSearch.itemsList = AppCoreCode.getInstance().cache.advancedSearch.itemsList;*/
                }
                searchResultsResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getprovidersbysearch"
                        , AppCoreCode.getInstance().cache.advancedSearchJsonToSend + ",\"pageNumber\": " + pageNumber + ",\"pageSize\": 10}");

                Log.d(TAG, "doInBackground() --> searchResultsResponse: " + searchResultsResponse);
                JSONObject reader = new JSONObject(searchResultsResponse);
                AppCoreCode.getInstance().cache.advancedSearch = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), AdvancedSearch.class);

                String itemsArray = reader.getJSONObject("result").getJSONArray("items").toString();

                AppCoreCode.getInstance().cache.advancedSearch.itemsList = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<AdvancedSearch.Items>>() {
                }.getType());

                if (advancedSearch != null) {
                    advancedSearch.itemsList.addAll(AppCoreCode.getInstance().cache.advancedSearch.itemsList);
                    AppCoreCode.getInstance().cache.advancedSearch.itemsList = advancedSearch.itemsList;
                }

                totalPages = AppCoreCode.getInstance().cache.advancedSearch.totalPages;

                Log.d(TAG, "doInBackground() --> itemsList size: " + AppCoreCode.getInstance().cache.advancedSearch.itemsList.size());
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // https://stackoverflow.com/questions/30220771/recyclerview-inconsistency-detected-invalid-item-position
//            recyclerViewSearchResults.getRecycledViewPool().clear();
            recyclerViewSearchResults.getAdapter().notifyDataSetChanged();
            recyclerViewSearchResults.removeOnItemTouchListener(disabler);
            ibtnBack.setEnabled(true);
            progressBarAdvancedSearch.setVisibility(View.GONE);
            AppCoreCode.getInstance().cache.isSearchLoading = false;
        }
    }

    public class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private class AsyncTaskProviderProfileClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                providerProfileResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getproviderinfo"
                        , "{\"id\": " + AppCoreCode.getInstance().cache.currentSelectedProviderIndexFromSearchResults + "}");

                Log.d(TAG, "doInBackground() --> providerProfileResponse: " + providerProfileResponse);
                JSONObject reader = new JSONObject(providerProfileResponse);
                AppCoreCode.getInstance().cache.providerProfile = AppCoreCode.getInstance().cache.gson.fromJson(providerProfileResponse, ProviderProfile.class);
                Log.d(TAG, "doInBackground() --> providerProfile: " + AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData);

                String providerProfileData = reader.getJSONObject("result").getJSONObject("providerProfileData").toString();
                Log.d(TAG, "doInBackground() --> providerProfileData: " + providerProfileData);
                // TODO: Checking for null cause Network delay
                if (AppCoreCode.getInstance().cache.providerProfile != null && AppCoreCode.getInstance().cache.providerProfile.result != null) {
                    AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData = AppCoreCode.getInstance().cache.gson.fromJson(providerProfileData, ProviderProfile.ProviderProfileData.class);

                    Log.d(TAG, "doInBackground() --> mediaFiles: " + AppCoreCode.getInstance().cache.providerProfile.result.mediaFiles);

                    String providerServices = reader.getJSONObject("result").getJSONArray("providerServices").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList = AppCoreCode.getInstance().cache.gson.fromJson(providerServices, new TypeToken<List<ProviderProfile.ProviderServices>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerServicesList: " + AppCoreCode.getInstance().cache.providerProfile.result.providerServicesList);

                    String providerOffers = reader.getJSONObject("result").getJSONArray("providerOffers").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList = AppCoreCode.getInstance().cache.gson.fromJson(providerOffers, new TypeToken<List<ProviderProfile.ProviderOffers>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerOffers: " + AppCoreCode.getInstance().cache.providerProfile.result.providerOffersList);

                    String providerSpecializations = reader.getJSONObject("result").getJSONArray("providerSpecializations").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerSpecializationsList = AppCoreCode.getInstance().cache.gson.fromJson(providerSpecializations, new TypeToken<List<ProviderProfile.ProviderSpecializations>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerSpecializations: " + AppCoreCode.getInstance().cache.providerProfile.result.providerSpecializationsList);

                    String providerWorkinDays = reader.getJSONObject("result").getJSONArray("providerWorkinDays").toString();
                    AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays = AppCoreCode.getInstance().cache.gson.fromJson(providerWorkinDays, new TypeToken<List<ProviderProfile.ProviderWorkinDays>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> providerWorkinDays: " + AppCoreCode.getInstance().cache.providerProfile.result.providerWorkinDays);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO: Checking for null cause Network delay
            if (AppCoreCode.getInstance().cache.providerProfile != null
                    && AppCoreCode.getInstance().cache.providerProfile.result != null
                    && AppCoreCode.getInstance().cache.providerProfile.result.providerProfileData != null) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(15);
            }
        }
    }

    // https://stackoverflow.com/questions/31759171/recyclerview-and-java-lang-indexoutofboundsexception-inconsistency-detected-in
//    class WrapContentLinearLayoutManager extends LinearLayoutManager {
//        public WrapContentLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
//            super(context, orientation, reverseLayout);
//        }
//
//        //... constructor
//        @Override
//        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
//            try {
//                super.onLayoutChildren(recycler, state);
//            } catch (IndexOutOfBoundsException e) {
//                Log.e("TAG", "meet a IOOBE in RecyclerView");
//            }
//        }
//    }
}

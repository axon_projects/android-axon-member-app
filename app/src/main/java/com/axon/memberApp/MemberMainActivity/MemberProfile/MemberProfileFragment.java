package com.axon.memberApp.MemberMainActivity.MemberProfile;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.FavoriteFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.NextAppointmentsFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.PreAppointmentsFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfilePagerAdapter;
import com.axon.memberApp.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.stfalcon.frescoimageviewer.ImageViewer;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberProfileFragment extends Fragment {
    private View view;
    private TabLayout tabLayoutMemberProfile;
    private ViewPager viewPagerMemberProfile;
    private Button btnProfileSettings;
    private ImageButton ibtnSideMenu, ibtnQRCode;
    private ProfilePagerAdapter profilePagerAdapter;
    private TextView txtTotalSavedAmount, txtMemberName;
    private SimpleDraweeView imgMemberAvatar;

    public MemberProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.member_profile_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        tabLayoutMemberProfile = view.findViewById(R.id.tabLayoutMemberProfile);
        viewPagerMemberProfile = view.findViewById(R.id.viewPagerMemberProfile);
        btnProfileSettings = view.findViewById(R.id.btnProfileSettings);
        ibtnSideMenu = view.findViewById(R.id.ibtnSideMenu);
        ibtnQRCode = view.findViewById(R.id.ibtnQRCode);
        txtTotalSavedAmount = view.findViewById(R.id.txtTotalSavedAmount);
        txtMemberName = view.findViewById(R.id.txtMemberName);
        imgMemberAvatar = view.findViewById(R.id.imgMemberAvatar);

        initPagerAdapter();
        setViewsActions();
        getDataFromCache();
    }

    private void initPagerAdapter() {
//        profilePagerAdapter = new ProfilePagerAdapter(getFragmentManager(), tabLayoutMemberProfile.getTabCount());
        profilePagerAdapter = new ProfilePagerAdapter(getChildFragmentManager(), tabLayoutMemberProfile.getTabCount());
        profilePagerAdapter.add(new NextAppointmentsFragment());
        profilePagerAdapter.add(new PreAppointmentsFragment());
        profilePagerAdapter.add(new FavoriteFragment());
        viewPagerMemberProfile.setAdapter(profilePagerAdapter);
        viewPagerMemberProfile.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutMemberProfile));
    }

    private void setViewsActions() {
        ibtnSideMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MemberMainViewActivity) getActivity()).drawer.openDrawer(Gravity.START);
            }
        });
        ibtnQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(7);
            }
        });

        btnProfileSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(6);
            }
        });

        tabLayoutMemberProfile.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerMemberProfile.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        imgMemberAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] listimages = {AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl};
                new ImageViewer.Builder(getActivity(), listimages)
                        .show();
            }
        });
    }

    private void getDataFromCache() {
        // TODO: Cause of network delay, sometimes memberProfile ends with NPE
        if (AppCoreCode.getInstance().cache.memberProfile != null) {
            imgMemberAvatar.setImageURI(AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl);
            txtMemberName.setText(AppCoreCode.getInstance().cache.memberProfile.memberName);
            txtTotalSavedAmount.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + AppCoreCode.getInstance().cache.memberProfile.totalSavedAmount);
        }
    }
}

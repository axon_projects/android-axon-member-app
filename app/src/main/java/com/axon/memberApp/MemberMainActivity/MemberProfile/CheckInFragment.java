package com.axon.memberApp.MemberMainActivity.MemberProfile;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInFragment extends Fragment {
    private View view;
    private ImageButton ibtnCheckInClose;
    private ImageView imgQRCode;
    private TextView txtCheckInMembershipID;

    public CheckInFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.check_in_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void initViews() {
        ibtnCheckInClose = view.findViewById(R.id.ibtnCheckInClose);
        imgQRCode = view.findViewById(R.id.imgQRCode);
        txtCheckInMembershipID = view.findViewById(R.id.txtCheckInMembershipID);

        setViewsActions();
        getDataFromCache();
    }

    private void setViewsActions() {
        ibtnCheckInClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
            }
        });
    }

    private void getDataFromCache() {
        if (AppCoreCode.getInstance().cache.memberProfile != null && AppCoreCode.getInstance().cache.memberProfile.memberShip != null) {
            imgQRCode.setImageBitmap(AppCoreCode.getInstance().cache.qrCodeFileName);
            txtCheckInMembershipID.setText("#" + AppCoreCode.getInstance().cache.memberProfile.memberCode);
        } else {
            imgQRCode.setImageBitmap(null);
        }
    }
}

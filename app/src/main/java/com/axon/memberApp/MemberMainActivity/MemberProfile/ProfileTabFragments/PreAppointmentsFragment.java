package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.PreAppointments;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreAppointmentsFragment extends Fragment {
    private View view;
    private RecyclerView recyclerViewPreAppointments;
    private RecyclerView.Adapter adapter;
    private ProgressBar progressBarPreAppointment;

    private AsyncTaskClass asyncTaskClass;
    private String preAppointmentsResponse;
    private String TAG = "PreAppointFragment";

    private int pageNumber;
    private int totalPages;

    public PreAppointmentsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.pre_appointments_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && view != null){
            initViews();
        }
    }

    private void initViews() {
        pageNumber = 1;
        totalPages = 0;
        progressBarPreAppointment = view.findViewById(R.id.progressBarPreAppointment);
        recyclerViewPreAppointments = view.findViewById(R.id.recyclerViewPreAppointments);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewPreAppointments.setHasFixedSize(true);
        recyclerViewPreAppointments.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        getPreAppointmentsFromServer();
    }

    private void getPreAppointmentsFromServer() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    void loadRecyclerView() {
        Log.d(TAG, "MyAdapter --> loadRecyclerViewDate() called");
        adapter = new MyAdapter();
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewPreAppointments.setHasFixedSize(true);
        recyclerViewPreAppointments.setAdapter(adapter);
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pre_appointment_custom_row, viewGroup, false);
            Log.d(TAG, "MyAdapter --> onCreateViewHolder() --> row index: " + i);
            return new MyAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);
            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row position: " + position);
            final PreAppointments.Items currentItem = AppCoreCode.getInstance().cache.preAppointments.itemsList.get(position);

            Button btnDetail = holder.itemView.findViewById(R.id.btnDetail);
            RatingBar ratingBarPreAppointment = holder.itemView.findViewById(R.id.ratingBarPreAppointment);
            final CircleImageView imgPreAppointProviderAvatarURL = holder.itemView.findViewById(R.id.imgPreAppointProviderAvatarURL);
            TextView txtPreAppointProviderName = holder.itemView.findViewById(R.id.txtPreAppointProviderName);
            TextView txtPreAppointCanceled = holder.itemView.findViewById(R.id.txtPreAppointCanceled);
            TextView txtPreAppointDocName = holder.itemView.findViewById(R.id.txtPreAppointDocName);
            TextView txtPreAppointTimeFrom = holder.itemView.findViewById(R.id.txtPreAppointTimeFrom);
            TextView txtPreAppointTimeTo = holder.itemView.findViewById(R.id.txtPreAppointTimeTo);
            TextView txtPreAppointPrice = holder.itemView.findViewById(R.id.txtPreAppointPrice);
            TextView txtPreAppointReserveDateDayName = holder.itemView.findViewById(R.id.txtPreAppointReserveDateDayName);
            TextView txtPreAppointReserveDateDayNum = holder.itemView.findViewById(R.id.txtPreAppointReserveDateDayNum);
            TextView txtPreAppointReserveDateMonth = holder.itemView.findViewById(R.id.txtPreAppointReserveDateMonth);

            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> Provider rate: " + currentItem.rate);
            ratingBarPreAppointment.setRating(currentItem.rate);

            if (!currentItem.providerAvatarUrl.contains("nophoto-slider")) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        final Drawable drawable = Tools.LoadImageFromWebOperations(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.providerAvatarUrl);
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imgPreAppointProviderAvatarURL.setImageDrawable(drawable);
                                }
                            });
                        }
                    }
                });
            }
            txtPreAppointProviderName.setText(currentItem.providerName);
            txtPreAppointDocName.setText(currentItem.docName);
            txtPreAppointTimeFrom.setText(currentItem.timeFrom);
            if(currentItem.timeTo != null) {
                txtPreAppointTimeTo.setText(currentItem.timeTo);
            }else {
                txtPreAppointTimeTo.setText("");
            }
            txtPreAppointPrice.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + currentItem.price);

            try {
                Date dateDay = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(currentItem.reservationDate);
                SimpleDateFormat outFormat = new SimpleDateFormat("EEE");
                String dayName = outFormat.format(dateDay);
                txtPreAppointReserveDateDayName.setText(dayName);

                txtPreAppointReserveDateDayNum.setText(currentItem.reservationDate.substring(currentItem.reservationDate.length() - 2, currentItem.reservationDate.length()));

                SimpleDateFormat month_date = new SimpleDateFormat("MMM", Locale.getDefault());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateMonth = sdf.parse(currentItem.reservationDate);
                String month_name = month_date.format(dateMonth);

                txtPreAppointReserveDateMonth.setText(month_name);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (currentItem.reservationStatus.equalsIgnoreCase("Finished") || currentItem.reservationStatus.equalsIgnoreCase("CheckedOut")) {
                ratingBarPreAppointment.setVisibility(View.VISIBLE);
                btnDetail.setVisibility(View.VISIBLE);
                txtPreAppointCanceled.setVisibility(View.GONE);
            } else if (currentItem.reservationStatus.contains("Canceled")) {
                txtPreAppointCanceled.setVisibility(View.VISIBLE);
                ratingBarPreAppointment.setVisibility(View.GONE);
                btnDetail.setVisibility(View.GONE);
            } else {
                txtPreAppointCanceled.setVisibility(View.GONE);
                ratingBarPreAppointment.setVisibility(View.GONE);
                btnDetail.setVisibility(View.GONE);
            }

            btnDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCoreCode.getInstance().cache.currentSelectedProviderListIndex = position;
                    if (getActivity() != null)
                        ((MemberMainViewActivity) getActivity()).changeFragment(14);
                }
            });

            if (AppCoreCode.getInstance().cache.preAppointments.itemsList.size() - 1 == position && pageNumber < totalPages) {
                getPreAppointmentsFromServer();
                progressBarPreAppointment.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, int i) {
            Log.d(TAG, "MyAdapter --> onBindViewHolder() --> row index: " + i);
        }

        @Override
        public int getItemCount() {
            if (AppCoreCode.getInstance().cache.preAppointments != null && AppCoreCode.getInstance().cache.preAppointments.itemsList != null) {
                Log.d(TAG, "MyAdapter --> getItemCount() --> getItemCount: " + AppCoreCode.getInstance().cache.preAppointments.itemsList.size());
                return AppCoreCode.getInstance().cache.preAppointments.itemsList.size();
            } else {
                return 0;
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                PreAppointments preAppointments = null;
                if (AppCoreCode.getInstance().cache.preAppointments != null && pageNumber < totalPages) {
                    pageNumber = pageNumber + 1;
                    preAppointments = AppCoreCode.getInstance().cache.preAppointments;
                }
                preAppointmentsResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getpreviousappointments", "{\"pageNumber\": " + pageNumber + ",\"pageSize\": 7}");
                Log.d(TAG, "doInBackground() --> preAppointmentsResponse: " + preAppointmentsResponse);
                JSONObject reader = new JSONObject(preAppointmentsResponse);
                AppCoreCode.getInstance().cache.preAppointments = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), PreAppointments.class);
                String itemsArray = reader.getJSONObject("result").getJSONArray("items").toString();
                AppCoreCode.getInstance().cache.preAppointments.itemsList = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<PreAppointments.Items>>() {}.getType());

                if (preAppointments != null) {
                    preAppointments.itemsList.addAll(AppCoreCode.getInstance().cache.preAppointments.itemsList);
                    AppCoreCode.getInstance().cache.preAppointments.itemsList = preAppointments.itemsList;
                }

                totalPages = AppCoreCode.getInstance().cache.preAppointments.totalPages;

                Log.d(TAG, "doInBackground() --> itemsList size: " + AppCoreCode.getInstance().cache.preAppointments.itemsList.size());
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (pageNumber == 1) {
                loadRecyclerView();
            } else {
                adapter.notifyDataSetChanged();
                progressBarPreAppointment.setVisibility(View.GONE);
            }
        }
    }
}

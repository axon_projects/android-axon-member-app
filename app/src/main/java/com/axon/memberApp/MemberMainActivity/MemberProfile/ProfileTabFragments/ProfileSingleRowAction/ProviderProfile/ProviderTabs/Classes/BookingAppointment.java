package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes;

import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.MembershipFeesInfo;

public class BookingAppointment {
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public Error error = new Error();
    public int result;

    public class Error {
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }
}

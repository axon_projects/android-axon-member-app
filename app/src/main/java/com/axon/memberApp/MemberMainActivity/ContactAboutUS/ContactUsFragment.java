package com.axon.memberApp.MemberMainActivity.ContactAboutUS;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsFragment extends Fragment {
    private View view;
    private ImageButton ibtnContactUsBack;
    private Button btnContactUsSend;
    private TextView txtContactUsPhoneNumber, txtContactUsEmail, txtContactUsMsg;
    private String TAG = "ContactUsFragment";
    private String contactUsResponse;
    private AsyncTaskContactUsClass asyncTaskContactUsClass;

    public ContactUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.contact_us_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser  && getActivity() != null && isAdded() && view != null){
            initViews();
        }
    }

    private void initViews() {
        ibtnContactUsBack = view.findViewById(R.id.ibtnContactUsBack);

        btnContactUsSend = view.findViewById(R.id.btnContactUsSend);
        txtContactUsPhoneNumber = view.findViewById(R.id.txtContactUsPhoneNumber);
        txtContactUsEmail = view.findViewById(R.id.txtContactUsEmail);
        txtContactUsMsg = view.findViewById(R.id.txtContactUsMsg);

        setViewsActions();
    }

    private void setViewsActions() {
        ibtnContactUsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
            }
        });

        btnContactUsSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInputs();
            }
        });
    }

    private void showLoadingDialog() {
        if (getActivity() != null) {
            AppCoreCode.getInstance().sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            AppCoreCode.getInstance().sweetAlertDialog.getProgressHelper().setBarColor(R.color.axonTextDarkPurpleColor);
            AppCoreCode.getInstance().sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_loading));
            AppCoreCode.getInstance().sweetAlertDialog.setCancelable(true);
            AppCoreCode.getInstance().sweetAlertDialog.show();
        }
    }

    private void validateUserInputs() {
        if (txtContactUsPhoneNumber.getText().toString().isEmpty()) {
            txtContactUsPhoneNumber.setError(getResources().getString(R.string.view_error_number_required));
            txtContactUsPhoneNumber.requestFocus();
            return;
        }
        if (Tools.isValidPhone(txtContactUsPhoneNumber.getText().toString().trim())) {
            txtContactUsPhoneNumber.setError(getResources().getString(R.string.view_error_number_wrong));
            txtContactUsPhoneNumber.requestFocus();
            return;
        }

        if (txtContactUsEmail.getText().toString().isEmpty()) {
            txtContactUsEmail.setError(getResources().getString(R.string.view_error_email_required));
            txtContactUsEmail.requestFocus();
            return;
        }

        if (!Tools.isValidEmailAddress(txtContactUsEmail.getText().toString().trim())) {
            txtContactUsEmail.setError(getResources().getString(R.string.view_error_email_wrong));
            txtContactUsEmail.requestFocus();
            return;
        }

        if (txtContactUsMsg.getText().toString().isEmpty()) {
            txtContactUsMsg.setError(getResources().getString(R.string.member_contact_message));
            txtContactUsMsg.requestFocus();
            return;
        }

        sendDataToServer();
    }

    private void sendDataToServer() {
        try {
            asyncTaskContactUsClass = new AsyncTaskContactUsClass();
            asyncTaskContactUsClass.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncTaskContactUsClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
            showLoadingDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                contactUsResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/contactus"
                        , "{\"mobileNumber\": \" " + txtContactUsPhoneNumber.getText().toString() + " \",\"email\": \" " + txtContactUsEmail.getText().toString() + " \",\"message\": \" " + txtContactUsMsg.getText().toString() + " \"}");

                Log.d(TAG, "doInBackground() --> contactUsResponse: " + contactUsResponse);
                JSONObject reader = new JSONObject(contactUsResponse);

//                String providerProfileData = reader.getJSONObject("result").getJSONObject("providerProfileData").toString();
//                Log.d(TAG, "doInBackground() --> providerProfileData: " + providerProfileData);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            AppCoreCode.getInstance().sweetAlertDialog.dismissWithAnimation();
            if (getActivity() != null)
                ((MemberMainViewActivity) getActivity()).changeFragment(0);

        }
    }
}

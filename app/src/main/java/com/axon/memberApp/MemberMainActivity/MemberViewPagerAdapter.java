package com.axon.memberApp.MemberMainActivity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.axon.memberApp.MemberMainActivity.ContactAboutUS.AboutUsFragment;
import com.axon.memberApp.MemberMainActivity.ContactAboutUS.ContactUsFragment;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.ConfirmMemberShipPaymentFragment;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.CreateMembershipFragment;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.CreateMembershipWithFawryFragment;
import com.axon.memberApp.MemberMainActivity.MemberNotification.NotificationFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.CancelAppointmentFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.CheckInFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.MemberProfileFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.MemberProfileSettings.ProfileResetPasswordLinkFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.MemberProfileSettings.ProfileSettingsFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderProfileFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.ImageViewer.ImageViewerFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderRateFragment;
import com.axon.memberApp.MemberMainActivity.MemberSearch.AdvancedSearchFragment;
import com.axon.memberApp.MemberMainActivity.MemberSearch.AdvancedSearchResultsWithEditFragment;
import com.axon.memberApp.MemberMainActivity.MemberSearch.MemberMainViewFragment;
import com.axon.memberApp.MemberMainActivity.MemberSearch.QuickSearchFragment;

public class MemberViewPagerAdapter extends FragmentStatePagerAdapter {
    // Declare Login Fragments
    private MemberMainViewFragment memberMainViewFragment; // 0
    private QuickSearchFragment quickSearchFragment; // 1
    private AdvancedSearchFragment advancedSearchFragment; // 2
    private AdvancedSearchResultsWithEditFragment advancedSearchResultsWithEditFragment; // 3
    private NotificationFragment notificationFragment; // 4
    private MemberProfileFragment memberProfileFragment; // 5
    private ProfileSettingsFragment profileSettingsFragment; // 6
    private CheckInFragment checkInFragment; // 7
    private ContactUsFragment contactUsFragment; // 8
    private CreateMembershipFragment createMembershipFragment; // 9
    private CreateMembershipWithFawryFragment createMembershipWithFawryFragment; // 10
    private ConfirmMemberShipPaymentFragment confirmMemberShipPaymentFragment; // 11
    private ProfileResetPasswordLinkFragment profileResetPasswordLinkFragment; // 12
    private CancelAppointmentFragment cancelAppointmentFragment; // 13
    private ProviderRateFragment providerRateFragment; // 14
    private ProviderProfileFragment providerProfileFragment; // 15
    private AboutUsFragment aboutUsFragment; // 16
    private ImageViewerFragment imageViewerFragment; // 17

    public MemberViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.w("LoginSignUpViewPager", "getItem() --> position: " + position);
        if(position == 0){
            if(memberMainViewFragment == null)
                memberMainViewFragment = new MemberMainViewFragment();
            return memberMainViewFragment;
        } else if(position == 1){
            if(quickSearchFragment == null)
                quickSearchFragment = new QuickSearchFragment();
            return quickSearchFragment;
        } else if(position == 2){
            if( advancedSearchFragment == null)
                 advancedSearchFragment = new AdvancedSearchFragment();
            return  advancedSearchFragment;
        } else if(position == 3){
            if(advancedSearchResultsWithEditFragment == null)
                advancedSearchResultsWithEditFragment = new AdvancedSearchResultsWithEditFragment();
            return advancedSearchResultsWithEditFragment;
        } else if(position == 4){
            if(notificationFragment == null)
                notificationFragment = new NotificationFragment();
            return notificationFragment;
        } else if(position == 5){
            if(memberProfileFragment == null)
                memberProfileFragment = new MemberProfileFragment();
            return memberProfileFragment;
        } else if(position == 6){
            if(profileSettingsFragment == null)
                profileSettingsFragment = new ProfileSettingsFragment();
            return profileSettingsFragment;
        } else if(position == 7){
            if(checkInFragment == null)
                checkInFragment = new CheckInFragment();
            return checkInFragment;
        } else if(position == 8){
            if(contactUsFragment == null)
                contactUsFragment = new ContactUsFragment();
            return contactUsFragment;
        }  else if(position == 9){
            if(createMembershipFragment == null)
                createMembershipFragment = new CreateMembershipFragment();
            return createMembershipFragment;
        } else if(position == 10){
            if(createMembershipWithFawryFragment == null)
                createMembershipWithFawryFragment = new CreateMembershipWithFawryFragment();
            return createMembershipWithFawryFragment;
        } else if(position == 11){
            if(confirmMemberShipPaymentFragment == null)
                confirmMemberShipPaymentFragment = new ConfirmMemberShipPaymentFragment();
            return confirmMemberShipPaymentFragment;
        } else if(position == 12){
            if(profileResetPasswordLinkFragment == null)
                profileResetPasswordLinkFragment = new ProfileResetPasswordLinkFragment();
            return profileResetPasswordLinkFragment;
        } else if(position == 13){
            if(cancelAppointmentFragment == null)
                cancelAppointmentFragment = new CancelAppointmentFragment();
            return cancelAppointmentFragment;
        } else if(position == 14){
            if(providerRateFragment == null)
                providerRateFragment = new ProviderRateFragment();
            return providerRateFragment;
        } else if(position == 15){
            if(providerProfileFragment == null)
                providerProfileFragment = new ProviderProfileFragment();
            return providerProfileFragment;
        } else if(position == 16){
            if(aboutUsFragment == null)
                aboutUsFragment = new AboutUsFragment();
            return aboutUsFragment;
        } else if(position == 17){
            if(imageViewerFragment == null)
                imageViewerFragment = new ImageViewerFragment();
            return imageViewerFragment;
        } else {
            return null;
        }

    }

    @Override
    public int getCount() {
        return 18;
    }
}

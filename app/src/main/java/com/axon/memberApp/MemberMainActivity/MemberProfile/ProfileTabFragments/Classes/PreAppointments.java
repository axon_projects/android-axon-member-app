package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes;

import java.util.List;

public class PreAppointments {
    public int currentPage;
    public int pageSize;
    public int totalPages;
    public int totalCount;

    public List<Items> itemsList;

    public class Items {
        public String checkInTime;
        public String checkOutTime;
        public String cancellationReasonText;
        public String cancellationReasonNotes;
        public int reviewId;
        public float rate;
        public int providerId;
        public int appointmentId;
        public String providerName;
        public String docTitle;
        public String docName;
        public String providerAvatarUrl;
        public int reservationId;
        public String reservationDate;
        public String timeFrom;
        public String timeTo;
        public String reservationStatus;
        public String isWalkIn;
        public int price;
    }
}

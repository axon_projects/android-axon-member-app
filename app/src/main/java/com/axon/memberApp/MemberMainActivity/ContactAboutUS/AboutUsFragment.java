package com.axon.memberApp.MemberMainActivity.ContactAboutUS;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {
    private View view;
    private ImageButton ibtnAboutUsBack;

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.about_us_fragment, container, false);

        ibtnAboutUsBack = view.findViewById(R.id.ibtnAboutUsBack);
        ibtnAboutUsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
//                    if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("en")) {
//                        Tools.loadLocale(getActivity(), "en");
//                    } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("ar")) {
//                        Tools.loadLocale(getActivity(), "ar");
//                    }
                    ((MemberMainViewActivity) getActivity()).changeFragment(0);
                }
            }
        });

        return view;
    }

}

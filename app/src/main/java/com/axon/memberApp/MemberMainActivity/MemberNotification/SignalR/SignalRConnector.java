package com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

import static android.content.Context.NOTIFICATION_SERVICE;

public class SignalRConnector {
    HubConnection hubConnection;
    HubProxy hubProxy;
    HubProxy hubProxy1;
    Handler handler = new Handler();
    public Context context;
    String state = "NotConnected";
    String TAG = "SignalRConnector";

    public NotificationManager mNotificationManager;
    private static String DEFAULT_CHANNEL_ID = "default_channel";
    private static String DEFAULT_CHANNEL_NAME = "Default";

//    String bearerToken = "Bearer " + AppCoreCode.getInstance().cache.accessToken;

    /**
     * Signalr
     */
    public void connect() {
        Log.d(TAG, "connect() called");
//        Platform.loadPlatformComponent(new AndroidPlatformComponent());
        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(Request request) {
                if (AppCoreCode.getInstance().cache != null && AppCoreCode.getInstance().cache.accessToken != null)
                    request.addHeader("Authorization", "Bearer " + AppCoreCode.getInstance().cache.accessToken);
            }
        };
        hubConnection = new HubConnection(AppCoreCode.getInstance().cache.mainURL);
        hubConnection.setCredentials(credentials);
        hubConnection.connected(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "connect() --> Connected to: " + AppCoreCode.getInstance().cache.mainURL);
                        state = "Connected";
                    }
                });
            }
        });
        hubProxy = hubConnection.createHubProxy("abpCommonHub");
        ClientTransport clientTransport = new ServerSentEventsTransport(hubConnection.getLogger());
        SignalRFuture<Void> signalRFuture = hubConnection.start(clientTransport);

        hubProxy.on("getNotification", new SubscriptionHandler1<Object>() {
            @Override
            public void run(Object json) {
                Log.d(TAG, "Json response: " + json);
                try {
                    // Solve Object not taken Json format(shape)
                    JSONObject jsonObject = Tools.decodeResult(json);
                    Log.d(TAG, "Message Received --> jsonObject " + jsonObject.toString());

                    AppCoreCode.getInstance().cache.signalR = AppCoreCode.getInstance().cache.gson.fromJson(jsonObject.toString(), SignalR.class);
                    // TODO: Show notification here
                    showNotification(AppCoreCode.getInstance().cache.signalR.notification.data.senderUserName, AppCoreCode.getInstance().cache.signalR.notification.data.messageEn);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, Object.class);

        try {
            signalRFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reinit
     */
    public void disconnect() {
        if (state.equalsIgnoreCase("Connected")) {
            state = "NotConnected";
            hubConnection.stop();
        }
    }

    private void showNotification(String title, String text) {
        //1.Get reference to Notification Manager
        mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notification;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Create channel only if it is not already created
            if (mNotificationManager.getNotificationChannel(DEFAULT_CHANNEL_ID) == null) {
                mNotificationManager.createNotificationChannel(new NotificationChannel(
                        DEFAULT_CHANNEL_ID, DEFAULT_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
                ));
            }
            //2.Build Notification with NotificationCompat.Builder
            notification = new NotificationCompat.Builder(context, DEFAULT_CHANNEL_ID);
        } else {
            //2.Build Notification with NotificationCompat.Builder
            notification = new NotificationCompat.Builder(context);
        }

        Intent intent = new Intent(context, MemberMainViewActivity.class);
        intent.setAction("NotificationAction");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        notification.setContentTitle(title)   //Set the title of Notification
                .setSmallIcon(android.R.drawable.ic_menu_view)   //Set the icon
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_round))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(text)
                        .setSummaryText(text.substring(0, text.length() / 2)))
                .addAction(new NotificationCompat.Action(
                        R.drawable.notification_read_all_icon,
                        context.getString(R.string.member_main_view_mark_as_read),
                        PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)))
                .build();

        //Send the notification.
        mNotificationManager.notify(1, notification.getNotification());
    }
}

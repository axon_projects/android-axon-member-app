package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.Classes;

import java.util.List;

public class ProviderRate {
    public  String targetUrl;
    public  boolean success;
    public  boolean unAuthorizedRequest;
    public  boolean __abp;

    public Error error = new Error();
    public Result result;

    public class Result{
        public int appointmentId;
        public String reviewText;
        public int rate;
    }

    public class Error{
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }
}

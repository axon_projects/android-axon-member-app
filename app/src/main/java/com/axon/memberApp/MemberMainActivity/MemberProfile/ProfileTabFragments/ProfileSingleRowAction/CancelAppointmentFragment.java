package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction;


import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.Classes.CancellationReasons;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CancelAppointmentFragment extends Fragment {
    private View view;
    private ImageButton ibtnClose;
    private EditText etxtCancelationsNotes;
    private Button btnConfirmCancel;
    private RadioGroup rgCancellationReasons;

    private AsyncTaskClass asyncTaskClass;
    private String cancellationReasonsResponse;
    private String cancellationAppointmentResponse;
    private String typeOfServerOperation;

    private String TAG = "CancelAppointmentFragment";

    public CancelAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.cancel_appointment_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null && isAdded() && getActivity() != null) {
            initViews();
        }
    }

    private void initViews() {
        ibtnClose = view.findViewById(R.id.ibtnClose);
        etxtCancelationsNotes = view.findViewById(R.id.etxtCancelationsNotes);
        btnConfirmCancel = view.findViewById(R.id.btnConfirmCancel);
        rgCancellationReasons = view.findViewById(R.id.rgCancellationReasons);

        setViewsActions();
        getSetCancellationReasonsFromServer("Get");
    }

    private void getSetCancellationReasonsFromServer(String typeOfServerOperation) {
        this.typeOfServerOperation = typeOfServerOperation;
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void setViewsActions() {
        ibtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(5);
            }
        });

        btnConfirmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSetCancellationReasonsFromServer("Post");
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(5);
            }
        });
    }

    private String onCancelAppointmentMsg = "";

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfServerOperation.equalsIgnoreCase("Get")) {
                    cancellationReasonsResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getcancellationreasons");
                    Log.d(TAG, "doInBackground() --> cancellationReasons: " + cancellationReasonsResponse);
                    JSONObject reader = new JSONObject(cancellationReasonsResponse);
                    AppCoreCode.getInstance().cache.cancellationReasons = AppCoreCode.getInstance().cache.gson.fromJson(cancellationReasonsResponse, CancellationReasons.class);

                    String itemsArray = reader.getJSONArray("result").toString();
                    AppCoreCode.getInstance().cache.cancellationReasons.result = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<CancellationReasons.Result>>() {
                    }.getType());
                    Log.d(TAG, "doInBackground() --> Cancellation reasons itemsList size: " + AppCoreCode.getInstance().cache.cancellationReasons.result.size());
                } else {
                    cancellationAppointmentResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/cancelappointment"
                            , "{\"appointmentId\": " + AppCoreCode.getInstance().cache.currentNextAppointToCancelId
                                    + ",\"cancellationReasonId\": " + rgCancellationReasons.getCheckedRadioButtonId()
                                    + ",\"cancellationReasonNotes\": \"" + etxtCancelationsNotes.getText().toString().trim() + "\"}");
                    JSONObject reader = new JSONObject(cancellationAppointmentResponse);
                    onCancelAppointmentMsg = reader.getString("result");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (typeOfServerOperation.equalsIgnoreCase("Get")) {
                if (AppCoreCode.getInstance().cache.cancellationReasons != null && AppCoreCode.getInstance().cache.cancellationReasons.result != null) {
                    for (CancellationReasons.Result result : AppCoreCode.getInstance().cache.cancellationReasons.result) {
                        if (getActivity() != null) {
                            RadioButton radioButton = new RadioButton(getActivity());
                            radioButton.setId(result.id);
                            radioButton.setText(result.name);
                            rgCancellationReasons.addView(radioButton);
                        }
                    }
                }
            } else {
                if (onCancelAppointmentMsg.isEmpty())
                    Toast.makeText(getActivity(), onCancelAppointmentMsg, Toast.LENGTH_LONG);
            }
        }
    }
}

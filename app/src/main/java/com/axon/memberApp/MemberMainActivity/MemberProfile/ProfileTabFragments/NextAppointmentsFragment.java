package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.NextAppointments;
import com.axon.memberApp.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NextAppointmentsFragment extends Fragment {
    private View view;
    private RecyclerView recyclerViewNextAppointments;
    private RecyclerView.Adapter adapter;

    private AsyncTaskClass asyncTaskClass;
    private String nextAppointmentsResponse;
    private ProgressBar progressBarNextAppointment;

    private int pageNumber;
    private int totalPages;

    public NextAppointmentsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.next_appointments_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null) {
            initViews();
        }
    }

    private void initViews() {
        pageNumber = 1;
        totalPages = 0;

        progressBarNextAppointment = view.findViewById(R.id.progressBarNextAppointment);
        recyclerViewNextAppointments = view.findViewById(R.id.recyclerViewNextAppointments);
        // setHasFixedSize() is used to let the RecyclerView keep the same size.
//        recyclerViewNextAppointments.setHasFixedSize(true);
        recyclerViewNextAppointments.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));

        getNextAppointmentsFromServer();
    }

    private void getNextAppointmentsFromServer() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    void loadRecyclerView() {
        adapter = new MyAdapter();
        recyclerViewNextAppointments.setAdapter(adapter);
    }

    class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        @NonNull
        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.next_appointment_custom_row, viewGroup, false);

            return new MyAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.ViewHolder viewHolder, int i) {

        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
            super.onBindViewHolder(holder, position, payloads);

            final NextAppointments.Items currentItem = AppCoreCode.getInstance().cache.nextAppointments.itemsList.get(position);

            Button btnCancel = holder.itemView.findViewById(R.id.btnCancel);
            RatingBar ratingBarNextAppointment = holder.itemView.findViewById(R.id.ratingBarNextAppointment);
            final CircleImageView imgNextAppointProviderAvatarURL = holder.itemView.findViewById(R.id.imgNextAppointProviderAvatarURL);
            TextView txtNextAppointProviderName = holder.itemView.findViewById(R.id.txtNextAppointProviderName);
            TextView txtNextAppointDocName = holder.itemView.findViewById(R.id.txtNextAppointDocName);
            TextView txtNextAppointTimeFrom = holder.itemView.findViewById(R.id.txtNextAppointTimeFrom);
            TextView txtNextAppointTimeTo = holder.itemView.findViewById(R.id.txtNextAppointTimeTo);
            TextView txtNextAppointPrice = holder.itemView.findViewById(R.id.txtNextAppointPrice);
            TextView txtNextAppointReserveDateDayName = holder.itemView.findViewById(R.id.txtNextAppointReserveDateDayName);
            TextView txtNextAppointReserveDateDayNum = holder.itemView.findViewById(R.id.txtNextAppointReserveDateDayNum);
            TextView txtNextAppointReserveDateMonth = holder.itemView.findViewById(R.id.txtNextAppointReserveDateMonth);

            ratingBarNextAppointment.setRating(currentItem.rate);

            if (!currentItem.providerAvatarUrl.contains("nophoto-slider")) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        final Drawable drawable = Tools.LoadImageFromWebOperations(AppCoreCode.getInstance().cache.mainURL + "/" + currentItem.providerAvatarUrl);
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imgNextAppointProviderAvatarURL.setImageDrawable(drawable);
                                }
                            });
                    }
                });
            }
            txtNextAppointProviderName.setText(currentItem.providerName);
            txtNextAppointDocName.setText(currentItem.docName);
            txtNextAppointTimeFrom.setText(currentItem.timeFrom);
            if (currentItem.timeTo != null) {
                txtNextAppointTimeTo.setText(currentItem.timeTo);
            } else {
                txtNextAppointTimeTo.setText("");
            }
            txtNextAppointPrice.setText(getActivity().getResources().getString(R.string.member_main_view_egp) + currentItem.price);

            try {
                Date dateDay = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(currentItem.reservationDate);
                SimpleDateFormat outFormat = new SimpleDateFormat("EEE");
                String dayName = outFormat.format(dateDay);
                txtNextAppointReserveDateDayName.setText(dayName);

                txtNextAppointReserveDateDayNum.setText(currentItem.reservationDate.substring(currentItem.reservationDate.length() - 2, currentItem.reservationDate.length()));

                SimpleDateFormat month_date = new SimpleDateFormat("MMM", Locale.getDefault());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date dateMonth = sdf.parse(currentItem.reservationDate);
                String month_name = month_date.format(dateMonth);

                txtNextAppointReserveDateMonth.setText(month_name);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppCoreCode.getInstance().cache.currentNextAppointToCancelId = currentItem.appointmentId;
                    if (getActivity() != null)
                        ((MemberMainViewActivity) getActivity()).changeFragment(13);
                }
            });

            if (AppCoreCode.getInstance().cache.nextAppointments.itemsList.size() - 1 == position && pageNumber < totalPages) {
                getNextAppointmentsFromServer();
                progressBarNextAppointment.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            if (AppCoreCode.getInstance().cache.nextAppointments != null && AppCoreCode.getInstance().cache.nextAppointments.itemsList != null) {
                return AppCoreCode.getInstance().cache.nextAppointments.itemsList.size();
            } else {
                return 0;
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                NextAppointments nextAppointments = null;
                if (AppCoreCode.getInstance().cache.nextAppointments != null && pageNumber < totalPages) {
                    pageNumber = pageNumber + 1;
                    nextAppointments = AppCoreCode.getInstance().cache.nextAppointments;
                }

                nextAppointmentsResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/getnextappointments", "{\"pageNumber\": 1,\"pageSize\": 7}");
                Log.d("NextAppointFragment", "doInBackground() --> nextAppointmentsResponse: " + nextAppointmentsResponse);
                JSONObject reader = new JSONObject(nextAppointmentsResponse);
                AppCoreCode.getInstance().cache.nextAppointments = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), NextAppointments.class);

                String itemsArray = reader.getJSONObject("result").getJSONArray("items").toString();
                AppCoreCode.getInstance().cache.nextAppointments.itemsList = AppCoreCode.getInstance().cache.gson.fromJson(itemsArray, new TypeToken<List<NextAppointments.Items>>() {
                }.getType());

                if (nextAppointments != null) {
                    nextAppointments.itemsList.addAll(AppCoreCode.getInstance().cache.nextAppointments.itemsList);
                    AppCoreCode.getInstance().cache.nextAppointments.itemsList = nextAppointments.itemsList;
                }

                totalPages = AppCoreCode.getInstance().cache.nextAppointments.totalPages;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (pageNumber == 1) {
                loadRecyclerView();
            } else {
                adapter.notifyDataSetChanged();
                progressBarNextAppointment.setVisibility(View.GONE);
            }
        }
    }
}

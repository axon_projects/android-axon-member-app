package com.axon.memberApp.MemberMainActivity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import androidx.viewpager.widget.ViewPager;

import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.CustomViewPager;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.ValidMembership;
import com.axon.memberApp.MemberMainActivity.MemberNotification.Classes.Notification;
import com.axon.memberApp.MemberMainActivity.MemberProfile.Classes.MemberProfile;
import com.axon.memberApp.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.login.LoginManager;

import org.json.JSONObject;

import java.io.InputStream;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;

public class MemberMainViewActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    final int MAIN_VIEW_FRAGMENT_INDEX = 0;
    final int QUICK_SEARCH_FRAGMENT_INDEX = 1;
    final int ADVANCED_SEARCH_FRAGMENT_INDEX = 2;
    final int ADVANCED_SEARCH_RESULT_WITH_EDIT_FRAGMENT_INDEX = 3;
    final int NOTIFICATION_FRAGMENT_INDEX = 4;
    final int PROFILE_FRAGMENT_INDEX = 5;
    final int PROFILE_SETTINGS_FRAGMENT_INDEX = 6;
    final int CHECK_IN_FRAGMENT_INDEX = 7;
    final int CONTACT_US_FRAGMENT_INDEX = 8;
    final int CREATE_MEMBERSHIP_FRAGMENT_INDEX = 9;
    final int CREATE_MEMBERSHIP_WITH_FAWRY_FRAGMENT_INDEX = 10;
    final int CONFIRM_MEMBERSHIP_PAYMENT_FRAGMENT_INDEX = 11;
    final int RESET_PASSWORD_LINK_FRAGMENT_INDEX = 12;
    final int CANCEL_APPOINTMENT_FRAGMENT_INDEX = 13;
    final int PROVIDER_RATE_FRAGMENT_INDEX = 14;
    final int PROVIDER_Profile_FRAGMENT_INDEX = 15;
    final int ABOUT_US_FRAGMENT_INDEX = 16;
    final int PROVIDER_IMAGE_VIEWER_FRAGMENT_INDEX = 17;

    private String TAG = "MemberMainViewActivity";

    private String requestType = "";

    public DrawerLayout drawer;

    private CustomViewPager viewPager;
    private MemberViewPagerAdapter memberViewPagerAdapter;
    private NavigationView navigationView;
    private View sideMenuHeaderView;
    private CircleImageView imgSideMenuProfilePhoto;
    private TextView txtSideMenuMemberName, txtSideMenuTotalSavedAmount;

    private SweetAlertDialog sweetAlertDialog;

    private AsyncTaskClass asyncTaskClass;
    private ReadNotificationAsyncTaskClass readNotificationAsyncTaskClass;

    private boolean isDataFromServerRequested = false;

    public int currentAdvancedSearch = -1;
    public int currentSelectedProviderType = -1;

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.member_main_view_activity);
        Log.d(TAG, "MemberActivityLifeCycle --> onCreate() called");

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                //Catch your exception
                // Without System.exit() this will not work.
                System.exit(2);
            }
        });

        try {
            initViews(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initViews(boolean isFirstLoad) {
        Log.d(TAG, "MemberActivityLifeCycle --> initViews() called");
        AppCoreCode.getInstance().memberMainViewActivity = this;
        if (!Fresco.hasBeenInitialized()) {
            Fresco.initialize(this);
        }

        if (sideMenuHeaderView == null && navigationView == null) {
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            /*Setup Navigation View*/
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }
        // Get header view of side menu
        if (sideMenuHeaderView == null && imgSideMenuProfilePhoto == null && txtSideMenuMemberName == null && txtSideMenuTotalSavedAmount == null) {
            sideMenuHeaderView = navigationView.getHeaderView(0);
            imgSideMenuProfilePhoto = sideMenuHeaderView.findViewById(R.id.imgSideMenuProfilePhoto);
            txtSideMenuMemberName = sideMenuHeaderView.findViewById(R.id.txtSideMenuMemberName);
            txtSideMenuTotalSavedAmount = sideMenuHeaderView.findViewById(R.id.txtSideMenuTotalSavedAmount);
        }
        if (viewPager == null && isFirstLoad) {
            setupViewPager();
            if (AppCoreCode.getInstance().cache.signalRConnector.context == null) {
                AppCoreCode.getInstance().cache.signalRConnector.context = this;
                AppCoreCode.getInstance().cache.signalRConnector.connect();
            }
            if (AppCoreCode.getInstance().cache.signalRBookingNotification.context == null) {
                AppCoreCode.getInstance().cache.signalRBookingNotification.context = this;
                AppCoreCode.getInstance().cache.signalRBookingNotification.connect();
            }
        }
        setViewsValues();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MemberActivityLifeCycle --> onResume() called");
        if (AppCoreCode.getInstance().cache.isBrowserOpened) {
            changeFragment(CONFIRM_MEMBERSHIP_PAYMENT_FRAGMENT_INDEX);
            AppCoreCode.getInstance().cache.isBrowserOpened = false;
        }
    }

    private void setupViewPager() {
        Log.d(TAG, "MemberActivityLifeCycle --> setupViewPager() called");
        viewPager = findViewById(R.id.memberMainActivityContainer);
        memberViewPagerAdapter = new MemberViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(memberViewPagerAdapter);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(19);
        changeFragment(MAIN_VIEW_FRAGMENT_INDEX);
    }

    @Override
    public void onBackPressed() {
        if (!AppCoreCode.getInstance().cache.isSearchLoading) {
            Log.d(TAG, "MemberActivityLifeCycle --> onBackPressed() called");
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (viewPager.getCurrentItem() == MAIN_VIEW_FRAGMENT_INDEX) {
                    // Clicking the back button twice to exit an activity
                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        return;
                    }

                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                } else if (viewPager.getCurrentItem() == PROVIDER_Profile_FRAGMENT_INDEX) {
                    changeFragment(ADVANCED_SEARCH_RESULT_WITH_EDIT_FRAGMENT_INDEX);
                } else if (viewPager.getCurrentItem() == PROVIDER_IMAGE_VIEWER_FRAGMENT_INDEX) {
                    changeFragment(PROVIDER_Profile_FRAGMENT_INDEX);
                } else if (viewPager.getCurrentItem() == ADVANCED_SEARCH_FRAGMENT_INDEX) {
                    changeFragment(MAIN_VIEW_FRAGMENT_INDEX);
                } else if (viewPager.getCurrentItem() == ADVANCED_SEARCH_RESULT_WITH_EDIT_FRAGMENT_INDEX) {
                    changeFragment(ADVANCED_SEARCH_FRAGMENT_INDEX);
                }
            }
        }
    }

    public void changeFragment(int fragmentIndex) {
        Log.d(TAG, "MemberActivityLifeCycle --> changeFragment() called");
        Log.d(TAG, "changeFragment() --> fragmentType: " + fragmentIndex + " -- ViewPager: " + viewPager);
        if (viewPager != null) {
            if (fragmentIndex == QUICK_SEARCH_FRAGMENT_INDEX) {
                viewPager.setCurrentItem(0);
            }
            if (fragmentIndex == 1 || fragmentIndex == 3 || fragmentIndex == 5)
                AppCoreCode.getInstance().cache.lastFragmentIndexBeforeProfileProvider = fragmentIndex;
            viewPager.setCurrentItem(fragmentIndex);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    if (i == MAIN_VIEW_FRAGMENT_INDEX) {
//                        getProfileSettingInfoFromServer("Notification");
                        Log.d(TAG, "MemberActivityLifeCycle --> getProfileSettingInfoFromServer() about to be called");
                        if (!isDataFromServerRequested)
                            getProfileSettingInfoFromServer("");
                        Log.d(TAG, "changeFragment() --> Current index: " + i);
                    } else if (i == CONFIRM_MEMBERSHIP_PAYMENT_FRAGMENT_INDEX) {
//                        getProfileSettingInfoFromServer("");
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        }
    }

    private void setViewsValues() {
        Log.d(TAG, "MemberActivityLifeCycle --> setViewsValues() called");
        if (AppCoreCode.getInstance().cache.memberProfile != null) {
            Glide.with(getApplicationContext())
                    .load(AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .fitCenter()
                    .into(imgSideMenuProfilePhoto);
//            imgSideMenuProfilePhoto.setImageBitmap(AppCoreCode.getInstance().cache.memberAvatarBitmapImg);
            txtSideMenuMemberName.setText(AppCoreCode.getInstance().cache.memberProfile.memberName);
            txtSideMenuTotalSavedAmount.setText(AppCoreCode.getInstance().cache.memberProfile.totalSavedAmount + " " + getResources().getString(R.string.member_main_view_egp));
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Log.d(TAG, "MemberActivityLifeCycle --> onNavigationItemSelected() called");
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            changeFragment(MAIN_VIEW_FRAGMENT_INDEX);
        } else if (id == R.id.nav_profile) {
            changeFragment(PROFILE_FRAGMENT_INDEX);
        } else if (id == R.id.nav_qr_code) {
            changeFragment(CHECK_IN_FRAGMENT_INDEX);
        } else if (id == R.id.nav_notification) {
            changeFragment(NOTIFICATION_FRAGMENT_INDEX);
        } else if (id == R.id.nav_settings) {
            Log.d(TAG, "R.id.nav_settings called");
            if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("en")) {
                Tools.loadLocale(this, "ar");
            } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("ar")) {
                Tools.loadLocale(this, "en");
            }
            if (!isDataFromServerRequested)
                getProfileSettingInfoFromServer("");
        } else if (id == R.id.nav_contact_us) {
            changeFragment(CONTACT_US_FRAGMENT_INDEX);
        } else if (id == R.id.nav_about_us) {
            changeFragment(ABOUT_US_FRAGMENT_INDEX);
        } else if (id == R.id.nav_sign_out_us) {
            logout();
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public void logout() {
        Log.d(TAG, "MemberActivityLifeCycle --> logout() called");

        AppCoreCode.getInstance().saveValuesToSharedPreferences("username", "");
        AppCoreCode.getInstance().saveValuesToSharedPreferences("password", "");
        AppCoreCode.getInstance().cache.signalRConnector.disconnect();
        AppCoreCode.getInstance().cache.signalRBookingNotification.disconnect();

        if (LoginManager.getInstance() != null)
            LoginManager.getInstance().logOut();
        AppCoreCode.getInstance().saveValuesToSharedPreferences("typeOfRequest", "");
        /**
         * Sign out deosn't destroy previous activity
         *
         * Intent Flag FLAG_ACTIVITY_REORDER_TO_FRONT
         * This will simply bring ActivityA to the front of the stack and leave B and C where
         * they are which I believe is what you want.
         * Then you can obviously call finish() on D if you want to remove it from the stack
         */
        Intent intent = new Intent(this, LoginActivity.class);
//        i.setFlags(FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        finish();
        startActivity(intent);

//            startActivity(new Intent(this, LoginActivity.class));
    }

    private void getProfileSettingInfoFromServer(String s) {
        Log.d(TAG, "MemberActivityLifeCycle --> getProfileSettingInfoFromServer() called");
        isDataFromServerRequested = true;
        Log.d(TAG, "getProfileSettingInfoFromServer() called");
        requestType = s;
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "MemberActivityLifeCycle --> onNewIntent() called");
        processIntentAction(intent);
        super.onNewIntent(intent);
    }

    private void processIntentAction(Intent intent) {
        Log.d(TAG, "MemberActivityLifeCycle --> processIntentAction() called");
        if (intent.getAction() != null) {
            switch (intent.getAction()) {
                case "NotificationAction":
                    readNotificationAsyncTaskClass = new ReadNotificationAsyncTaskClass();
                    readNotificationAsyncTaskClass.execute();
                    if (AppCoreCode.getInstance().cache.signalRConnector != null && AppCoreCode.getInstance().cache.signalRConnector.mNotificationManager != null)
                        AppCoreCode.getInstance().cache.signalRConnector.mNotificationManager.cancelAll();
                    break;
            }
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            if (requestType.equalsIgnoreCase("")) {
                getMemberInfo();
                getValidMembership();
                getQRcodeLink();
                getNotifications();
            }
            if (requestType.equalsIgnoreCase("ValidMembership"))
                getValidMembership();
            if (requestType.equalsIgnoreCase("MemberInfo"))
                getMemberInfo();
            if (requestType.equalsIgnoreCase("QRCodeLink"))
                getQRcodeLink();
            if (requestType.equalsIgnoreCase("Notification"))
                getNotifications();

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            isDataFromServerRequested = false;
            try {
                initViews(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // TODO: NPE here cause server delay
        }

        private void getMemberInfo() {
            Log.d(TAG, "getMemberInfo() called");
            String profileInfoResponse;
            try {
                profileInfoResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getprofile");

                JSONObject reader = new JSONObject(profileInfoResponse);
                AppCoreCode.getInstance().cache.memberProfile = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), MemberProfile.class);

//                InputStream in = new java.net.URL(AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl).openStream();
//                AppCoreCode.getInstance().cache.memberAvatarBitmapImg = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getValidMembership() {
            Log.d(TAG, "getValidMembership() called");
            String validMembershipResponse;
            try {
                validMembershipResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getvalidmembership");
                JSONObject reader = new JSONObject(validMembershipResponse);
                Log.d(TAG, "getValidMembership() --> result: " + reader.toString());
                AppCoreCode.getInstance().cache.validMembership = AppCoreCode.getInstance().cache.gson.fromJson(reader.toString(), ValidMembership.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getQRcodeLink() {
            Log.d(TAG, "getQRcodeLink() called");
            String qrCodeResponse;
            try {
                qrCodeResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/membercard/getmemberqrcodelink");
                JSONObject reader = new JSONObject(qrCodeResponse);

                InputStream in = new java.net.URL(AppCoreCode.getInstance().cache.mainURL + "//" + reader.getString("FileName")).openStream();
                AppCoreCode.getInstance().cache.qrCodeFileName = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getNotifications() {
            Log.d(TAG, "getNotifications() called");
            String notificationResponse;
            try {
                notificationResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getnotifications");
                AppCoreCode.getInstance().cache.notification = AppCoreCode.getInstance().cache.gson.fromJson(notificationResponse, Notification.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ReadNotificationAsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String notificationResponse = AppCoreCode.getInstance().httpRest.sendHTTPPutRequest("/api/memberapp/updatenotificationstate"
                        , "{\"id\": \" " + AppCoreCode.getInstance().cache.signalR.id + " \"}");
                Log.d(TAG, "doInBackground() --> notificationResponse: " + notificationResponse);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }
}

package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.PreAppointments;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.Classes.ProviderRate;
import com.axon.memberApp.R;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderRateFragment extends Fragment {
    private View view;
    private Button btnProviderRateSubmit;
    private ImageButton ibtnProviderRateBack;
    private TextView txtProviderRateName;
    private EditText etxtProviderRateUserComment;
    private RatingBar rateBarProviderRate;
    private CircleImageView imgProviderRateAvatarURL;

    private PreAppointments.Items currentSelectedProviderItem;

    private AsyncTaskClass asyncTaskClass;
    private String getAppointmentReviewResponse;
    private String setAppointmentReviewResponse;
    private String typeOfServerOperation;

    private String TAG = "ProviderRateFragment";
    private float rate;
    private String serverMsg;

    public ProviderRateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.provider_rate_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null && isAdded() && getActivity() != null) {
            initViews();
        }
    }

    private void initViews() {
        ibtnProviderRateBack = view.findViewById(R.id.ibtnProviderRateBack);
        txtProviderRateName = view.findViewById(R.id.txtProviderRateName);
        rateBarProviderRate = view.findViewById(R.id.rateBarProviderRate);
        etxtProviderRateUserComment = view.findViewById(R.id.etxtProviderRateUserComment);
        btnProviderRateSubmit = view.findViewById(R.id.btnProviderRateSubmit);
        imgProviderRateAvatarURL = view.findViewById(R.id.imgProviderRateAvatarURL);

        getSetAppointmentReviewFromServer("GET");
        setViewsValues();
        setViewsActions();
    }

    private void getSetAppointmentReviewFromServer(String typeOfServerOperation) {
        this.typeOfServerOperation = typeOfServerOperation;
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void setViewsValues() {
        // TODO: NPE here cause Server delay
        if (AppCoreCode.getInstance().cache.preAppointments != null
                && AppCoreCode.getInstance().cache.preAppointments.itemsList != null
                && AppCoreCode.getInstance().cache.preAppointments.itemsList.size() > 0) {
            currentSelectedProviderItem = AppCoreCode.getInstance().cache.preAppointments.itemsList.get(AppCoreCode.getInstance().cache.currentSelectedProviderListIndex);
            txtProviderRateName.setText(currentSelectedProviderItem.providerName);
            if (!currentSelectedProviderItem.providerAvatarUrl.contains("nophoto-slider")) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        final Drawable drawable = Tools.LoadImageFromWebOperations(AppCoreCode.getInstance().cache.mainURL + "/" + currentSelectedProviderItem.providerAvatarUrl);
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imgProviderRateAvatarURL.setImageDrawable(drawable);
                                }
                            });
                        }
                    }
                });
            }

            if (currentSelectedProviderItem.rate == 0 && currentSelectedProviderItem.reviewId == 0) {
                setViewsVisibilityDependsOnRateData(true);
            } else {
                setViewsVisibilityDependsOnRateData(false);
            }
        }
    }

    private void setViewsVisibilityDependsOnRateData(boolean enabled) {
        btnProviderRateSubmit.setEnabled(enabled);
        etxtProviderRateUserComment.setEnabled(enabled);
        rateBarProviderRate.setEnabled(enabled);
        if (enabled) {
            btnProviderRateSubmit.setAlpha(1);
        } else {
            btnProviderRateSubmit.setAlpha((float) 0.5);
        }
    }

    private void setViewsActions() {
        ibtnProviderRateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(5);
            }
        });

        btnProviderRateSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSetAppointmentReviewFromServer("Post");
            }
        });

        rateBarProviderRate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d(TAG, "onRatingChanged() --> rating: " + rating);
                rate = rating;
            }
        });
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfServerOperation.equalsIgnoreCase("Get") && currentSelectedProviderItem != null) {
                    getAppointmentReviewResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getappointmentreview?appointmentId=" + currentSelectedProviderItem.appointmentId);
                    Log.d(TAG, "doInBackground() --> getAppointmentReviewResponse: " + getAppointmentReviewResponse);
                    AppCoreCode.getInstance().cache.providerRate = AppCoreCode.getInstance().cache.gson.fromJson(getAppointmentReviewResponse, ProviderRate.class);
                } else {
                    if (currentSelectedProviderItem != null) {
                        setAppointmentReviewResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/createappointmentreview"
                                , "{\"appointmentId\": " + currentSelectedProviderItem.appointmentId + ",\"rate\": " + rate +
                                        " ,\"reviewText\": \"" + etxtProviderRateUserComment.getText().toString().trim() + "\"}");
                        Log.d(TAG, "doInBackground() --> setAppointmentReviewResponse: " + setAppointmentReviewResponse);
                        JSONObject reader = new JSONObject(setAppointmentReviewResponse);
                        boolean success = reader.getBoolean("success");
                        Log.d(TAG, "doInBackground() --> setAppointmentReviewResponse success status: " + success);
                        if (success) {
                            serverMsg = "Thanks for review!";
                        } else {
                            JSONObject error = reader.getJSONObject("error");
                            if (error != null) {
                                serverMsg = error.getString("message");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "doInBackground() --> Exception: " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (typeOfServerOperation.equalsIgnoreCase("Get")) {
                if (AppCoreCode.getInstance().cache.providerRate != null && AppCoreCode.getInstance().cache.providerRate.result != null) {
                    rateBarProviderRate.setRating((float) AppCoreCode.getInstance().cache.providerRate.result.rate);
                    etxtProviderRateUserComment.setText(AppCoreCode.getInstance().cache.providerRate.result.reviewText);
                }
            } else {
                Toast.makeText(getActivity(), serverMsg, Toast.LENGTH_LONG).show();
                if (getActivity() != null)
                    ((MemberMainViewActivity) getActivity()).changeFragment(5);
            }
        }
    }
}

package com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.axon.memberApp.Global.AppCoreCode;

public class ProviderProfileTabAdapter extends FragmentStatePagerAdapter {
    private String TAG = "ProviderProfileTabAdapter";
    int numberOfTabs;
    public ProviderProfileTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem() --> position: " + position);
        switch (position) {
            case 0:
               AppCoreCode.getInstance().cache.providerProfileInfoTabFragment = new ProviderProfileInfoTabFragment();
                return AppCoreCode.getInstance().cache.providerProfileInfoTabFragment;
            case 1:
                AppCoreCode.getInstance().cache.providerProfileBookingTabFragment = new ProviderProfileBookingTabFragment();
                return AppCoreCode.getInstance().cache.providerProfileBookingTabFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}

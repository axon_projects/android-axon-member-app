package com.axon.memberApp.Global;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;

import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;

import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AppCoreCode {
    /*Shared Preferences*/
    public SharedPreferences sharedpreferences;
    public SharedPreferences.Editor editor;

    public SweetAlertDialog sweetAlertDialog;

    public LoginActivity loginActivity;
    public MemberMainViewActivity memberMainViewActivity;
    public Cache cache;
    public JsonParser jsonParser;

    public HTTPRest httpRest = new HTTPRest();

    private static AppCoreCode appCoreCodeObject;

    public static AppCoreCode getInstance() {
        if (appCoreCodeObject == null) {
            appCoreCodeObject = new AppCoreCode();
            Log.d("AppCoreCode", "AppService --> getInstance() called");
        }
        return appCoreCodeObject;
    }

    private AppCoreCode() {
        loginActivity = new LoginActivity();
        memberMainViewActivity = new MemberMainViewActivity();
        cache = new Cache();
        jsonParser = new JsonParser();
    }

    public void saveValuesToSharedPreferences(String key, boolean flag) {
        if (editor != null) {
            editor.putBoolean(key, flag);
            editor.commit();
        }
    }

    public void saveValuesToSharedPreferences(String key, String value) {
        if (editor != null) {
            editor.putString(key, value);
            editor.commit();
        }
    }

    public boolean getBooleanValuesFromSharedPreferences(String key) {
        if (sharedpreferences != null) {
            return sharedpreferences.getBoolean(key, false);
        } else {
            return false;
        }
    }

    public String getStringValuesFromSharedPreferences(String key) {
        if (sharedpreferences != null) {
            return sharedpreferences.getString(key, "");
        } else {
            return "";
        }
    }
}

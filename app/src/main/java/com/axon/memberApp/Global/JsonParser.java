package com.axon.memberApp.Global;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

    public void getAccessTokenOnMemberLogin(Activity context, String response) {
        JSONObject reader = new JSONObject();
//        if (!reader.isNull(response)) {
            try {
                reader = new JSONObject(response);
                boolean loginSuccess = reader.getBoolean("success");
                if (loginSuccess) {
                    JSONObject resultObj = reader.getJSONObject("result");
                    String accountType = resultObj.getString("accountType");
                    if (accountType.equals("Member")) {
                        AppCoreCode.getInstance().cache.accessToken = resultObj.getString("result");
                        context.startActivity(new Intent(context, MemberMainViewActivity.class));
                    } else {
                        // Client isn't a member, but AxProvider
                        Toast.makeText(context, "Sorry, you need to Provider App to Login!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    JSONObject errorObj = reader.getJSONObject("error");
                    Toast.makeText(context, errorObj.getString("details").replace("[", "").replace("]", ""), Toast.LENGTH_LONG).show();
                }
                Log.d("JsonParser", "getAccessTokenOnMemberLogin() --> Access Token: " + AppCoreCode.getInstance().cache.accessToken);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
            }
//        }else {
//            try {
//                JSONObject responseWithNull = new JSONObject(response);
//                JSONObject errorObj = responseWithNull.getJSONObject("error");
//                Toast.makeText(context, errorObj.getString("details").replace("[", "").replace("]", ""), Toast.LENGTH_LONG).show();
//            } catch (JSONException e) {
//                e.printStackTrace();
//                Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
//            }
//        }
    }


}

package com.axon.memberApp.Global;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

public class HTTPRest {
    private String TAG = "HTTPRest";

    // HTTP GET request
    public String sendHTTPGetRequest(String url) throws Exception {
        URL obj = new URL(AppCoreCode.getInstance().cache.mainURL + url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestProperty("Authorization", "Bearer " + AppCoreCode.getInstance().cache.accessToken);
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("en")) {
            con.setRequestProperty("LanguageCode", "en"); // "en"
        } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("ar")) {
            con.setRequestProperty("LanguageCode", "ar"); // "en"
        }else {
            con.setRequestProperty("LanguageCode", "en"); // "en"
        }

        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        int responseCode = con.getResponseCode();
        Log.d("HTTPRest", "sendHTTPGetRequest() --> Sending 'GET' request to URL: " + AppCoreCode.getInstance().cache.mainURL);
        Log.d("HTTPRest", "sendHTTPGetRequest() --> Response Code: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        Log.d("HTTPRest", "sendHTTPGetRequest() --> response: " + response.toString());
        return response.toString();
    }

    // HTTP POST request
    public String sendHTTPPutRequest(String url, String json) throws Exception {
        URL obj = new URL(AppCoreCode.getInstance().cache.mainURL + url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection(); // HttpsURL
        StringBuffer response = new StringBuffer();
        try {
            con.setRequestMethod("PUT");
            if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("en")) {
                con.setRequestProperty("LanguageCode", "en"); // "en"
            } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("ar")) {
                con.setRequestProperty("LanguageCode", "ar"); // "en"
            }else {
                con.setRequestProperty("LanguageCode", "en"); // "en"
            }
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + AppCoreCode.getInstance().cache.accessToken);

            if (!json.isEmpty()) {
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(json);
                wr.flush();
                wr.close();
            }

            int responseCode = con.getResponseCode();
            Log.d("HTTPRest", "sendHTTPPutRequest() --> Sending 'PUT' request to URL : " + url);
            Log.d("HTTPRest", "sendHTTPPutRequest() --> Post Data : " + json);
            Log.d("HTTPRest", "sendHTTPPutRequest() --> Response Code : " + responseCode);

            BufferedReader in;
            if (responseCode == 500 || responseCode == 400 || responseCode == 401) {
                in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            } else {
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            }
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.d("HTTPRest", "sendHTTPPutRequest() --> response: " + response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("HTTPRest", "sendHTTPPutRequest() --> doInBackground exception: " + e.getMessage());
        }
        return response.toString();
    }

    public String POST_Data(String filepath) throws Exception {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        String[] q = filepath.split("/");
        int idx = q.length - 1;
        File file = new File(filepath);
        FileInputStream fileInputStream = new FileInputStream(file);
        URL url = new URL(AppCoreCode.getInstance().cache.mainURL + "/api/memberapp/saveprofileavatar");
        connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        connection.setRequestProperty("Authorization", "Bearer " + AppCoreCode.getInstance().cache.accessToken);
        outputStream = new DataOutputStream(connection.getOutputStream());
        outputStream.writeBytes("--" + boundary + "\r\n");
        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "img_upload" + "\"; filename=\"" + q[idx] + "\"" + "\r\n");
        outputStream.writeBytes("Content-Type: image/jpeg" + "\r\n");
        outputStream.writeBytes("Content-Transfer-Encoding: binary" + "\r\n");
        outputStream.writeBytes("\r\n");
        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, 1048576);
        buffer = new byte[bufferSize];
        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, 1048576);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        outputStream.writeBytes("\r\n");
        outputStream.writeBytes("--" + boundary + "--" + "\r\n");
        inputStream = connection.getInputStream();
        int status = connection.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            int responseCode = connection.getResponseCode();
            Log.d("HTTPRest", "sendHTTPPutRequest() --> Sending 'POST' request to URL : " + url);
            Log.d("HTTPRest", "sendHTTPPutRequest() --> Response Code : " + responseCode);

            inputStream.close();
            connection.disconnect();
            fileInputStream.close();
            outputStream.flush();
            outputStream.close();
            return response.toString();
        } else {
            throw new Exception("Non ok response returned");
        }
    }

    // HTTP POST request
    public String sendHTTPPostRequest(String url, String json) throws Exception {
        URL obj = new URL(AppCoreCode.getInstance().cache.mainURL + url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection(); // HttpsURL
        StringBuffer response = new StringBuffer();
        try {
            con.setRequestMethod("POST");
            if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("en")) {
                con.setRequestProperty("LanguageCode", "en"); // "en"
            } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("ar")) {
                con.setRequestProperty("LanguageCode", "ar"); // "en"
            }else {
                con.setRequestProperty("LanguageCode", "en"); // "en"
            }
            con.setRequestProperty("Content-Type", "application/json"); // ; charset=utf-8
            if (url.equalsIgnoreCase("/api/memberapp/getnextappointments")
                    || url.equalsIgnoreCase("/api/memberapp/getpreviousappointments")
                    || url.equalsIgnoreCase("/api/memberapp/updateprofileinfo")
                    || url.equalsIgnoreCase("/api/memberapp/cancelappointment")
                    || url.equalsIgnoreCase("/api/memberapp/createappointmentreview")
                    || url.equalsIgnoreCase("/api/memberapp/cities")
                    || url.equalsIgnoreCase("/api/memberapp/areas")
                    || url.equalsIgnoreCase("/api/memberapp/services")
                    || url.equalsIgnoreCase("/api/memberapp/getprovidersbysearch")
                    || url.equalsIgnoreCase("/api/memberapp/getproviderinfo")
                    || url.equalsIgnoreCase("/api/memberapp/getmembershipfeesinfo")
                    || url.equalsIgnoreCase("/api/memberapp/createmembership")
                    || url.equalsIgnoreCase("/api/memberapp/getproviderreservationtime")
                    || url.equalsIgnoreCase("/api/memberapp/bookappointment")
                    || url.equalsIgnoreCase("/api/authtoken/FacebookSignUp")
                    || url.equalsIgnoreCase("/api/authtoken/FacebookLogin")
            || url.equalsIgnoreCase("/api/memberapp/changepassword")
            || url.equalsIgnoreCase("/api/memberapp/getfavoritelist")
            || url.equalsIgnoreCase("/api/memberapp/favoriteupsert")
            || url.equalsIgnoreCase("/api/memberapp/contactus")) {
                con.setRequestProperty("Authorization", "Bearer " + AppCoreCode.getInstance().cache.accessToken);
            }

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            // To send arabic words --> wr.write(json.getBytes()); instead of ws.writeBytes(json);
            // Ref --> https://stackoverflow.com/questions/18759199/java-utf-8-encoding-not-working-httpurlconnection
            wr.write(json.getBytes());
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            Log.d("HTTPRest", "sendHTTPPostRequest() --> Sending 'POST' request to URL : " + url);
            Log.d("HTTPRest", "sendHTTPPostRequest() --> Post Data : " + json);
            Log.d("HTTPRest", "sendHTTPPostRequest() --> Response Code : " + responseCode);

            BufferedReader in;
            if (responseCode == 500 || responseCode == 400 || responseCode == 401) {
                in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            } else {
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            }
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.d("HTTPRest", "sendHTTPPostRequest() --> response: " + response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("HTTPRest", "sendHTTPPostRequest() --> doInBackground exception: " + e.getMessage());
        }
        return response.toString();
    }
}

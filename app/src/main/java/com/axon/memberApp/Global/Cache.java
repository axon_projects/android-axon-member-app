package com.axon.memberApp.Global;

import android.graphics.Bitmap;
import android.net.Uri;

import com.axon.memberApp.Login.Classes.LoginMsg;
import com.axon.memberApp.Login.LoginFragments.LoginFragment;
import com.axon.memberApp.Login.SignUpFragments.SignUpWithoutMailFragment;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.Membership;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.MembershipFeesInfo;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.ValidMembership;
import com.axon.memberApp.MemberMainActivity.MemberNotification.Classes.Notification;
import com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR.BookingNotification;
import com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR.SignalR;
import com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR.SignalRBookingNotification;
import com.axon.memberApp.MemberMainActivity.MemberNotification.SignalR.SignalRConnector;
import com.axon.memberApp.MemberMainActivity.MemberProfile.Classes.MemberProfile;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.Favorites;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.NextAppointments;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.Classes.PreAppointments;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.Classes.CancellationReasons;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.Classes.ProviderRate;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.BookingAppointment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderProfile;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.Classes.ProviderReservations;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.ProviderProfileBookingTabFragment;
import com.axon.memberApp.MemberMainActivity.MemberProfile.ProfileTabFragments.ProfileSingleRowAction.ProviderProfile.ProviderTabs.ProviderProfileInfoTabFragment;
import com.axon.memberApp.MemberMainActivity.MemberSearch.AdvancedSearchFragment;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.AdvancedSearch;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Areas;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Cities;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Services;
import com.axon.memberApp.MemberMainActivity.MemberSearch.Classes.Specializations;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.gson.Gson;

public class Cache {
    public Gson gson = new Gson();
    /**
     * Change password / Forget password
     */
    public String mainURL = "https://www.axontest.axon1.com";
    //    public String mainURL = "http://sogoko-001-site1.atempurl.com";
//    public String mainURL = "https://www.axon1.com";
    public String accessToken = "";
    public int currentNextAppointToCancelId;

    public GoogleSignInClient mGoogleSignInClient;

    public boolean isBrowserOpened = false;

    public boolean isSearchLoading = false;

    public boolean isSignUpRequest;
    public boolean isAppLanguageChanged;

    public String forgetPasswordEmailResult = "";

    public String googleEmail;
    public String googleAccessToken;
    public boolean isGoogleSignUp = false;

    public String facebookEmail;
    public String facebookAccessToken;
    public boolean isFacebookSignUp = false;

    public SignUpWithoutMailFragment signUpWithoutMailFragment;
    public LoginFragment loginFragment;

    public SignalRConnector signalRConnector = new SignalRConnector();
    public SignalRBookingNotification signalRBookingNotification = new SignalRBookingNotification();
    public SignalR signalR;
    public BookingNotification bookingNotification;

    public LoginMsg loginMsg;
    public MemberProfile memberProfile;
    public ValidMembership validMembership;
    public Notification notification;
    public NextAppointments nextAppointments;
    public PreAppointments preAppointments;
    public Favorites favorites;
    public CancellationReasons cancellationReasons;
    public ProviderProfile providerProfile;
    public ProviderReservations providerReservations;
    public BookingAppointment bookingAppointment;
    public ProviderRate providerRate;
    public Uri selectedImage;

    public String fawryPaymentSource = "";

    public String currentImageSlider = "";
    public int currentImageSliderIndex;

    public int lastFragmentIndexBeforeProfileProvider;

    public boolean isBookingTabFragmentIsVisible;

    public String strQuickSearchProviderName;
    public AdvancedSearch advancedSearch;
    public Membership membership;
    public String membershipJsonToServer;
    public MembershipFeesInfo membershipFeesInfo;
    public String advancedSearchJsonToSend;
    public String quickSearchJsonToSend;
    public Cities cities;
    public Services services;
    public Areas areas;
    public Specializations specializations;
    public int currentSelectedProviderIndexFromSearchResults;
    public String currentSelectedProviderTypeFromSearchResults;

    public ProviderProfileInfoTabFragment providerProfileInfoTabFragment;
    public ProviderProfileBookingTabFragment providerProfileBookingTabFragment;

    public int currentSelectedProviderListIndex;

    public Bitmap memberAvatarBitmapImg = null;
    public Bitmap qrCodeFileName;
}

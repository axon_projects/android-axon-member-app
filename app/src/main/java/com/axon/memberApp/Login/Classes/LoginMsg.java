package com.axon.memberApp.Login.Classes;

public class LoginMsg {
    public Result result = new Result();
    public Error error = new Error();
    public String targetUrl;
    public boolean success;
    public boolean unAuthorizedRequest;
    public boolean __abp;

    public class Result{
        public String result;
        public String roleName;
        public String accountType;
        public String accountVerification;
    }

    public class Error{
        public int code;
        public String message;
        public String details;
        public String validationErrors;
    }
}

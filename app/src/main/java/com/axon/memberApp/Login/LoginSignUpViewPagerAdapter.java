package com.axon.memberApp.Login;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import android.util.Log;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Login.LoginFragments.LoginFragment;
import com.axon.memberApp.Login.LoginFragments.LoginSignUpFragment;
import com.axon.memberApp.Login.LoginFragments.ResetPasswordFragment;
import com.axon.memberApp.Login.LoginFragments.ResetPasswordLinkFragment;
import com.axon.memberApp.Login.SignUpFragments.SignUpWithMailFragment;
import com.axon.memberApp.Login.SignUpFragments.SignUpWithoutMailFragment;

public class LoginSignUpViewPagerAdapter extends FragmentStatePagerAdapter {
    // Declare Login Fragments
    private LoginSignUpFragment loginSignUpFragment; // 0
    /*private LoginFragment loginFragment; // 1*/
    private ResetPasswordFragment resetPasswordFragment; // 2
    private ResetPasswordLinkFragment resetPasswordLinkFragment; // 3
    // Declare SignUp Fragments
    /*public SignUpWithoutMailFragment signUpWithoutMailFragment; // 4*/
    private SignUpWithMailFragment signUpWithMailFragment; //5

    public LoginSignUpViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.w("LoginSignUpViewPager", "getItem() --> position: " + position);
        if (position == 0) {
            if (loginSignUpFragment == null)
                loginSignUpFragment = new LoginSignUpFragment();
            return loginSignUpFragment;
        } else if (position == 1) {
            if (AppCoreCode.getInstance().cache.loginFragment == null)
                AppCoreCode.getInstance().cache.loginFragment = new LoginFragment();
            return AppCoreCode.getInstance().cache.loginFragment;
        } else if (position == 2) {
            if (resetPasswordFragment == null)
                resetPasswordFragment = new ResetPasswordFragment();
            return resetPasswordFragment;
        } else if (position == 3) {
            if (resetPasswordLinkFragment == null)
                resetPasswordLinkFragment = new ResetPasswordLinkFragment();
            return resetPasswordLinkFragment;
        } else if (position == 4) {
            if (AppCoreCode.getInstance().cache.signUpWithoutMailFragment == null) {
                AppCoreCode.getInstance().cache.signUpWithoutMailFragment = new SignUpWithoutMailFragment();
            }
            return AppCoreCode.getInstance().cache.signUpWithoutMailFragment;
        } else if (position == 5) {
            if (signUpWithMailFragment == null)
                signUpWithMailFragment = new SignUpWithMailFragment();
            return signUpWithMailFragment;
        } else {
            return null;
        }

    }

    @Override
    public int getCount() {
        return 6;
    }
}

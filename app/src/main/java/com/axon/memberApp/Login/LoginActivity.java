package com.axon.memberApp.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.CustomViewPager;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class LoginActivity extends AppCompatActivity {
    final int LOGIN_SIGN_UP_FRAGMENT_INDEX = 0;
    final int LOGIN_FRAGMENT_INDEX = 1;
    final int RESET_PASSWORD_FRAGMENT_INDEX = 2;
    final int RESET_PASSWORD_LINK_FRAGMENT_INDEX = 3;
    final int SIGNUP_WITHOUT_MAIL_FRAGMENT_INDEX = 4;
    final int SIGNUP_WITH_MAIL_FRAGMENT_INDEX = 5;

    private String TAG = "LoginActivity";

    private CustomViewPager viewPager;
    public LoginSignUpViewPagerAdapter loginSignUpViewPagerAdapter;

    public String fullName, birthDate, mobileNo, gender, email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!AppCoreCode.getInstance().cache.isAppLanguageChanged) {
            if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language") != null) {
                if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").isEmpty()) {
                    AppCoreCode.getInstance().saveValuesToSharedPreferences("Language", Tools.getAppLanguage());
                } else {
                    Tools.loadLocale(this, AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language"));
                }
            }
        }
        setupViewPager();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w(TAG, "onActivityResult() -> Google Signed in successfully!!!");
        if (requestCode == 3) {
            if (AppCoreCode.getInstance().cache.signUpWithoutMailFragment != null)
                AppCoreCode.getInstance().cache.signUpWithoutMailFragment.updateUIOnGoogleSignInIntent(data);
        }else if (requestCode == 4) {
            if (AppCoreCode.getInstance().cache.loginFragment != null)
                AppCoreCode.getInstance().cache.loginFragment.updateUIOnGoogleSignInIntent(data);
        }
    }

    private void checkIfUserAlreadySignedIn() {
        if (AppCoreCode.getInstance().sharedpreferences != null && !AppCoreCode.getInstance().getStringValuesFromSharedPreferences("username").isEmpty() && !AppCoreCode.getInstance().getStringValuesFromSharedPreferences("password").isEmpty()) {
            changeFragment(LOGIN_FRAGMENT_INDEX);
        } else {
            changeFragment(LOGIN_SIGN_UP_FRAGMENT_INDEX);
        }
    }

    private void setupViewPager() {
        viewPager = findViewById(R.id.loginContainer);
        loginSignUpViewPagerAdapter = new LoginSignUpViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(loginSignUpViewPagerAdapter);
        viewPager.setPagingEnabled(false);
        checkIfUserAlreadySignedIn();
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == LOGIN_SIGN_UP_FRAGMENT_INDEX)
            super.onBackPressed();
        else {
            changeFragment(LOGIN_SIGN_UP_FRAGMENT_INDEX);
        }
    }

    public void changeFragment(int fragmentIndex) {
        Log.w("LoginActivity", "changeFragment() --> fragmentType: " + fragmentIndex + " -- ViewPager: " + viewPager);
        viewPager.setCurrentItem(fragmentIndex);
    }
}

package com.axon.memberApp.Login.LoginFragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPasswordLinkFragment extends Fragment {
    private View view;
    private ImageButton ibtnBack;
    private Button btnDone;
    private TextView etxtEmailToRememberPassword;

    public ResetPasswordLinkFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.reset_password_link_fragment, container, false);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            initViews();
        }
    }

    private void initViews() {
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnDone = view.findViewById(R.id.btnDone);
        etxtEmailToRememberPassword = view.findViewById(R.id.etxtEmailToRememberPassword);

        if (AppCoreCode.getInstance().cache.forgetPasswordEmailResult != null && !AppCoreCode.getInstance().cache.forgetPasswordEmailResult.isEmpty()) {
            etxtEmailToRememberPassword.setText(AppCoreCode.getInstance().cache.forgetPasswordEmailResult);
        }

        setViewActions();
    }

    private void setViewActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).changeFragment(2);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).changeFragment(1);
            }
        });
    }
}

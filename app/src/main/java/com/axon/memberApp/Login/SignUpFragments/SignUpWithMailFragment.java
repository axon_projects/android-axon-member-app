package com.axon.memberApp.Login.SignUpFragments;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpWithMailFragment extends Fragment {
    private View view;
    private ImageButton ibtnSignUpWithMailBack;
    private Button btnSignUp;
    private EditText etxtSignUpEmail, etxtSignUpPassword, etxtSignUpConfirmPassword;

    private AsyncTaskClass asyncTaskClass;
    private String signUpResponse;
    private String msg;
    private boolean success;
    private String TAG = "SignUpWithMailFragment";

    public SignUpWithMailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.sign_up_with_mail_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && view != null && AppCoreCode.getInstance().cache.isSignUpRequest
                && etxtSignUpEmail != null && etxtSignUpPassword != null && etxtSignUpConfirmPassword != null) {
            etxtSignUpEmail.setText("");
            etxtSignUpPassword.setText("");
            etxtSignUpConfirmPassword.setText("");
            if (AppCoreCode.getInstance().cache.facebookEmail != null && !AppCoreCode.getInstance().cache.facebookEmail.isEmpty()) {
                etxtSignUpEmail.setText(AppCoreCode.getInstance().cache.facebookEmail);
                etxtSignUpEmail.setEnabled(false);
                if (AppCoreCode.getInstance().cache.isFacebookSignUp) {
                    etxtSignUpPassword.setVisibility(View.GONE);
                    etxtSignUpConfirmPassword.setVisibility(View.GONE);
                }
            } else if (AppCoreCode.getInstance().cache.googleEmail != null && !AppCoreCode.getInstance().cache.googleEmail.isEmpty()) {
                etxtSignUpEmail.setText(AppCoreCode.getInstance().cache.googleEmail);
                etxtSignUpEmail.setEnabled(false);
                if (AppCoreCode.getInstance().cache.isGoogleSignUp) {
                    etxtSignUpPassword.setVisibility(View.GONE);
                    etxtSignUpConfirmPassword.setVisibility(View.GONE);
                }
            }
        }
    }

    private void initViews() {
        ibtnSignUpWithMailBack = view.findViewById(R.id.ibtnSignUpWithMailBack);
        etxtSignUpEmail = view.findViewById(R.id.etxtSignUpEmail);
        etxtSignUpPassword = view.findViewById(R.id.etxtSignUpPassword);
        etxtSignUpConfirmPassword = view.findViewById(R.id.etxtSignUpConfirmPassword);
        btnSignUp = view.findViewById(R.id.btnSignUp);

        setViewsActions();
    }

    private void setViewsActions() {
        ibtnSignUpWithMailBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().cache.isSignUpRequest = false;
                ((LoginActivity) getActivity()).changeFragment(4);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInputs();
            }
        });
    }

    private void validateUserInputs() {
        if (etxtSignUpEmail.getText().toString().trim().isEmpty()) {
            etxtSignUpEmail.setError(getResources().getString(R.string.view_error_email_required));
            etxtSignUpEmail.requestFocus();
            return;
        }
        if (!Tools.isValidEmailAddress(etxtSignUpEmail.getText().toString().trim())) {
            etxtSignUpEmail.setError(getResources().getString(R.string.view_error_email_wrong));
            etxtSignUpEmail.requestFocus();
            return;
        }
        if (AppCoreCode.getInstance().cache.facebookEmail == null && AppCoreCode.getInstance().cache.googleEmail == null) {
            if (etxtSignUpPassword.getText().toString().trim().isEmpty()) {
                etxtSignUpPassword.setError(getResources().getString(R.string.view_error_password_required));
                etxtSignUpPassword.requestFocus();
                return;
            }
            if (etxtSignUpConfirmPassword.getText().toString().trim().isEmpty()) {
                etxtSignUpConfirmPassword.setError(getResources().getString(R.string.view_error_confirm_password_required));
                etxtSignUpConfirmPassword.requestFocus();
                return;
            }
            if (!etxtSignUpPassword.getText().toString().trim().equalsIgnoreCase(etxtSignUpConfirmPassword.getText().toString().trim())) {
                etxtSignUpConfirmPassword.setError(getResources().getString(R.string.view_error_confirm_email_not_match));
                etxtSignUpConfirmPassword.requestFocus();
                return;
            }
            AppCoreCode.getInstance().loginActivity.password = etxtSignUpPassword.getText().toString().trim();
        }
        AppCoreCode.getInstance().loginActivity.email = etxtSignUpEmail.getText().toString().trim();

        createNewMember();
    }

    private void createNewMember() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private void showLoadingDialog() {
        AppCoreCode.getInstance().sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        AppCoreCode.getInstance().sweetAlertDialog.getProgressHelper().setBarColor(R.color.axonTextDarkPurpleColor);
        AppCoreCode.getInstance().sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_loading));
        AppCoreCode.getInstance().sweetAlertDialog.setCancelable(true);
        AppCoreCode.getInstance().sweetAlertDialog.show();
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
            showLoadingDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (AppCoreCode.getInstance().cache.isFacebookSignUp) {
                    signUpResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/authtoken/FacebookSignUp"
                            , "{\"accessToken\": \"" + AppCoreCode.getInstance().cache.facebookAccessToken + "\" ,\"name\": \"" + AppCoreCode.getInstance().loginActivity.fullName + "\",\"mobileNumber\": \"" + AppCoreCode.getInstance().loginActivity.mobileNo
                                    + "\",\"gender\": \"" + AppCoreCode.getInstance().loginActivity.gender + "\" ,\"birthDate\": \""
                                    + AppCoreCode.getInstance().loginActivity.birthDate + "\"}");
                    Log.d("SignUpWithMailFragment", "doInBackground() --> signUpResponse: " + signUpResponse);
                    JSONObject reader = new JSONObject(signUpResponse);
                    success = reader.getBoolean("success");
                    Log.d("SignUpWithMailFragment", "doInBackground() --> signUp success status: " + success);
                    if (success) {
                        msg = getActivity().getResources().getString(R.string.login_signed_up_successfully);
                    } else {
                        JSONObject error = reader.getJSONObject("error");
                        if (error != null) {
                            msg = error.getString("message");
                        }
                    }
                } else if (AppCoreCode.getInstance().cache.isGoogleSignUp) {
                    signUpResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/authtoken/GoogleSignUp"
                            , "{\"accessToken\": \"" + AppCoreCode.getInstance().cache.googleAccessToken + "\" ,\"name\": \"" + AppCoreCode.getInstance().loginActivity.fullName + "\",\"mobileNumber\": \"" + AppCoreCode.getInstance().loginActivity.mobileNo
                                    + "\",\"gender\": \"" + AppCoreCode.getInstance().loginActivity.gender + "\" ,\"birthDate\": \""
                                    + AppCoreCode.getInstance().loginActivity.birthDate + "\"}");
                    Log.d("SignUpWithMailFragment", "doInBackground() --> signUpResponse: " + signUpResponse);
                    JSONObject reader = new JSONObject(signUpResponse);
                    success = reader.getBoolean("success");
                    Log.d("SignUpWithMailFragment", "doInBackground() --> signUp success status: " + success);
                    if (success) {
                        msg = getActivity().getResources().getString(R.string.login_signed_up_successfully);
                    } else {
                        JSONObject error = reader.getJSONObject("error");
                        if (error != null) {
                            msg = error.getString("message");
                        }
                    }
                } else {
                    signUpResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/signup"
                            , "{\"name\": \"" + AppCoreCode.getInstance().loginActivity.fullName + "\",\"mobileNumber\": \"" + AppCoreCode.getInstance().loginActivity.mobileNo
                                    + "\",\"emailAddress\": \"" + etxtSignUpEmail.getText().toString().trim() + "\",\"password\": \"" + AppCoreCode.getInstance().loginActivity.password + "\",\"gender\": \""
                                    + AppCoreCode.getInstance().loginActivity.gender + "\",\"birthDate\": \""
                                    + AppCoreCode.getInstance().loginActivity.birthDate + "\"}");
                    Log.d("SignUpWithMailFragment", "doInBackground() --> signUpResponse: " + signUpResponse);
                    JSONObject reader = new JSONObject(signUpResponse);
                    success = reader.getBoolean("success");
                    Log.d("SignUpWithMailFragment", "doInBackground() --> signUp success status: " + success);
                    if (success) {
                        msg = getActivity().getResources().getString(R.string.login_signed_up_successfully);
                    } else {
                        JSONObject error = reader.getJSONObject("error");
                        if (error != null) {
                            msg = error.getString("message");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (AppCoreCode.getInstance().sweetAlertDialog != null)
                AppCoreCode.getInstance().sweetAlertDialog.dismissWithAnimation();
            if (getActivity() != null) {
                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                if (success) {
                    AppCoreCode.getInstance().cache.isSignUpRequest = false;

                    ((LoginActivity) getActivity()).changeFragment(1);
                }
            }
        }
    }
}

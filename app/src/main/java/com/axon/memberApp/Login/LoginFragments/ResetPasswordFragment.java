package com.axon.memberApp.Login.LoginFragments;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPasswordFragment extends Fragment {
    private View view;
    private ImageButton ibtnBack;
    private Button btnReset;
    private EditText etxtEmailToRememberPassword;

    private AsyncTaskClass asyncTaskClass;
    private String forgetResponse;
    private String msg;
    private String TAG = "ResetPasswordFragment";
    private boolean success;

    public ResetPasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.reset_password_fragment, container, false);

        initViews();

        return view;
    }

    private void initViews() {
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnReset = view.findViewById(R.id.btnReset);
        etxtEmailToRememberPassword = view.findViewById(R.id.etxtEmailToRememberPassword);
        setViewActions();
    }

    private void setViewActions() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).changeFragment(1);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etxtEmailToRememberPassword.getText().toString().trim().isEmpty()) {
                    etxtEmailToRememberPassword.setError(getResources().getString(R.string.view_error_email_required));
                    etxtEmailToRememberPassword.requestFocus();
                    return;
                }
                getDataFromServer();
            }
        });
    }

    private void getDataFromServer() {
        asyncTaskClass = new AsyncTaskClass();
        asyncTaskClass.execute();
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                forgetResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/memberapp/forgetpassword"
                        , "{\"emailAddress\": \"" + etxtEmailToRememberPassword.getText().toString().trim() + "\"}");

                Log.d(TAG, "doInBackground() --> forgetResponse: " + forgetResponse);
                JSONObject reader = new JSONObject(forgetResponse);
                success = reader.getBoolean("success");
                Log.d(TAG, "doInBackground() --> forget password success status: " + success);
                if (success) {
                    AppCoreCode.getInstance().cache.forgetPasswordEmailResult = reader.getString("result");
                    msg = getResources().getString(R.string.member_password_success_update);
                } else {
                    JSONObject error = reader.getJSONObject("error");
                    if (error != null) {
                        msg = error.getString("message");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            if (success && getActivity() != null)
                ((LoginActivity) getActivity()).changeFragment(3);
        }
    }
}

package com.axon.memberApp.Login.LoginFragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginSignUpFragment extends Fragment {
    private View view;
    private Button btnLogin, btnSignUp, btnChangeAppLanguage;

    public LoginSignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.login_sign_up_fragment, container, false);

        initViews();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void initViews() {
        btnLogin = view.findViewById(R.id.btnLogin);
        btnSignUp = view.findViewById(R.id.btnSignUp);
        btnChangeAppLanguage = view.findViewById(R.id.btnChangeAppLanguage);

        setViewActions();
    }

    private void setViewActions() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).changeFragment(1);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCoreCode.getInstance().cache.isSignUpRequest = true;
                if (AppCoreCode.getInstance().cache.signUpWithoutMailFragment != null
                        && AppCoreCode.getInstance().cache.signUpWithoutMailFragment.etxtSignUpFullName != null
                        && AppCoreCode.getInstance().cache.signUpWithoutMailFragment.txtSignUpBirthDate != null
                        && AppCoreCode.getInstance().cache.signUpWithoutMailFragment.etxtSignUpMobileNo != null
                        && AppCoreCode.getInstance().cache.signUpWithoutMailFragment.spSIgnUpGender != null) {
                    AppCoreCode.getInstance().cache.signUpWithoutMailFragment.etxtSignUpFullName.setText("");
                    AppCoreCode.getInstance().cache.signUpWithoutMailFragment.txtSignUpBirthDate.setText("");
                    AppCoreCode.getInstance().cache.signUpWithoutMailFragment.etxtSignUpMobileNo.setText("");
                    AppCoreCode.getInstance().cache.signUpWithoutMailFragment.spSIgnUpGender.setSelection(0);
                }
                ((LoginActivity) getActivity()).changeFragment(4);
            }
        });
        btnChangeAppLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("en")) {
                    Tools.loadLocale(getActivity(), "ar");
                } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("Language").equalsIgnoreCase("ar")) {
                    Tools.loadLocale(getActivity(), "en");
                }
            }
        });
    }
}

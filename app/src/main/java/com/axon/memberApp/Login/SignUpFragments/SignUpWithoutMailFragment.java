package com.axon.memberApp.Login.SignUpFragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.Tools;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.R;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.philliphsu.bottomsheetpickers.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpWithoutMailFragment extends Fragment implements com.philliphsu.bottomsheetpickers.date.DatePickerDialog.OnDateSetListener {
    private View view;
    public Spinner spSIgnUpGender;
    public TextView etxtSignUpFullName, txtSignUpBirthDate, etxtSignUpMobileNo;
    private ImageButton ibtnSignUpWithoutMailBack;
    private Button btnSignUpNext;

    /*Facebook Login*/
    private ImageButton ibtnFacebookSignup;
    private LoginButton btnFacebookSignup;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    /*Google Login*/
    private ImageButton ibtnGoogleSignup;
    private SignInButton googleSignUpButton;

    private ArrayAdapter<CharSequence> genderAdapter;

    private String TAG = "SignUpWithoutMailFragment";

    public SignUpWithoutMailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.sign_up_without_mail_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*Facebook Login*/
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        /*Log.w(TAG, "onActivityResult() -> Google Signed in successfully!!!");
        if (requestCode == 3) {
            updateUIOnGoogleSignInIntent(data);
        } else {

        }*/
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }

    private void initViews() {
        spSIgnUpGender = view.findViewById(R.id.spSIgnUpGender);
        etxtSignUpFullName = view.findViewById(R.id.etxtSignUpFullName);
        txtSignUpBirthDate = view.findViewById(R.id.txtSignUpBirthDate);
        etxtSignUpMobileNo = view.findViewById(R.id.etxtSignUpMobileNo);
        ibtnSignUpWithoutMailBack = view.findViewById(R.id.ibtnSignUpWithoutMailBack);
        btnSignUpNext = view.findViewById(R.id.btnSignUpNext);
        // Setup Gender drop down list
        genderAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.gender, R.layout.spinner_textview_align);
        spSIgnUpGender.setAdapter(genderAdapter);

        /*Facebook Login*/
        ibtnFacebookSignup = view.findViewById(R.id.ibtnFacebookSignup);
        btnFacebookSignup = view.findViewById(R.id.btnFacebookSignup);
        callbackManager = CallbackManager.Factory.create();

        /*Google Signin*/
        ibtnGoogleSignup = view.findViewById(R.id.ibtnGoogleSignup);
        googleSignUpButton = view.findViewById(R.id.sign_up_button);
        googleSignUpButton.setSize(SignInButton.SIZE_STANDARD);

        if (AppCoreCode.getInstance().cache.isSignUpRequest) {
            etxtSignUpFullName.setText("");
            txtSignUpBirthDate.setText("");
            etxtSignUpMobileNo.setText("");
            spSIgnUpGender.setSelection(0);
        }
        initGoogleSignIn();
        setViewsActions();
    }

    private void initGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        AppCoreCode.getInstance().cache.mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }

    private void googleSignUp() {
        ibtnGoogleSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
        googleSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
    }

    private void googleSignIn() {
        Intent signInIntent = AppCoreCode.getInstance().cache.mGoogleSignInClient.getSignInIntent();
        Log.d(TAG, "Google Sign-In Button Called");
        getActivity().startActivityForResult(signInIntent, 3);
    }

    public void updateUIOnGoogleSignInIntent(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        handleSignInResult(task);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Log.w(TAG, "handleSignInResult() -> Google Signed in successfully!!!");
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.w(TAG, "handleSignInResult() -> account email: " + account.getEmail()
                    + " \n ServerAuthCode: " + account.getServerAuthCode()
                    + " \n getIdToken: " + account.getIdToken()
                    + " \n getId: " + account.getId());

            etxtSignUpFullName.setText(account.getDisplayName());
            AppCoreCode.getInstance().cache.googleEmail = account.getEmail();
            AppCoreCode.getInstance().cache.googleAccessToken = account.getIdToken();
            AppCoreCode.getInstance().cache.isGoogleSignUp = true;
        } catch (ApiException e) {
            AppCoreCode.getInstance().cache.mGoogleSignInClient.signOut();
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void setViewsActions() {
        ibtnSignUpWithoutMailBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etxtSignUpFullName.setText("");
                txtSignUpBirthDate.setText("");
                etxtSignUpMobileNo.setText("");
                spSIgnUpGender.setSelection(0);

                AppCoreCode.getInstance().cache.isSignUpRequest = false;
                ((LoginActivity) getActivity()).changeFragment(0);
            }
        });

        txtSignUpBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDatePickerDialog();
            }
        });

        btnSignUpNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInput();
            }
        });

        googleSignUp();

        facebookSignUp();
    }

    private void facebookSignUp() {
        /*Facebook Login*/
        ibtnFacebookSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnFacebookSignup.performClick();
            }
        });
        checkFacebookLoginStatus();
//        btnFacebookSignup.setReadPermissions(Arrays.asList("email", "public_profile"));
        btnFacebookSignup.setFragment(this);
        // To understand & fix facebook token not working visit that link
        // https://developers.facebook.com/docs/reference/androidsdk/current/facebook/com/facebook/login/loginbehavior.html/
//        LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
        btnFacebookSignup.setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
        callbackManager = CallbackManager.Factory.create();
        btnFacebookSignup.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "setViewsAction() --> registerCallback() --> onSuccess() --> Facebook Token: " + loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "setViewsAction() --> registerCallback() --> onError(): " + error);
            }
        });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
//                if (currentAccessToken == null) {
//                    // Log out
//                } else {
                loadUserProfile(currentAccessToken);
//                }

            }
        };
    }

    private void checkFacebookLoginStatus() {
        if (AccessToken.getCurrentAccessToken() != null) {
            loadUserProfile(AccessToken.getCurrentAccessToken());
        }
    }

    private void loadUserProfile(final AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (object != null) {
                        String firstName = object.getString("first_name");
                        String lastName = object.getString("last_name");
                        String email = object.getString("email");
                        String id = object.getString("id");
                        String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

                        Log.d(TAG, "setViewsAction() --> Facebook data: \n" + firstName + "\n- " + lastName + "\n- " + email + "\n- " + image_url);

                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.dontAnimate();

                        etxtSignUpFullName.setText(firstName + " " + lastName);
                        AppCoreCode.getInstance().cache.facebookEmail = email;
                        AppCoreCode.getInstance().cache.facebookAccessToken = accessToken.getToken();
                        AppCoreCode.getInstance().cache.isFacebookSignUp = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoginManager.getInstance().logOut();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name, last_name, email, id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void validateUserInput() {
        if (etxtSignUpFullName.getText().toString().trim().isEmpty()) {
            etxtSignUpFullName.setError(getResources().getString(R.string.view_error_name_required));
            etxtSignUpFullName.requestFocus();
            return;
        }
        if (txtSignUpBirthDate.getText().toString().trim().isEmpty()) {
            txtSignUpBirthDate.setError(getResources().getString(R.string.view_error_birthdate_required));
            txtSignUpBirthDate.requestFocus();
            return;
        }
        if (etxtSignUpMobileNo.getText().toString().trim().isEmpty()) {
            etxtSignUpMobileNo.setError(getResources().getString(R.string.view_error_number_required));
            etxtSignUpMobileNo.requestFocus();
            return;
        }
        if (Tools.isValidPhone(etxtSignUpMobileNo.getText().toString().trim())) {
            etxtSignUpMobileNo.setError(getResources().getString(R.string.view_error_number_wrong));
            etxtSignUpMobileNo.requestFocus();
            return;
        }
        AppCoreCode.getInstance().loginActivity.fullName = etxtSignUpFullName.getText().toString().trim();
        AppCoreCode.getInstance().loginActivity.birthDate = txtSignUpBirthDate.getText().toString().trim();
        AppCoreCode.getInstance().loginActivity.mobileNo = etxtSignUpMobileNo.getText().toString().trim();
        AppCoreCode.getInstance().loginActivity.gender = spSIgnUpGender.getSelectedItem().toString();
        AppCoreCode.getInstance().cache.isSignUpRequest = true;
        if (AppCoreCode.getInstance().cache.isGoogleSignUp) {
            AppCoreCode.getInstance().cache.mGoogleSignInClient.signOut();
        } else if (AppCoreCode.getInstance().cache.isFacebookSignUp) {
            LoginManager.getInstance().logOut();
        }
        ((LoginActivity) getActivity()).changeFragment(5);
    }

    private void showCustomDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog date = new DatePickerDialog.Builder(this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)).build();
        date.setHeaderTextColorSelected(Color.parseColor("#FFFFFF"));
        date.setHeaderTextColorUnselected(Color.parseColor("#FFFFFF"));
        date.setDayOfWeekHeaderTextColorSelected(Color.parseColor("#FFFFFF"));
        date.setDayOfWeekHeaderTextColorUnselected(Color.parseColor("#FFFFFF"));

        date.setAccentColor(Color.parseColor("#171743"));
        date.setBackgroundColor(Color.parseColor("#FFFFFF"));
        date.setHeaderColor(Color.parseColor("#171743"));
        date.setHeaderTextDark(false);
        date.show(getFragmentManager(), TAG);
    }

    /**
     * @param dialog      The dialog associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        String strMonth = String.valueOf(monthOfYear + 1);
        String strDays = String.valueOf(dayOfMonth);
        if ((monthOfYear + 1) < 10) {
            strMonth = "0" + (monthOfYear + 1);
        }
        if (dayOfMonth < 10) {
            strDays = "0" + dayOfMonth;
        }
        txtSignUpBirthDate.setText(year + "-" + strMonth + "-" + strDays);
        dialog.dismiss();
    }
}

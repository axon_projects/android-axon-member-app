package com.axon.memberApp.Login.LoginFragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.axon.memberApp.Global.AppCoreCode;
import com.axon.memberApp.Global.HTTPRest;
import com.axon.memberApp.Login.LoginActivity;
import com.axon.memberApp.Login.Classes.LoginMsg;
import com.axon.memberApp.MemberMainActivity.CreateMemberShip.Classes.ValidMembership;
import com.axon.memberApp.MemberMainActivity.MemberMainViewActivity;
import com.axon.memberApp.MemberMainActivity.MemberNotification.Classes.Notification;
import com.axon.memberApp.MemberMainActivity.MemberProfile.Classes.MemberProfile;
import com.axon.memberApp.R;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private View view;
    private ImageButton ibtnBack;
    private Button btnForgotPassword, btnLoginLoading;
    private EditText txtUsername, txtPassword;

    private AsyncTaskClass asyncTaskClass;
    private AsyncTaskMemberDataClass asyncTaskMemberDataClass;
    private String loginResponse;
    private String TAG = "LoginFragment";

    private boolean isMemberDataReady;
    private Handler handler;

    /*Facebook Login*/
    private ImageButton ibtnFacebookLogin;
    private LoginButton btnFacebookLogin;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    /*Google+ Login*/
    private ImageButton ibtnGoogleLogin;
    private SignInButton sign_in_button;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;
    private static final int RC_SIGN_IN = 111;

    private String typeOfRequest;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.login_fragment, container, false);
        initViews();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.w(TAG, "onActivityResult() -->  requestCode: " + requestCode);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Log.w(TAG, "onActivityResult() -->  data: " + data);
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            /*Facebook Login*/
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initViews() {
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnForgotPassword = view.findViewById(R.id.btnForgotPassword);
        btnLoginLoading = view.findViewById(R.id.btnLoginLoading);

        /*Facebook Login*/
        ibtnFacebookLogin = view.findViewById(R.id.ibtnFacebookLogin);
        btnFacebookLogin = view.findViewById(R.id.btnFacebookLogin);
        callbackManager = CallbackManager.Factory.create();

        /*Google+ Login*/
        ibtnGoogleLogin = view.findViewById(R.id.ibtnGoogleLogin);
        sign_in_button = view.findViewById(R.id.sign_in_button);
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);


        txtUsername = view.findViewById(R.id.txtUsername);
        txtPassword = view.findViewById(R.id.txtPassword);

        checkIfUserAlreadySignedIn();
        initGoogleSignIn();
        setViewsAction();
    }

    private void initGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        AppCoreCode.getInstance().cache.mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }

    private void checkIfUserAlreadySignedIn() {
        if (AppCoreCode.getInstance().sharedpreferences != null) {
            typeOfRequest = AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest");
            startLoadingTimer(true);
        }
    }

    private void setViewsAction() {
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtUsername.setError(null);
                txtPassword.setError(null);
                ((LoginActivity) getActivity()).changeFragment(0);
            }
        });

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivity) getActivity()).changeFragment(2);
            }
        });

        btnLoginLoading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeOfRequest = "NormalLogin";
                startLoadingTimer(false);
            }
        });
        facebookSignIn();
        googleSignUp();
    }

    private void googleSignUp() {
        ibtnGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
    }

    private void googleSignIn() {
        Intent signInIntent = AppCoreCode.getInstance().cache.mGoogleSignInClient.getSignInIntent();
        Log.d(TAG, "Google Sign-In Button Called");
        getActivity().startActivityForResult(signInIntent, 4);
    }

    public void updateUIOnGoogleSignInIntent(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        handleSignInResult(task);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Log.w(TAG, "handleSignInResult() -> Google Signed in successfully!!!");
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.w(TAG, "handleSignInResult() -> account email: " + account.getEmail()
                    + " \n ServerAuthCode: " + account.getServerAuthCode()
                    + " \n getIdToken: " + account.getIdToken()
                    + " \n getId: " + account.getId());

            AppCoreCode.getInstance().cache.googleEmail = account.getEmail();
            AppCoreCode.getInstance().cache.googleAccessToken = account.getIdToken();

            typeOfRequest = "GoogleLogin";
            getDataFromServer();
        } catch (ApiException e) {
            AppCoreCode.getInstance().cache.mGoogleSignInClient.signOut();
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void facebookSignIn() {
        /*Facebook Login*/
        ibtnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnFacebookLogin.performClick();
            }
        });
        checkFacebookLoginStatus();
//        btnFacebookLogin.setReadPermissions(Arrays.asList("email", "public_profile"));
        btnFacebookLogin.setFragment(this);
        // To understand & fix facebook token not working visit that link
        // https://developers.facebook.com/docs/reference/androidsdk/current/facebook/com/facebook/login/loginbehavior.html/
//        LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
        btnFacebookLogin.setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
        callbackManager = CallbackManager.Factory.create();
        btnFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "setViewsAction() --> registerCallback() --> onSuccess() --> Facebook Token: " + loginResult.getAccessToken().getToken());
                AppCoreCode.getInstance().cache.facebookAccessToken = loginResult.getAccessToken().getToken();
                typeOfRequest = "FaceLogin";
                getDataFromServer();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "setViewsAction() --> registerCallback() --> onError(): " + error);
            }
        });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) {
                    // Log out
                } else {
                    loadUserProfile(currentAccessToken);
                }

            }
        };
    }

    private void loadUserProfile(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (object != null) {
                        String firstName = object.getString("first_name");
                        String lastName = object.getString("last_name");
                        String email = object.getString("email");
                        String id = object.getString("id");
                        String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

                        Log.d(TAG, "setViewsAction() --> Facebook data: \n" + firstName + "\n- " + lastName + "\n- " + email + "\n- " + image_url);

                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.dontAnimate();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    LoginManager.getInstance().logOut();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name, last_name, email, id");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private void checkFacebookLoginStatus() {
        if (AccessToken.getCurrentAccessToken() != null) {
            loadUserProfile(AccessToken.getCurrentAccessToken());
        }
    }

    private void startLoadingTimer(boolean isAutoLogin) {
        if (isAutoLogin) {
            if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("FaceLogin")) {
                AppCoreCode.getInstance().cache.facebookAccessToken = AppCoreCode.getInstance().getStringValuesFromSharedPreferences("facebookAccessToken");
                if (!AppCoreCode.getInstance().getStringValuesFromSharedPreferences("facebookAccessToken").equalsIgnoreCase(""))
                    getDataFromServer();
            }else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("GoogleLogin")) {
                AppCoreCode.getInstance().cache.googleAccessToken = AppCoreCode.getInstance().getStringValuesFromSharedPreferences("googleAccessToken");
                if (!AppCoreCode.getInstance().getStringValuesFromSharedPreferences("googleAccessToken").equalsIgnoreCase(""))
                    getDataFromServer();
            } else if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("NormalLogin")) {
                if (!AppCoreCode.getInstance().getStringValuesFromSharedPreferences("username").isEmpty() && !AppCoreCode.getInstance().getStringValuesFromSharedPreferences("password").isEmpty()) {
                    txtUsername.setText(AppCoreCode.getInstance().getStringValuesFromSharedPreferences("username"));
                    txtPassword.setText(AppCoreCode.getInstance().getStringValuesFromSharedPreferences("password"));
                    validateUserInputs();
                }
            }
        } else {
            validateUserInputs();
        }

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isMemberDataReady) {
                    if (AppCoreCode.getInstance().sweetAlertDialog != null)
                        AppCoreCode.getInstance().sweetAlertDialog.dismissWithAnimation();
                }
            }
        }, 3000);
    }

    private void getProfileSettingInfoFromServer() {
        asyncTaskMemberDataClass = new AsyncTaskMemberDataClass();
        asyncTaskMemberDataClass.execute();
    }

    private void showLoadingDialog() {
        if (getActivity() != null) {
            AppCoreCode.getInstance().sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            AppCoreCode.getInstance().sweetAlertDialog.getProgressHelper().setBarColor(R.color.axonTextDarkPurpleColor);
            AppCoreCode.getInstance().sweetAlertDialog.setTitleText(getResources().getString(R.string.sweet_alert_loading));
            AppCoreCode.getInstance().sweetAlertDialog.setCancelable(true);
            AppCoreCode.getInstance().sweetAlertDialog.show();
            AppCoreCode.getInstance().sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    txtUsername.setEnabled(true);
                    txtPassword.setEnabled(true);
                }
            });
        }
    }

    private void validateUserInputs() {
        if (txtUsername.getText().toString().trim().isEmpty()) {
            txtUsername.requestFocus();
            txtUsername.setError(getResources().getString(R.string.view_error_username));
            return;
        }
        if (txtPassword.getText().toString().trim().isEmpty()) {
            txtPassword.requestFocus();
            txtPassword.setError(getResources().getString(R.string.view_error_password));
            return;
        }
        txtUsername.setEnabled(false);
        txtPassword.setEnabled(false);
//        showLoadingDialog();
        typeOfRequest = "NormalLogin";
        getDataFromServer();
    }

    private void getDataFromServer() {
        try {
            asyncTaskClass = new AsyncTaskClass();
            asyncTaskClass.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncTaskClass extends AsyncTask<String, String, String> {
        String username = "";
        String password = "";

        @Override
        protected void onPreExecute() {
            Log.d(TAG, "AsyncTaskClass --> called");
            if (typeOfRequest != null && typeOfRequest.equalsIgnoreCase("NormalLogin")) {
                username = txtUsername.getText().toString().trim();
                password = txtPassword.getText().toString().trim();
            }
            showLoadingDialog();
            AppCoreCode.getInstance().httpRest = new HTTPRest();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                if (typeOfRequest.equalsIgnoreCase("NormalLogin")) {
                    loginResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/account/authenticate", "{\"usernameOrEmailAddress\":\"" + username + "\",\"password\":\"" + password + "\"}");
                } else if (typeOfRequest.equalsIgnoreCase("FaceLogin")) {
                    loginResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/authtoken/FacebookLogin", "{\"accessToken\":\"" + AppCoreCode.getInstance().cache.facebookAccessToken + "\"}");
                } else if (typeOfRequest.equalsIgnoreCase("GoogleLogin")) {
                    loginResponse = AppCoreCode.getInstance().httpRest.sendHTTPPostRequest("/api/authtoken/GoogleLogin", "{\"accessToken\":\"" + AppCoreCode.getInstance().cache.googleAccessToken + "\"}");
                }
                Log.d("LoginFragment", "JSON response: " + loginResponse);
                AppCoreCode.getInstance().cache.loginMsg = AppCoreCode.getInstance().cache.gson.fromJson(loginResponse, LoginMsg.class);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            // TODO: NPE cause server delay
            if (AppCoreCode.getInstance().cache.loginMsg != null && AppCoreCode.getInstance().cache.loginMsg.success) {
                if (AppCoreCode.getInstance().cache.loginMsg.result.accountType.equalsIgnoreCase("Member")) {
                    AppCoreCode.getInstance().cache.accessToken = AppCoreCode.getInstance().cache.loginMsg.result.result;
                    // Save Login info after success
                    AppCoreCode.getInstance().saveValuesToSharedPreferences("typeOfRequest", typeOfRequest);
                    if (typeOfRequest.equalsIgnoreCase("NormalLogin")) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("username", txtUsername.getText().toString().trim());
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("password", txtPassword.getText().toString().trim());
                    } else if (typeOfRequest.equalsIgnoreCase("FaceLogin")) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("facebookAccessToken", AppCoreCode.getInstance().cache.facebookAccessToken);
                    }else if (typeOfRequest.equalsIgnoreCase("GoogleLogin")) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("googleAccessToken", AppCoreCode.getInstance().cache.googleAccessToken);
                    }
                    cancelLoadingIfSomethingWentWrong();
//                    getActivity().startActivity(new Intent(getActivity(), MemberMainViewActivity.class));
                    getProfileSettingInfoFromServer();
                } else {
//                    if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("FaceLogin") && LoginManager.getInstance() != null) {
//                        AppCoreCode.getInstance().saveValuesToSharedPreferences("facebookAccessToken", "");
//                        LoginManager.getInstance().logOut();
//                    }
                    cancelLoadingIfSomethingWentWrong();

                    Toast.makeText(getActivity(), "Sorry, you need to Provider App to Login!", Toast.LENGTH_LONG).show();
                }
            } else {
                if (AppCoreCode.getInstance().cache.loginMsg != null &&
                        AppCoreCode.getInstance().cache.loginMsg.error != null
                        && AppCoreCode.getInstance().cache.loginMsg.error.details != null) {
                    if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("FaceLogin") && LoginManager.getInstance() != null) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("facebookAccessToken", "");
                        LoginManager.getInstance().logOut();
                    }
                    if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("GoogleLogin") && LoginManager.getInstance() != null) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("googleAccessToken", "");
                        AppCoreCode.getInstance().cache.mGoogleSignInClient.signOut();
                    }
                    cancelLoadingIfSomethingWentWrong();
                    if (getActivity() != null)
                        Toast.makeText(getActivity(), AppCoreCode.getInstance().cache.loginMsg.error.details
                                .replace("[", "").replace("]", ""), Toast.LENGTH_LONG).show();
                } else {
                    if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("FaceLogin") && LoginManager.getInstance() != null) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("facebookAccessToken", "");
                        LoginManager.getInstance().logOut();
                    }
                    if (AppCoreCode.getInstance().getStringValuesFromSharedPreferences("typeOfRequest").equalsIgnoreCase("GoogleLogin") && LoginManager.getInstance() != null) {
                        AppCoreCode.getInstance().saveValuesToSharedPreferences("googleAccessToken", "");
                        AppCoreCode.getInstance().cache.mGoogleSignInClient.signOut();
                    }
                    cancelLoadingIfSomethingWentWrong();
                    if (getActivity() != null)
                        Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void cancelLoadingIfSomethingWentWrong() {
        isMemberDataReady = false;
        if (AppCoreCode.getInstance().sweetAlertDialog != null)
            AppCoreCode.getInstance().sweetAlertDialog.dismissWithAnimation();
        if (handler != null)
            handler.removeCallbacksAndMessages(null);
    }

    private class AsyncTaskMemberDataClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            AppCoreCode.getInstance().httpRest = new HTTPRest();
            Log.d(TAG, "AsyncTaskMemberDataClass --> called");
            showLoadingDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            getMemberInfo();
            getValidMembership();
            getQRcodeLink();
            getNotifications();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (AppCoreCode.getInstance().sweetAlertDialog != null)
                AppCoreCode.getInstance().sweetAlertDialog.dismissWithAnimation();
//            isMemberDataReady = false;
            isMemberDataReady = true;
            txtUsername.setText("");
            txtPassword.setText("");
            if (getActivity() != null) {
                getActivity().finish();

                getActivity().startActivity(new Intent(getActivity(), MemberMainViewActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        }

        private void getMemberInfo() {
            String profileInfoResponse;
            try {
                profileInfoResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getprofile");

                JSONObject reader = new JSONObject(profileInfoResponse);
                AppCoreCode.getInstance().cache.memberProfile = AppCoreCode.getInstance().cache.gson.fromJson(reader.getJSONObject("result").toString(), MemberProfile.class);

//                InputStream in = new java.net.URL(AppCoreCode.getInstance().cache.mainURL + "//" + AppCoreCode.getInstance().cache.memberProfile.avatarUrl).openStream();
//                AppCoreCode.getInstance().cache.memberAvatarBitmapImg = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getValidMembership() {
            String validMembershipResponse;
            try {
                validMembershipResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getvalidmembership");
                JSONObject reader = new JSONObject(validMembershipResponse);
                AppCoreCode.getInstance().cache.validMembership = AppCoreCode.getInstance().cache.gson.fromJson(reader.toString(), ValidMembership.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getQRcodeLink() {
            String qrCodeResponse;
            try {
                qrCodeResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/membercard/getmemberqrcodelink");
                JSONObject reader = new JSONObject(qrCodeResponse);

                InputStream in = new java.net.URL(AppCoreCode.getInstance().cache.mainURL + "//" + reader.getString("FileName")).openStream();
                AppCoreCode.getInstance().cache.qrCodeFileName = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getNotifications() {
            String notificationResponse;
            try {
                notificationResponse = AppCoreCode.getInstance().httpRest.sendHTTPGetRequest("/api/memberapp/getnotifications");
                AppCoreCode.getInstance().cache.notification = AppCoreCode.getInstance().cache.gson.fromJson(notificationResponse, Notification.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
